# Flying Sea Bird Template
# (Fulmars, Antarctic Petrels and Cape Petrels)
# KBN 2013/08/29

# NOTE: must have these values in here to calculate power consumptions
# turn on delay
config 1 5
# expose and write delay
config 2 15
# camera pulse time
config h 2

# settle shutter every day
event a ** ** 00 05 "servo s"

# January - 10 per day
event a 01 ** 20 00 "picture"
event a 01 ** 21 00 "picture"
event a 01 ** 22 00 "picture"
event a 01 ** 23 00 "picture"
event a 01 ** 00 00 "picture"
event a 01 ** 01 00 "picture"
event a 01 ** 02 00 "picture"
event a 01 ** 03 00 "picture"
event a 01 ** 04 00 "picture"
event a 01 ** 05 00 "picture"

# February - 10 per day
event a 02 ** 20 00 "picture"
event a 02 ** 21 00 "picture"
event a 02 ** 22 00 "picture"
event a 02 ** 23 00 "picture"
event a 02 ** 00 00 "picture"
event a 02 ** 01 00 "picture"
event a 02 ** 02 00 "picture"
event a 02 ** 03 00 "picture"
event a 02 ** 04 00 "picture"
event a 02 ** 05 00 "picture"

# March - 10 per day
event a 03 ** 20 00 "picture"
event a 03 ** 21 00 "picture"
event a 03 ** 22 00 "picture"
event a 03 ** 23 00 "picture"
event a 03 ** 00 00 "picture"
event a 03 ** 01 00 "picture"
event a 03 ** 02 00 "picture"
event a 03 ** 03 00 "picture"
event a 03 ** 04 00 "picture"
event a 03 ** 05 00 "picture"

# April - 1 per day
event a 04 ** 00 00 "picture"

# May to end August, 1 per week
event a 05 01 00 00 "picture"
event a 05 08 00 00 "picture"
event a 05 15 00 00 "picture"
event a 05 22 00 00 "picture"
event a 05 29 00 00 "picture"
event a 06 05 00 00 "picture"
event a 06 12 00 00 "picture"
event a 06 19 00 00 "picture"
event a 06 26 00 00 "picture"
event a 07 03 00 00 "picture"
event a 07 10 00 00 "picture"
event a 07 17 00 00 "picture"
event a 07 24 00 00 "picture"
event a 07 31 00 00 "picture"
event a 08 07 00 00 "picture"
event a 08 14 00 00 "picture"
event a 08 21 00 00 "picture"
event a 08 28 00 00 "picture"

# September - 1 per day
event a 09 ** 00 00 "picture"

# October - 1 per day 
event a 10 ** 00 00 "picture"

# November - 10 per day
event a 11 ** 20 00 "picture"
event a 11 ** 21 00 "picture"
event a 11 ** 22 00 "picture"
event a 11 ** 23 00 "picture"
event a 11 ** 00 00 "picture"
event a 11 ** 01 00 "picture"
event a 11 ** 02 00 "picture"
event a 11 ** 03 00 "picture"
event a 11 ** 04 00 "picture"
event a 11 ** 05 00 "picture"

# December - 10 per day
event a 12 ** 20 00 "picture"
event a 12 ** 21 00 "picture"
event a 12 ** 22 00 "picture"
event a 12 ** 23 00 "picture"
event a 12 ** 00 00 "picture"
event a 12 ** 01 00 "picture"
event a 12 ** 02 00 "picture"
event a 12 ** 03 00 "picture"
event a 12 ** 04 00 "picture"
event a 12 ** 05 00 "picture"

