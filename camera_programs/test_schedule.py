#!/usr/bin/env python

"""
The intent of this script is to provide some better tools to arrive at a
schedule to be used in penguin population camera controllers that best 
suits research needs while balancing the need for light, power and storage
space withing the camera
Mark Milnes - 01 October 2012 - mark_mil@aad.gov.au
"""

import ephem
import datetime
import math
import re
import calendar
from pprint import pprint

#Set up an object that is a dictionary keyed by datetime......

#Set up camera location
camera_location = ephem.Observer()
camera_location.lon = '77.9689'
camera_location.lat = '-68.5764'
camera_location.elevation = 0
camera_location.horizon = '-6' #Civil twighlight......

#Set up list of dates over the next year
datelist = [datetime.date.today() + datetime.timedelta(days = offset) 
			   for offset in range(365)]

def pictime(date,interval,location):
	"""Returns a set containing the times when valid photos may be taken on a
	   particular date at a particular time """
	#Calculate rough UTC ofset....
	utc_offset = round(12*camera_location.lon.real/math.pi)
	valid_times = range(0,24,interval) 
	#Set valid times such that middle pic is at solar noon
	noon = 12 - utc_offset
	valid_times = [(time + noon) % 24 for time in valid_times]
	#Now I have a list of times... Work out which ones occur during daylight
	return_list = list()
	for time in valid_times:
		location.date = datetime.datetime.combine(date,datetime.time()) \
		                + datetime.timedelta(hours=time)
		if ephem.Sun(location).alt > location.horizon :
			return_list.append(time%24)
	return set(return_list)		

interval = 4 #Time between pictures in hours......				 
picture_dictionary = dict(zip(datelist,[pictime(date,interval,camera_location) for date in datelist]))

#Now we have a dictionary... We need to convert it into cron fromat....

#Find all the times that appear in ALL days i nthe year:
cron_dict = dict()
cron_dict['** **'] = set.intersection(*[picture_dictionary[index] for index in picture_dictionary])

#Find the times that appear on ALL days in each month:
for month in range(1,13):
	month_key = '%02d **'%month
	month_value = set.intersection(*[picture_dictionary[index] for index in picture_dictionary if (index.month == month)])
	month_value = month_value - cron_dict['** **']
	if (month_value):
		cron_dict[month_key]=month_value
	tens = calendar.monthrange(datetime.date.today().year,month)[1]/10
	for day_tens in range(0,tens+1):
		day10_key = '%02d %d*'%(month,day_tens)
		day10_re = '%02d %d.?'%(month,day_tens)
		day10_value = set.intersection(*[picture_dictionary[index] \
	                                   for index in picture_dictionary \
	                                   if re.match(day10_re,index.strftime('%m %d'))])
		day10_value = day10_value - month_value
		day10_value = day10_value - cron_dict['** **']
		if (day10_value):
			cron_dict[day10_key] = day10_value
			
		if (day_tens == 0):
			start = 1
		else:
			start = 0
			
		for day in range(start,min(calendar.monthrange(datetime.date.today().year,month)[1]-10*day_tens,10)) :
			day_key = '%02d %d%d'%(month,day_tens,day)
			day_value = set.intersection(*[picture_dictionary[index]
	                                   for index in picture_dictionary 
	                                   if re.match(day_key,index.strftime('%m %d'))])
			day_value = day_value - day10_value
			day_value = day_value - month_value
			day_value = day_value - cron_dict['** **']
			if (day_value):
				cron_dict[day_key] = day_value
				            
for entry in cron_dict:
	for hour in cron_dict[entry]:
		print 'event a %s %02d 00 "picture"'%(entry,hour)

print "#Total Pictures = %d"%sum([len(day) for day in picture_dictionary.values()])