import serial
import serial.tools.list_ports
import time
import re
import os
import sys
import threading
from datetime import datetime as dt

#Some globals.... Yuck
PATH_TO_PROGRAMS = 'C:\\Documents and Settings\\Administrator\\Desktop\\PPCC\\'
GENERATOR_PROGRAM = 'generate_programs.py'
GENERATED_DIR = 'programs'
GENERATOR_CONF = 'camera_programs.conf'

THREAD_LOCK = threading.Lock()

#Objects to support threading....
class uploader:
    def __init__(self,controller,password,output_lock):
        self.prog_file = open(os.path.join(GENERATED_DIR,controller[1]))
        self.serial_port = controller[0]
        self.password = password
        self.ver = None
        self.lock = output_lock
    
    def run(self):
        with self.serial_port as comms:
            comms.open()
            comms.setDTR(1)
            comms.setRTS(1)
            time.sleep(0.5) #Time to charge internal caps....
            while len(comms.readline()) > 0 :
                pass
            comms.write("ver\r\n")
            comms.readline() #discard nonsense line
            self.ver = comms.readline()
            if not re.search('1.1.D007',self.ver) :
                self.output("Old Firmware detected.... Applying update....")
                comms.write("unlock %s\r\n"%password)
                comms.readline() #discard nonsense line
                comms.readline() #discard echo
                response = comms.readline()
                if re.search("^Unlocking flash",response):
                    self.output("Password accepted")
                else:
                    self.output("Password failed")
                    self.output("Aborting attempt")
                    self.stop()
                    return
                comms.write("intel\r\n")
                self.output("Erasing Flash")
                #Wait for controller to ask for file
                temp = comms.readline()
                while not re.search("Send Intel Hex file now!",temp):
                    temp = temp+comms.readline()
                self.output("Sending hex file... May take some time...")
                hexfile = open("ppcc.hex",'r')
                counter = 0
                for line in hexfile:
                    comms.write(line)
                    #Most lines seems to be acknowledged by a dot....
                    #Those that are not still get a 100ms timeout!
                    comms.read(1)
                    counter = counter + 1
                    if counter == 100:
                        self.output("Still going.....")
                        counter = 0
                hexfile.close()
                response = comms.read(10)
                if re.search("OK",response) :
                    self.output("Firmware Upgraded")
                else:
                    self.output("It appears the controller is not responding... Return to STS for service....")
                    self.close()
                    return
            time.sleep(0.5)
            while len(comms.readline()) > 0 :
                pass
            #Ensure that the clock is set properly
            comms.write("clock p\r\n")
            comms.readline() #Gobble Echo

            controller_time = dt.strptime(comms.readline().strip(),
                                          '%Y/%m/%d %H:%M:%S')
            error = abs((dt.utcnow()-controller_time).total_seconds())
            comms.readline() #Gobble Ready
            if error > 2 :
                self.output("RTC error is %s seconds... "%error + 
                            "Resetting clock from PC")
                comms.write(dt.strftime(dt.utcnow(),
                                        "clock s %y %m %d %H %M %S\r\n"))
                comms.readline() # Gobble Echo
                self.output(comms.readline().strip()) #Report Set time
                comms.readline() # Gobble ready
            #Now write new schedule
            self.output("Attempting to erase any existing schedules")
            comms.write("unlock %s\r\n"%password)
            comms.readline() #discard echo
            response = comms.readline()
            if re.search("^Unlocking flash",response):
                self.output("Password accepted")
            else:
                raise PasswordError
            comms.readline() #Gobble unlocked message
            comms.readline() #Gobble ready> prompt
            comms.write("event e\r\n")
            comms.readline() #Goble Echo
            comms.timeout = 20
            temp = comms.readline()
            if not re.search("Erased",temp):
                raise FlashEraseError
            comms.timeout = 0.1
            comms.readline() #Gobble ready> prompt    
            self.output("Writing new schedule....")
            for line in self.prog_file:
                if not (re.match('#',line)): #Ignore comments!
                    if (line.strip() != ""): #ignore empty lines
                        comms.write(line.rstrip()+"\r\n")
                        temp = comms.readline()
                        if (temp.strip() != line.strip()) :
                           raise FlashWriteError
                        while not re.search("ready>",comms.readline()):
                            pass
            self.stop()
            self.output("All done!!!")
            
    def stop(self):
        self.serial_port.setRTS(0)
        self.serial_port.setDTR(0)
        self.serial_port.close()
        self.prog_file.close()
                
    def output(self,output_string):
        self.lock.acquire()
        print self.serial_port.port+" reports: "+output_string
        print "" #Add some spacing....
        self.lock.release()

#Check the modification times of the directory
try:
    regenerate = False
    if os.path.getmtime(GENERATOR_PROGRAM) > os.path.getmtime(GENERATED_DIR) :
        regenerate = True
    if os.path.getmtime(GENERATOR_CONF) > os.path.getmtime(GENERATED_DIR) :
        regenerate = True
except:
    regenerate = True
    pass
try:
    if regenerate:
        #Generate the program files and report files using Python Script form Kym N
        execfile(GENERATOR_PROGRAM)
except:
    raise

#Now get a list of generated files
location_files = os.listdir(GENERATED_DIR)

#Get a list of ports with controllers....
controllers = [] #Will be a list of tuples containing port,file pairs

for port in serial.tools.list_ports.grep('.*'):
    try:
        if re.search("TruePort",port[1]):
            print "Ignoring Virtual Port: %s"%port[1]
            welcome = ""
        else:
            print "Checking %s"%port[1]
            with serial.Serial(port[0],timeout=0.1) as this_port:
                this_port.setDTR(0)
                this_port.setRTS(0)
                time.sleep(0.1)
                this_port.flush()
                this_port.setDTR(1)
                this_port.setRTS(1)
                time.sleep(0.5)
                welcome = this_port.readline();
                this_port.close()
    except:
        print "Error occured on Com Port labelled: %s"%port[1]
        print "Port is probably already open..."
        welcome = ""
        
    if re.search('Penguin Population Camera Controller',welcome):
        file_exists = False
        while not file_exists :
            print "Please enter the 4 letter + 1 number code to be" + \
                  " used on controller connect to %s,"%port[1] + \
                  " or hit enter to see a lits of possible names:"
            location_code = raw_input()
            matched_names = [re.search("^"+location_code,file_name)!=None
                             for file_name in location_files]
            if matched_names.count(True) == 1 :
                controllers.append((this_port,location_files[matched_names.index(True)]))
                file_exists = True
            else :
                print "Possible names are:"
                print [file_name[:5] for file_name in location_files]

if len(controllers) > 0 :
    #Now prompt for flash unlocking password:
    print "Enter password to unlock flash (hint: It's 'password' or CTRL-C to exit) :"
    password = raw_input()

    #Now for each connected controller, we can start a thread that loads the data on the controller!
    uploaders = [uploader(controller,password,THREAD_LOCK) for controller in controllers]

    #Now launch the run method for each of the uploaders as a thread......
    upload_threads = [threading.Thread(target=ppcc.run) for ppcc in uploaders]
    for upload in upload_threads:
        upload.start()
    for upload in upload_threads:
        upload.join()
            


