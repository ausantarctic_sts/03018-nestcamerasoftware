# AdeliePenguin Template
# Site name                     : LONG1
# Subject/template              : AdeliePenguin
# KBN 2013/08/29
# Longitude                     : 78.070
# Solar noon occurs at          : 06:47 UTC
# Template shifted by           : 07:00
# Template shift delta          : 00h -12m
# Leap year                     : False
#
# Serial String Length = 25 of 30 characters
serial "LONG1 @7.00 AdeliePenguin"

# KBN 2015/09/01 - brought forward 10 images/day from Nov 15 to Nov 1.

# NOTE: must have these values in here to calculate
# power consumptions
# turn on delay
config 1 5
# expose and write delay
config 2 15
# camera pulse time
config h 2

# settle shutter every day
event a ** ** 07 05 "servo s"

# Actual Picture programs
# January - 10 photos per day
event a 01 ** 03 00 "picture"
event a 01 ** 04 00 "picture"
event a 01 ** 05 00 "picture"
event a 01 ** 06 00 "picture"
event a 01 ** 07 00 "picture"
event a 01 ** 08 00 "picture"
event a 01 ** 09 00 "picture"
event a 01 ** 10 00 "picture"
event a 01 ** 11 00 "picture"
event a 01 ** 12 00 "picture"

# February - 10 photos per day
event a 02 ** 03 00 "picture"
event a 02 ** 04 00 "picture"
event a 02 ** 05 00 "picture"
event a 02 ** 06 00 "picture"
event a 02 ** 07 00 "picture"
event a 02 ** 08 00 "picture"
event a 02 ** 09 00 "picture"
event a 02 ** 10 00 "picture"
event a 02 ** 11 00 "picture"
event a 02 ** 12 00 "picture"

# March - September 20, 1 per week
event a 03 01 07 00 "picture"
event a 03 08 07 00 "picture"
event a 03 15 07 00 "picture"
event a 03 22 07 00 "picture"
event a 03 29 07 00 "picture"
event a 04 05 07 00 "picture"
event a 04 12 07 00 "picture"
event a 04 19 07 00 "picture"
event a 04 26 07 00 "picture"
event a 05 03 07 00 "picture"
event a 05 10 07 00 "picture"
event a 05 17 07 00 "picture"
event a 05 24 07 00 "picture"
event a 05 31 07 00 "picture"
event a 06 07 07 00 "picture"
event a 06 14 07 00 "picture"
event a 06 21 07 00 "picture"
event a 06 28 07 00 "picture"
event a 07 05 07 00 "picture"
event a 07 12 07 00 "picture"
event a 07 19 07 00 "picture"
event a 07 26 07 00 "picture"
event a 08 02 07 00 "picture"
event a 08 09 07 00 "picture"
event a 08 16 07 00 "picture"
event a 08 23 07 00 "picture"
event a 08 30 07 00 "picture"
event a 09 06 07 00 "picture"
event a 09 13 07 00 "picture"
# 1 photo per day, Sept/20 to October 31
event a 09 2* 07 00 "picture"
event a 09 3* 07 00 "picture"

# October - 1 photo per day
event a 10 ** 07 00 "picture"

# November - 10 photos per day
event a 11 ** 03 00 "picture"
event a 11 ** 04 00 "picture"
event a 11 ** 05 00 "picture"
event a 11 ** 06 00 "picture"
event a 11 ** 07 00 "picture"
event a 11 ** 08 00 "picture"
event a 11 ** 09 00 "picture"
event a 11 ** 10 00 "picture"
event a 11 ** 11 00 "picture"
event a 11 ** 12 00 "picture"

# December - 10 photos per day
event a 12 ** 03 00 "picture"
event a 12 ** 04 00 "picture"
event a 12 ** 05 00 "picture"
event a 12 ** 06 00 "picture"
event a 12 ** 07 00 "picture"
event a 12 ** 08 00 "picture"
event a 12 ** 09 00 "picture"
event a 12 ** 10 00 "picture"
event a 12 ** 11 00 "picture"
event a 12 ** 12 00 "picture"
