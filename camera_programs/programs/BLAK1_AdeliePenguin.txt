# AdeliePenguin Template
# Site name                     : BLAK1
# Subject/template              : AdeliePenguin
# KBN 2013/08/29
# Longitude                     : 110.580
# Solar noon occurs at          : 04:37 UTC
# Template shifted by           : 05:00
# Template shift delta          : 00h -22m
# Leap year                     : False
#
# Serial String Length = 25 of 30 characters
serial "BLAK1 @5.00 AdeliePenguin"

# KBN 2015/09/01 - brought forward 10 images/day from Nov 15 to Nov 1.

# NOTE: must have these values in here to calculate
# power consumptions
# turn on delay
config 1 5
# expose and write delay
config 2 15
# camera pulse time
config h 2

# settle shutter every day
event a ** ** 05 05 "servo s"

# Actual Picture programs
# January - 10 photos per day
event a 01 ** 01 00 "picture"
event a 01 ** 02 00 "picture"
event a 01 ** 03 00 "picture"
event a 01 ** 04 00 "picture"
event a 01 ** 05 00 "picture"
event a 01 ** 06 00 "picture"
event a 01 ** 07 00 "picture"
event a 01 ** 08 00 "picture"
event a 01 ** 09 00 "picture"
event a 01 ** 10 00 "picture"

# February - 10 photos per day
event a 02 ** 01 00 "picture"
event a 02 ** 02 00 "picture"
event a 02 ** 03 00 "picture"
event a 02 ** 04 00 "picture"
event a 02 ** 05 00 "picture"
event a 02 ** 06 00 "picture"
event a 02 ** 07 00 "picture"
event a 02 ** 08 00 "picture"
event a 02 ** 09 00 "picture"
event a 02 ** 10 00 "picture"

# March - September 20, 1 per week
event a 03 01 05 00 "picture"
event a 03 08 05 00 "picture"
event a 03 15 05 00 "picture"
event a 03 22 05 00 "picture"
event a 03 29 05 00 "picture"
event a 04 05 05 00 "picture"
event a 04 12 05 00 "picture"
event a 04 19 05 00 "picture"
event a 04 26 05 00 "picture"
event a 05 03 05 00 "picture"
event a 05 10 05 00 "picture"
event a 05 17 05 00 "picture"
event a 05 24 05 00 "picture"
event a 05 31 05 00 "picture"
event a 06 07 05 00 "picture"
event a 06 14 05 00 "picture"
event a 06 21 05 00 "picture"
event a 06 28 05 00 "picture"
event a 07 05 05 00 "picture"
event a 07 12 05 00 "picture"
event a 07 19 05 00 "picture"
event a 07 26 05 00 "picture"
event a 08 02 05 00 "picture"
event a 08 09 05 00 "picture"
event a 08 16 05 00 "picture"
event a 08 23 05 00 "picture"
event a 08 30 05 00 "picture"
event a 09 06 05 00 "picture"
event a 09 13 05 00 "picture"
# 1 photo per day, Sept/20 to October 31
event a 09 2* 05 00 "picture"
event a 09 3* 05 00 "picture"

# October - 1 photo per day
event a 10 ** 05 00 "picture"

# November - 10 photos per day
event a 11 ** 01 00 "picture"
event a 11 ** 02 00 "picture"
event a 11 ** 03 00 "picture"
event a 11 ** 04 00 "picture"
event a 11 ** 05 00 "picture"
event a 11 ** 06 00 "picture"
event a 11 ** 07 00 "picture"
event a 11 ** 08 00 "picture"
event a 11 ** 09 00 "picture"
event a 11 ** 10 00 "picture"

# December - 10 photos per day
event a 12 ** 01 00 "picture"
event a 12 ** 02 00 "picture"
event a 12 ** 03 00 "picture"
event a 12 ** 04 00 "picture"
event a 12 ** 05 00 "picture"
event a 12 ** 06 00 "picture"
event a 12 ** 07 00 "picture"
event a 12 ** 08 00 "picture"
event a 12 ** 09 00 "picture"
event a 12 ** 10 00 "picture"
