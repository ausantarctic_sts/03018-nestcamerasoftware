#!/usr/bin/python
"""
generate_programs.py

Creates site specific program files from a set of pre-existing templates for each species,
given a list of sites and their properties. This program replaces the previous Excel 
spreadsheet which had become unmanageable.

Inputs:
    1) Template files
    2) Site configuration conf

Outputs
    1) A list of controller programs for each site/species.
    2) A set of report files for each site analysing the picture schedule and power 
       consumption requirements.


Project      : 03018
Module       : camera_programs
Author       : KBN
Latest Update: 2013/09/31
             : 2022/10/25 - Python 3 ised..

Description:

This is a command line program, that takes a site configuration file in JSON format
which has global information about the camera programs and also site specific
info required to make the custom nest camera programs for each site/species/time offset
combination.

It uses the config longitude to determine the exact time of solar noon at a site, however
the user can over-ride this value by specifying the 'solar_noon_shift', which is the
number of hours that the template times are SHIFTED to make the template centred around
solar noon at the site.  For example, a template entry of "01 00" (ie solar noon +1 hr),
at Davis, where solar noon is really about 06:50 UTC, has solar_noon_shift = 7.0 hours 
(which value is floating point hours, NOT HH:MM), so the picture is taken when UTC time 
= 08:00.  

This script also performs reporting on the date times that events are expected to occur, 
and how much power will be consumed from the battery.

Doesn't (YET) calculate available solar power input because there is too much variation in the 
site solar panel arrangement, rather decides that there is plenty of light in the summer
and the battery needs to cope with 6 months of winter operation (or sleeping).

Note that the camera controller firmware requires the programs to have lines ending in CR or CR+LF
but not just LF, which is UNIX default, so this script makes sure lines are CRLF ended.

"""
import os
import sys
import json
import argparse
import datetime
import copy
import math
import calendar

#####################################################
# Dictionary and config file keys
# Site specific
KEY_ACTIVE = "active"
KEY_SUBJECT = "subject"
KEY_SOLAR_NOON_SHIFT = "solar_noon_shift"
KEY_CAMERA_MODEL = "camera_model"
KEY_LONGITUDE = "longitude"

# top level
KEY_GLOBAL_CONFIG = "global_config"
KEY_POWER_CONSUMPTION = "power_consumption"
KEY_TEMPLATE_DIR = "template_dir"
KEY_OUTPUT_DIR = "output_dir"
KEY_REPORT_DIR = "report_dir"
KEY_KNOWN_SUBJECTS = "known_subjects"
KEY_BATTERY_MODEL = "battery_model"
KEY_BATTERY_MODELS = "battery_models"
KEY_VOLTAGE = "voltage"
KEY_AH_NOMINAL = "Ah_nominal"
KEY_AH_NEG35C = "Ah_neg35C"
KEY_CAMERA_MODELS = "camera_models"
KEY_SITES = "sites"

# Event keys
KEY_EVENT_AS = "event_As"
KEY_EVENT_QUIESCENT = "quiescent"
KEY_EVENT_CAMERA_P = "camera p"
KEY_EVENT_PICTURE = "picture"
KEY_EVENT_SERVO_S = "servo s"
KEY_CONFIG = "config"

KEY_IDLE_TIME      = 'idleTime'
KEY_IDLE_ENERGY    = 'idleEnergy'
KEY_PICTURE_TIME   = 'pictureTime'
KEY_PICTURE_ENERGY = 'pictureEnergy'
KEY_PULSE_TIME     = 'pulseTime'
KEY_PULSE_ENERGY   = 'pulseEnergy'
KEY_SERVO_TIME     = 'servoTime'
KEY_SERVO_ENERGY   = 'servoEnergy'
KEY_PERIOD_TIME    = 'PeriodTime'
KEY_TOTAL_ENERGY   = 'totalEnergy'

##########################0###########################

def wc_list (wc = "", LeapYear = False):
    """
    Return a list of event positions that match the wildcard string
    LeapYear set to True or False to calculate for a leapyear

    Attempts to save calculation time by not having to visit every single possible 
    combination of dates/times to see if wildcard matches.
    """
    wcs = ['*', '?']
    num = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    short_months = ['0230', '0231', '0431', '0631', '0931', '1131']
    if not LeapYear:
        short_months.append('0229')
    nMin = [[ 0, 0],
            [ 1, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [ 1, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 0, 0, 0]]

            # 0  1  2  3  4  5  6  7  8  9
    nMax = [[ 1, 1],
            [ 9, 2],
            [ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
            [ 9, 9, 9, 1, 0],
            [ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [ 9, 9, 3],
            [ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
            [ 9, 9, 9, 9, 9, 9]]

    pList = []

    if (len(wc) == 8):
        # positions from 10's months to 1's of minutes
        for i in range(0, 8):
            # if plain number, add to end of all elements
            if wc[i] in num:
                if (len(pList) > 0):
                    for j in range(0, len(pList)):
                        pList[j] = pList[j]+wc[i]  
                else:
                    pList.append(wc[i])

            # else if wildcard (* or ?)
            elif wc[i] in wcs:
                if (len(pList) > 0):
                    nList = []
                    for iList in range(0, len(pList)):
                        # create & append all combinations where wildcard exists
                        item = pList[iList]
                        p = int(item[-1])  # current last digit
                        for j in range(nMin[i][p], nMax[i][p]+1):
                            if not (item[0:3]+str(j) in short_months):
                                nList.append(item + str(j))
                    pList = copy.copy(nList)
                    del nList
                else:
                    for c in range(nMin[i][0], nMax[i][0]+1):
                        pList.append(str(c))   
    else:
        return None

    # minor hack to remove zero months and zero days
    while ((pList[0][0:2] == '00') or (pList[0][2:4] == '00')):
        pList.pop(p)

    return pList

def wc_test (dt, wc_packed):
    """ 
    wc_test

    test a wildcard against a datetime, returns true/false

    dt = datetime to tes
    wc_packed = wildcard packed in string as "MMDDHHmm"  (MM = month, mm = minute)

    returns True or False
    """

    # build packed datetime to compare with the wildcard packed time
    dt_packed = dt.strftime("%m%d%H%M")

    # step through to compare strings for a match, skip wildcards
    digit = 0
    for char in wc_packed:
        if ((char == '*') or ( char == '?')):
            digit = digit + 1
            continue
        if (char == dt_packed[digit]):
            digit = digit + 1
        else:
            return False

    return True

def programFromTemplate(Template, TimeShift, Site):
    """
    Process a given template with the given shift, and return it    
    """
    ProgramLines = []
    TemplateLines = Template.splitlines()

    # for utcoffset = 0, the template becomes the program file, and no
    # offset is made to the times.
    if (not TimeShift == 0.0):
        for line in TemplateLines:
            if ((line == "") or
                (line[0] == '#')): 
                ProgramLines.append(line)
            elif not (TimeShift == 0.0):
                fields = line.split(' ', 6)

                # process each line
                cmd = fields[0]

                if (str.lower(cmd) == 'event'):

                    # make sure fields are all double '**'
                    for i in range(2, 5):
                        if (fields[i] == '*'):
                            fields[i] = '**'

                    action = fields[1]
                    MM = fields[2]
                    DD = fields[3]
                    HH = fields[4]
                    mm = fields[5]
                    run_cmd = fields[6]

                    if (str.isalnum(HH) and str.isalnum(mm)):
                        # calc offset shift in minutes
                        TimeShiftMins = int(TimeShift * 60)
                        timeMins = int(HH)*60 + int(mm)

                        timeMins += TimeShiftMins
                        offsetHH = int(timeMins / 60)
                        offsetmm  = int(timeMins % 60)

                        # don't extend to days, fold over in hours only
                        if (offsetHH >= 24):
                            offsetHH = offsetHH - 24
                        elif (offsetHH < 0):
                            offsetHH = 24 - offsetHH

                        ProgramLines.append("{:s} {:s} {:s} {:s} {:02d} {:02d} {:s}".format(cmd, action, MM, DD, offsetHH, offsetmm, run_cmd))
                    else:
                        sys.stderr.write("Error: wildcards encountered in Hours/Mins fields of template file for site '{}'".format(Site))
                        sys.stderr.write("whilst attempting timeshift correction at line '{}'".format(line))
                        return None

                else:
                    ProgramLines.append(line)

    else:
        ProgramLines = copy.copy(TemplateLines)

    if (len(ProgramLines) > 0):
        return ProgramLines
    else:
        return None

def energy_over_period(config, 
                       event_list, 
                       exposeWriteDelay,
                       pulsePeriod,
                       turnOnDelay,
                       battery_voltage,
                       dtStart, 
                       dtEnd,
                       LeapYear) :
    """
    Calculate energy consumption over a defined period, given a sorted command list
    """
    if LeapYear:
        year = 2000
    else:
        year = 2001

    dtBegin = copy.copy(dtStart)
    dtFinish = copy.copy(dtEnd)
    dtBegin = dtBegin.replace(year = year)
    dtFinish = dtFinish.replace(year = year)

    #reset counters
    pulseEnergy = 0.0
    pulseTime = datetime.timedelta(seconds = 0)
    pictureEnergy = 0.0
    pictureTime = datetime.timedelta(seconds = 0)
    servoEnergy = 0.0
    servoTime = datetime.timedelta(seconds = 0)

    for line in event_list:
        (wc, cmd) = line.split(',')        
        # turn MMDDHHmm into datetime
        dtLine = datetime.datetime(year = year,
                                   month = int(wc[0:2]),
                                   day = int(wc[2:4]),
                                   hour = int(wc[4:6]),
                                   minute = int(wc[6:8]))

        if ((dtLine <= dtFinish) and (dtLine >= dtBegin)):
            cmd = cmd.strip('"')
            if   (cmd == KEY_EVENT_CAMERA_P):
                # empirical formula based on measurements for pulse power per second
                pulseEnergy += battery_voltage * ((1.176 * pulsePeriod) - 0.2856)
                pulseTime += datetime.timedelta(seconds = pulsePeriod)
            elif (cmd == KEY_EVENT_PICTURE):
                pictureEnergy += battery_voltage * config[KEY_EVENT_AS][KEY_EVENT_PICTURE]
                pictureTime += datetime.timedelta(seconds = (turnOnDelay + exposeWriteDelay))
            elif (cmd == KEY_EVENT_SERVO_S):
                servoEnergy += battery_voltage * config[KEY_EVENT_AS][KEY_EVENT_SERVO_S]
                servoTime += datetime.timedelta(seconds = 1.5)
            else:
                # unknown event command to measure power for...
                pass         

    PeriodTime = dtFinish - dtBegin
    idleTime = PeriodTime - pulseTime - pictureTime - servoTime        
    idleEnergy = idleTime.total_seconds() * battery_voltage * config[KEY_EVENT_AS][KEY_EVENT_QUIESCENT]
    totalEnergy = idleEnergy + pictureEnergy + pulseEnergy + servoEnergy    

    return { KEY_IDLE_TIME      : idleTime,
             KEY_IDLE_ENERGY    : idleEnergy,
             KEY_PICTURE_TIME   : pictureTime,
             KEY_PICTURE_ENERGY : pictureEnergy,
             KEY_PULSE_TIME     : pulseTime,
             KEY_PULSE_ENERGY   : pulseEnergy,
             KEY_SERVO_TIME     : servoTime,
             KEY_SERVO_ENERGY   : servoEnergy,
             KEY_PERIOD_TIME    : PeriodTime,
             KEY_TOTAL_ENERGY   : totalEnergy} 

def reportOnProgram(config, programLines, LeapYear, site):
    """
    Generate program/power report on a program file
    """
    reportLines = []
    completeList = {}
    exposeWriteDelay = 30
    eventCount = 0

    # these are the defaults unless over-written by template commands
    exposeWriteDelay = 30
    pulsePeriod = 2
    turnOnDelay = 5

    if LeapYear:
        year = 2000
        daysInYear = 366
    else:
        year = 2001
        daysInYear = 365

    for line in programLines:
        if ((line == "") or
            (line[0] == '#')): 
            pass
        else:
            if (len(line) > 0):
                fields = line.split(' ', 6)

                # process each line
                cmd = fields[0]

                if (str.lower(cmd) == 'event'):
                    eventCount += 1

                    # make sure fields are all double '**'
                    for i in range(2, 5): 
                        if (fields[i] == '*'): 
                            fields[i] = '**'

                    #action = fields[1]
                    MM = fields[2]
                    DD = fields[3]
                    HH = fields[4]
                    mm = fields[5]
                    #run_cmd = fields[6]

                    lineList = wc_list(wc = MM+DD+HH+mm, 
                                       LeapYear = LeapYear)
                    if not lineList == None:
                        completeList[line] = lineList
                    else:
                        sys.stderr.write("Error making linelist for '%s'" % line)
                        return None

                elif (str.lower(cmd) == KEY_CONFIG):
                    # get expose and write delay from config parameter
                    config_par = fields[1]
                    config_val = float(fields[2])

                    if (config_par == '2'):
                        exposeWriteDelay = config_val
                    elif (config_par == 'h'):
                        pulsePeriod = config_val
                    elif (config_par == '1'):
                        turnOnDelay = config_val

                else:
                    # ignore everything else
                    pass

    # Dump the program file header
    reportLines.append("#################################################")
    reportLines.append("# Nest Camera Program Generator Report File")
    for line in programLines:
        if (line == ""):
            break
        elif (line[0] == '#'):
            reportLines.append(line) 

    reportLines.append("# Program file contains {} events".format(eventCount))

    ####################################
    # Output all events for each line
    total_events = 0
    sortList = []
    key_sorted = sorted(completeList.keys())

    eventsByType = []
    for key in key_sorted:
        eventsByType.append("[{}]".format(key))
        Lines = completeList[key]
        events_for_line = 0
        for line in Lines:
            eventsByType.append("   {}/{} {}:{}".format(line[0:2], line[2:4], line[4:6], line[6:8]))
            events_for_line += 1
            cmd = key.split(' ', 6)[6]
            sortList.append("{},{}".format(line, cmd))
        eventsByType.append("   sub-Total Events = {}".format(events_for_line))
        total_events += events_for_line
    eventsByType.append("Total Events = {}".format(total_events))

    # built a big list, with the command on the same line - so we can sort
    # by date/time and then addup the power consumption of each command over time
    # 'MMDDHHMM,"command"'
    sortList = sorted(sortList)

    # summarise by event type
    eventCounts = {}
    for line in sortList:
        (_, cmd) = line.split(',')
        if not cmd in eventCounts:
            eventCounts[cmd] = 1
        else:
            eventCounts[cmd] = eventCounts[cmd] + 1

    # output summary of event types over the year    
    reportLines.append("# Summary count of events for 1 year")    
    for event in eventCounts.keys():
        reportLines.append(" {} = {} ".format(event, eventCounts[event]))

    battery_model = config[KEY_SITES][site][KEY_BATTERY_MODEL]
    battery_voltage = config[KEY_BATTERY_MODELS][battery_model][KEY_VOLTAGE]
    battery_Ah_nominal = config[KEY_BATTERY_MODELS][battery_model][KEY_AH_NOMINAL]
    battery_energy_nominal = battery_voltage * battery_Ah_nominal * 3600
    battery_Ah_neg35C = config[KEY_BATTERY_MODELS][battery_model][KEY_AH_NEG35C]
    battery_energy_neg35C = battery_voltage * battery_Ah_neg35C * 3600

    # Calculate energy over 1 Year
    dtStart = datetime.datetime(year = year,
                                month = 1,
                                day = 1)
    dtEnd = dtStart + datetime.timedelta(days = daysInYear)       
    results = energy_over_period(config = config,
                                 event_list = sortList,
                                 exposeWriteDelay = exposeWriteDelay,
                                 pulsePeriod = pulsePeriod,
                                 turnOnDelay = turnOnDelay,
                                 battery_voltage = battery_voltage,
                                 dtStart = datetime.datetime(year = datetime.MINYEAR,
                                                             month = 1,
                                                             day = 1),
                                 dtEnd = datetime.datetime(year = datetime.MINYEAR+1,
                                                           month = 1,
                                                           day = 1) - datetime.timedelta(microseconds = 1),
                                 LeapYear = LeapYear)

    reportLines.append("")
    reportLines.append("# Summary of times used by events - seconds (Days HH:MM:SS)")
    reportLines.append("Start        = %s" % dtStart.isoformat())
    reportLines.append("End          = %s" % dtEnd.isoformat())
    reportLines.append("Days         = %d" % int((dtEnd - dtStart).total_seconds()/(24*3600)))    
    reportLines.append("Picture      = %d seconds (%s) (%0.1f %% of total)" % (results[KEY_PICTURE_TIME].total_seconds(), 
                                                                               str(results[KEY_PICTURE_TIME]), 
                                                                               100*(results[KEY_PICTURE_TIME].total_seconds() / 
                                                                                    results[KEY_PERIOD_TIME].total_seconds())))
    reportLines.append("Camera Pulse = %d seconds (%s) (%0.1f %% of total)" % (results[KEY_PULSE_TIME].total_seconds(), 
                                                                               str(results[KEY_PULSE_TIME]), 
                                                                               100*(results[KEY_PULSE_TIME].total_seconds() / 
                                                                                    results[KEY_PERIOD_TIME].total_seconds())))
    reportLines.append("Servo settle = %d seconds (%s) (%0.1f %% of total)" % (results[KEY_SERVO_TIME].total_seconds(), 
                                                                               str(results[KEY_SERVO_TIME]), 
                                                                               100*(results[KEY_SERVO_TIME].total_seconds() / 
                                                                                    results[KEY_PERIOD_TIME].total_seconds())))
    reportLines.append("Idle time    = %d seconds (%s) (%0.1f %% of total)" % (results[KEY_IDLE_TIME].total_seconds(), 
                                                                               str(results[KEY_IDLE_TIME]), 
                                                                               100*(results[KEY_IDLE_TIME].total_seconds() / 
                                                                                    results[KEY_PERIOD_TIME].total_seconds())))

    reportLines.append("")
    reportLines.append("# Summary of energy used by events")
    reportLines.append("Idle         = %0.2f J (%0.1f %%) " % (results[KEY_IDLE_ENERGY], 
                                                               100*results[KEY_IDLE_ENERGY]/results[KEY_TOTAL_ENERGY]))
    reportLines.append("Pictures     = %0.2f J (%0.1f %%) " % (results[KEY_PICTURE_ENERGY], 
                                                               100*results[KEY_PICTURE_ENERGY]/results[KEY_TOTAL_ENERGY]))
    reportLines.append("Camera Pulse = %0.2f J (%0.1f %%) " % (results[KEY_PULSE_ENERGY], 
                                                               100*results[KEY_PULSE_ENERGY]/results[KEY_TOTAL_ENERGY]))
    reportLines.append("Servo settle = %0.2f J (%0.1f %%) " % (results[KEY_SERVO_ENERGY], 
                                                               100*results[KEY_SERVO_ENERGY]/results[KEY_TOTAL_ENERGY]))
    reportLines.append("Year total   = %0.2f J " % (results[KEY_TOTAL_ENERGY]))
    reportLines.append("Year total compared to total available battery energy :")
    reportLines.append("     nominal = %0.1f %%" % (100*results[KEY_TOTAL_ENERGY] / battery_energy_nominal))
    reportLines.append("     neg35C  = %0.1f %%" % (100*results[KEY_TOTAL_ENERGY] / battery_energy_neg35C))

    reportLines.append("")
    reportLines.append("# Summary of available battery energy")
    reportLines.append("Battery model            : %s" % (battery_model))
    reportLines.append("Battery voltage          : %0.1f V" % (battery_voltage))
    reportLines.append("Battery Ah (nominal)     : %0.1f Ah" % (battery_Ah_nominal))
    reportLines.append("Battery Energy (nominal) : %0.1f J" % (battery_energy_nominal))
    reportLines.append("Battery Ah (neg35C)      : %0.1f Ah" % (battery_Ah_neg35C))
    reportLines.append("Battery Energy (neg35C)  : %0.1f J" % (battery_energy_neg35C))  

    reportLines.append("")
    reportLines.append("####################################################")
    reportLines.append("# Summary of power consumption by month")
    # Summary power by month
    for month in range (1, 13):
        dtStart = datetime.datetime(year = year,
                                    month = month,
                                    day = 1,
                                    hour = 0,
                                    minute = 0,
                                    second = 0)
        dtEnd = dtStart + datetime.timedelta(days = calendar.monthrange(year, month)[1]) - datetime.timedelta(microseconds = 1)  
        results = energy_over_period(config = config,
                                     event_list = sortList,
                                     exposeWriteDelay = exposeWriteDelay,
                                     pulsePeriod = pulsePeriod,
                                     turnOnDelay = turnOnDelay,
                                     battery_voltage = battery_voltage,
                                     dtStart = dtStart,
                                     dtEnd = dtEnd,
                                     LeapYear = LeapYear)

        reportLines.append("")
        reportLines.append("Month        = %s" % (calendar.month_name[month]))
        reportLines.append("Start        = %s" % dtStart.isoformat())
        reportLines.append("End          = %s" % dtEnd.isoformat())
        reportLines.append("Days         = %0.0f" % float((dtEnd - dtStart).total_seconds()/(24*3600)))
        reportLines.append("Idle         = %0.2f J (%0.1f %%) " % (results[KEY_IDLE_ENERGY], 
                                                                   100*results[KEY_IDLE_ENERGY]/results[KEY_TOTAL_ENERGY]))
        reportLines.append("Pictures     = %0.2f J (%0.1f %%) " % (results[KEY_PICTURE_ENERGY], 
                                                                   100*results[KEY_PICTURE_ENERGY]/results[KEY_TOTAL_ENERGY]))
        reportLines.append("Camera Pulse = %0.2f J (%0.1f %%) " % (results[KEY_PULSE_ENERGY], 
                                                                   100*results[KEY_PULSE_ENERGY]/results[KEY_TOTAL_ENERGY]))
        reportLines.append("Servo settle = %0.2f J (%0.1f %%) " % (results[KEY_SERVO_ENERGY], 
                                                                   100*results[KEY_SERVO_ENERGY]/results[KEY_TOTAL_ENERGY]))
        reportLines.append("Period total = %0.2f J " % (results[KEY_TOTAL_ENERGY]))
        reportLines.append("Period total compared to total available battery energy :")
        reportLines.append("  nominal    = %0.1f %%" % (100*results[KEY_TOTAL_ENERGY] / battery_energy_nominal))
        reportLines.append("  neg35C     = %0.1f %%" % (100*results[KEY_TOTAL_ENERGY] / battery_energy_neg35C))

    reportLines.append("")
    reportLines.append("# Analysis of runtime in winter months, (April (4) - September (9) inclusive) assuming no solar power:")
    dtStart = datetime.datetime(year = year,
                                month = 4,
                                day = 1)
    dtEnd = datetime.datetime(year = year,
                              month = 10,
                              day = 1) - datetime.timedelta(microseconds = 1)    
    results = energy_over_period(config = config,
                                 event_list = sortList,
                                 exposeWriteDelay = exposeWriteDelay,
                                 pulsePeriod = pulsePeriod,
                                 turnOnDelay = turnOnDelay,
                                 battery_voltage = battery_voltage,
                                 dtStart = dtStart,
                                 dtEnd = dtEnd,
                                 LeapYear = LeapYear)

    reportLines.append("")
    reportLines.append("Start        = %s" % dtStart.isoformat())
    reportLines.append("End          = %s" % dtEnd.isoformat())
    reportLines.append("Days         = %0.0f" % float((dtEnd - dtStart).total_seconds()/(24*3600)))
    reportLines.append("Idle         = %0.2f J (%0.1f %%) " % (results[KEY_IDLE_ENERGY], 
                                                               100*results[KEY_IDLE_ENERGY]/results[KEY_TOTAL_ENERGY]))
    reportLines.append("Pictures     = %0.2f J (%0.1f %%) " % (results[KEY_PICTURE_ENERGY], 
                                                               100*results[KEY_PICTURE_ENERGY]/results[KEY_TOTAL_ENERGY]))
    reportLines.append("Camera Pulse = %0.2f J (%0.1f %%) " % (results[KEY_PULSE_ENERGY], 
                                                               100*results[KEY_PULSE_ENERGY]/results[KEY_TOTAL_ENERGY]))
    reportLines.append("Servo settle = %0.2f J (%0.1f %%) " % (results[KEY_SERVO_ENERGY], 
                                                               100*results[KEY_SERVO_ENERGY]/results[KEY_TOTAL_ENERGY]))
    reportLines.append("Period total = %0.2f J " % (results[KEY_TOTAL_ENERGY]))
    reportLines.append("Period total compared to total available battery energy :")
    reportLines.append("  nominal    = %0.1f %%" % (100*results[KEY_TOTAL_ENERGY] / battery_energy_nominal))
    reportLines.append("  neg35C     = %0.1f %%" % (100*results[KEY_TOTAL_ENERGY] / battery_energy_neg35C))



    ######################################
    # Leave the lists until the end..
    reportLines.append("")
    reportLines.append("# List of all events, sorted by time")     

    for line in sortList:
        (wc, cmd) = line.split(',')
        reportLines.append("{}/{} {}:{}   {}".format(wc[0:2], wc[2:4], wc[4:6], wc[6:8], cmd))    
    reportLines.append("")

    reportLines.append("# List of all times for events")    
    for line in eventsByType:
        reportLines.append(line)
    reportLines.append("")

    if len(reportLines) > 0:
        return reportLines
    else:
        return None

##################################################################################
# Main program here
if __name__ == "__main__":

    # Parse command line arguments
    parser = argparse.ArgumentParser(description = "Generate nest camera programs from templates")

    parser.add_argument('-c', '--config',
                        dest='ConfigFilename',
                        action='store',
                        required=True,
                        type=str,
                        help="Site configuration file")
    parser.add_argument('-l', '--leapyear',
                        dest='LeapYear',
                        action='store_true',
                        required=False,
                        help='Include extra day for a leap year')

    args = parser.parse_args(sys.argv[1:])

    # open the sites config
    # test for config
    try:
        fConfig = open(args.ConfigFilename, 'r')
    except IOError as e:
        sys.stderr.write(e)
        sys.exit()

    # now load it
    try:
        config = json.load(fConfig)
    except ValueError as e:
        sys.stderr.write("Error loading site config file '{}'".format(args.ConfigFilename))
        sys.stderr.write(e)
        sys.exit()

    TemplateDir = config[KEY_GLOBAL_CONFIG][KEY_TEMPLATE_DIR]
    OutputDir = config[KEY_GLOBAL_CONFIG][KEY_OUTPUT_DIR]
    ReportDir = config[KEY_GLOBAL_CONFIG][KEY_REPORT_DIR]
    KnownCameraModels = config[KEY_CAMERA_MODELS]
    KnownSubjects = config[KEY_KNOWN_SUBJECTS]
    Sites = config[KEY_SITES]

    # Check subjects and camera models
    for site in Sites.keys():
        if Sites[site][KEY_ACTIVE]:
            if not (Sites[site][KEY_CAMERA_MODEL] in KnownCameraModels):
                sys.stderr.write("Error: Unknown camera model '{}' for site '{}'".format(Sites[site][KEY_CAMERA_MODEL], site))
                sys.exit()

            if not (Sites[site][KEY_SUBJECT] in KnownSubjects):
                sys.stderr.write("Error: Unknown subject '{}' for site '{}'".format(Sites[site][KEY_SUBJECT], site))
                sys.exit()

            if not (Sites[site][KEY_BATTERY_MODEL] in config[KEY_BATTERY_MODELS]):
                sys.stderr.write("Error: Unknown battery model '{}' for site '{}'".format(Sites[site][KEY_BATTERY_MODEL], site))
                sys.exit()

    #############################################################
    # load templates for known subjects
    templates = {}
    for subject in KnownSubjects:
        templateFileName = os.path.join(os.path.abspath(TemplateDir), subject+".txt")

        # test for template file
        try:
            fTemplate = open(templateFileName, 'r')
            templates[subject] = fTemplate.read()
        except IOError as e:
            sys.stderr.write("Error : Missing template file '{}' {}".format(templateFileName, e))
            sys.exit()

    #############################################################
    # make output & report directories
    if not os.path.exists(OutputDir):
        try:
            os.mkdir(OutputDir)
        except IOError as e:
            sys.stderr.write(e)
            sys.exit()

    if not os.path.exists(ReportDir):
        try:
            os.mkdir(ReportDir)
        except IOError as e:
            sys.stderr.write(e)
            sys.exit()    

    # print Leapyear status
    if args.LeapYear:
        print("Performing calculations & reports for a leap year")
    else:
        print("Performing calculations & reports for a non-leap year")

    #############################################################
    # process each site 
    for site in Sites.keys():
        if Sites[site][KEY_ACTIVE]:
            # Process template for this site
            solar_noon_shift = Sites[site][KEY_SOLAR_NOON_SHIFT]
            camera_model = Sites[site][KEY_CAMERA_MODEL]
            subject = Sites[site][KEY_SUBJECT]
            longitude = Sites[site][KEY_LONGITUDE]

            if subject in templates.keys():
                programLines = programFromTemplate(templates[subject], solar_noon_shift, site)
            else:
                sys.stderr.write("ERROR: Unknown subject '%s'. Check conf file, and known subject list" % subject)
                sys.exit()

            if not (programLines == None):
                today = datetime.datetime.today()
                programFileName = os.path.join(os.path.abspath(OutputDir), ("%s_%s.txt" % (site, subject)))

                serialString = '%s @%2.2f %s' % (site, solar_noon_shift, subject)

                # calculate actual solar noon based on longitude, in UTC
                SolarNoon = 12 - ( longitude / 15)
                ( SolarNoonMinutes, SolarNoonHours ) = math.modf(SolarNoon)
                SolarNoonMinutes *= 60

                ( SolarNoonShiftMins, SolarNoonShiftHrs ) =  math.modf(solar_noon_shift)
                SolarNoonShiftMins *= 60

                ( SolarNoonDeltaMins, SolarNoonDeltaHrs) = math.modf(SolarNoon - solar_noon_shift)
                SolarNoonDeltaMins *= 60

                # put comment information into file
                # programLines.insert(0, "# generated on %s by %s" % (str(today), getpass.getuser()))
                programLines.insert(1, "# Site name                     : %s" % (site))
                programLines.insert(2, "# Subject/template              : %s" % (subject))
                programLines.insert(4, "# Longitude                     : %3.3f" %  (longitude))
                programLines.insert(5, "# Solar noon occurs at          : %02d:%02d UTC" % (SolarNoonHours, SolarNoonMinutes))
                programLines.insert(6, "# Template shifted by           : %02d:%02d" % (SolarNoonShiftHrs, SolarNoonShiftMins))
                programLines.insert(7, "# Template shift delta          : %02dh %02dm" % (SolarNoonDeltaHrs, SolarNoonDeltaMins))
                programLines.insert(8, "# Leap year                     : %s" % (args.LeapYear))
                programLines.insert(9, "#")
                programLines.insert(10, "# Serial String Length = %d of 30 characters" % (len(serialString)))
                programLines.insert(11, 'serial "%s"' % (serialString))
                programLines.insert(12, "")

                # open output file - overwrite existing
                try:
                    fProgram = open(programFileName, 'w')
                except IOError as e:
                    sys.stderr.write("Error : unable to write to program file '{}' {}\n".format(programFileName, e))
                    sys.exit()

                for line in programLines:
                    fProgram.write("{}\n".format(line))

                print("Generated program file : {}".format(programFileName))
                fProgram.close()

                # Now make the report file
                reportFileLines = reportOnProgram(config = config, 
                                                  programLines = programLines, 
                                                  LeapYear = args.LeapYear,
                                                  site = site)

                if not (reportFileLines == None): 
                    reportFileName = os.path.join(os.path.abspath(ReportDir), ("{}_{}_report.txt".format(site, subject)))

                    try:
                        fReport = open(reportFileName, 'w')
                    except IOError as e:
                        sys.stderr.write("Error : unable to write to report file '{}' {}".format(reportFileName, e))
                        sys.exit()

                    for line in reportFileLines:
                        fReport.write("{}\n".format(line))

                    print("Generated report file : {}".format(reportFileName))
                    fReport.close()

                else:
                    sys.stderr.write("Error creating report for site '{}'\n".format(site))

