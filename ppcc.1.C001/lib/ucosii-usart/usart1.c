/*
 *	msp430 - The msp430 program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *      Copyright 2001, R O SoftWare
 *	Written by Peter Jansen <peter_jan@sts-cvs>
 *
 * This file contains functions used to to manipulate usarts.
 */

#define USART1_RX_INT_MODE

#include <ppcc.h>

#include <signal.h>
#include <limits.h>
#include <stdint.h>

#include <usart1.h>
#include <ucosii.h>

#if TARGET
# include <io.h>

# ifdef USART1_INT_MODE
#  ifndef USART1_TX_INT_MODE
#   define USART1_TX_INT_MODE
#  endif
#  ifndef USART1_RX_INT_MODE
#   define USART1_RX_INT_MODE
#  endif
# endif

#ifndef DEBUG
#define DEBUG 0
#endif

/* local vars */

#ifdef USART1_RX_INT_MODE
static uint8_t  usart1_rx_buffer[USART1_RX_BUFFER_SIZE];
static uint16_t usart1_rx_insert_idx, usart1_rx_extract_idx;
interrupt (UART1RX_VECTOR) wakeup usart1RcvIsr(void);
OS_EVENT *usart1RXSem;		/* UCOS - ECB */
static uint8_t usart1RXSemErr;
#endif
#ifdef USART1_TX_INT_MODE
static uint8_t  usart1_tx_buffer[USART1_TX_BUFFER_SIZE];
static uint16_t usart1_tx_insert_idx, usart1_tx_extract_idx;
static uint8_t  usart1_tx_running;
interrupt (UART1TX_VECTOR) usart1XmtIsr(void);
#endif


/******************************************************************************
 *
 * Function Name: usart1Init()
 *
 * Description:  
 *    This function initializes the USART for async mode
 *
 * Calling Sequence: 
 *    baudrate divisor - use USART1_BAUD_DIV0 macro in .h file
 *    baudrate modulation - use USART1_BAUD_MOD macro in .h file
 *    mode - see typical modes in .h file
 *
 * Returns:
 *    void
 *
 * NOTE: usart1Init(USART1_BAUD_DIV(9600), USART1_BAUD_MOD(9600), USART_8N1);
 *
 *****************************************************************************/
void usart1Init(uint16_t baudDiv, uint8_t baudMod, uint8_t mode)
{
    // enable USART1 module
    ME2 |= (UTXE1 | URXE1);
    
    // set Port 3 pins for USART1
    P3SEL |= (BIT6 | BIT7);
    
    // reset the USART
    U1CTL = SWRST;

    // Set the baudrate/character parameters
    usart1Baud(baudDiv, baudMod, mode);
    
    // init receiver control register
    U1RCTL = 0;
    
#ifdef USART1_TX_INT_MODE
    usart1_tx_extract_idx = usart1_tx_insert_idx = 0;
    usart1_tx_running = 0;

    /* Enable TX Interrupts */
    IE2 |= UTXIE1;    
#endif
    
#ifdef USART1_RX_INT_MODE
    if (usart1RXSem == (OS_EVENT *)NULL)
    {
	/* Create a semaphore for RX - initialize to 0 for blocking */
	usart1RXSem = OSSemCreate(0);
    
	// initialize data queues
	usart1_rx_extract_idx = usart1_rx_insert_idx = 0;
    }
    
    // enable receiver interrupts
    IE2 |= URXIE1;
#endif
}

/******************************************************************************
 *
 * Function Name: usart1Disable()
 *
 * Description:  
 *    This function disables USART hardware, turns off the interrupts
 *    and deletes the ucosii semaphore.
 * 
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart1Disable(void)
{
    INT8U i8Err;

#ifdef USART1_RX_INT_MODE
    /* disable USART1 RX interrupts */
    IE2 &= ~URXIE1;
#endif
    
#ifdef USART1_TX_INT_MODE
    /* disable USART1 TX interrupts */
    IE2 &= ~UTXIE1;                         
#endif
    
    /* disable USART1 module */
    ME2 &= ~(UTXE1 | URXE1);
    uiSleep &= ~SLEEP_CHAR_RX1;
    
#ifdef USART1_RX_INT_MODE
    /* Delete the semaphore - silently swallow i8Err */
    if (usart1RXSem != (OS_EVENT *)NULL)
	usart1RXSem = OSSemDel(usart1RXSem, OS_DEL_ALWAYS, &i8Err);
#endif
}

/******************************************************************************
 *
 * Function Name: usart1Baud()
 *
 * Description:  
 *    This function sets/changes the baud rate/character parameters.
 *
 * Calling Sequence: 
 *    baudrate divisor - use USART1_BAUD_DIV0 macro
 *    baudrate modulation - use USART1_BAUD_MOD macro
 *    mode - see typical modes (above)
 *
 * Returns:
 *    void
 *
 * NOTE: usart1Baud(USART1_BAUD_DIV(9600), USART1_BAUD_MOD(9600), USART_8N1);
 *
 *****************************************************************************/
void usart1Baud(uint16_t baudDiv, uint8_t baudMod, uint8_t mode)
{
    // set the number of characters and other
    // user specified operating parameters
    U1CTL = mode;
    
    // select the baudrate generator clock
    U1TCTL = USART1_BRSEL;
    
    // load the modulation & baudrate divisor registers
    U1MCTL = baudMod;
    U1BR1 = (uint8_t)(baudDiv >> 8);
    U1BR0 = (uint8_t)(baudDiv >> 0);    
}

/******************************************************************************
 *
 * Function Name: usart1Putch()
 *
 * Description:  
 *    This function puts a character into the USART output queue for
 *    transmission.
 *
 * Calling Sequence: 
 *    character to be transmitted
 *
 * Returns:
 *    ch on success, -1 on error (queue full)
 *
 *****************************************************************************/
int usart1Putch(char ch)
{
#ifdef USART1_TX_INT_MODE
    uint16_t temp;
    
    temp = (usart1_tx_insert_idx + 1) % USART1_TX_BUFFER_SIZE;
    
    if (temp == usart1_tx_extract_idx)
	return -1;                          // no room
    
    IE2 &= ~UTXIE1 ;                      // disable TX interrupts
    
    // check if in process of sending data
    if (usart1_tx_running)
    {
	// add to queue
	usart1_tx_buffer[usart1_tx_insert_idx] = (uint8_t)ch;
	usart1_tx_insert_idx = temp;
    }
    else
    {
	// set running flag and write to output register
	usart1_tx_running = 1;
	TXBUF1 = ch;
    }
    
    IE2 |= UTXIE1;                        // enable TX interrupts
#else
    /* Check that USART is enabled otherwise deadlock will occur */
    if ((ME2 & UTXE1) != UTXE1)
	return -1;
    
    while (!(IFG2 & UTXIFG1))             // wait for TX buffer to empty
	continue;                           // also either WDOG() or swap()
    
    U1TXBUF = ch;
    while (!(U1TCTL & TXEPT));
    
#endif
    return (uint8_t)ch;
}

/******************************************************************************
 *
 * Function Name: usart1TxEmpty()
 *
 * Description:
 *    This function returns the status of the USART transmit data
 *    registers.
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    TRUE - if both the tx holding & shift registers are empty
 *    FALSE - either the tx holding or shift register is not empty
 *
 *****************************************************************************/
int usart1TxEmpty(void)
{
    return (U1TCTL & TXEPT) == TXEPT;
}

/******************************************************************************
 *
 * Function Name: usart1TxFlush()
 *
 * Description:  
 *    This function removes all characters from the USART transmit queue
 *    (without transmitting them).
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart1TxFlush(void)
{
#ifdef USART1_TX_INT_MODE
    /* "Empty" the transmit buffer. */
    IE2 &= ~UTXIE1;                       // disable TX interrupts
    usart1_tx_insert_idx = usart1_tx_extract_idx = 0;
    IE2 |= UTXIE1;                        // enable TX interrupts
#endif
}

/******************************************************************************
 *
 * Function Name: usart1RxFlush()
 *
 * Description:  
 *    This function removes all characters from the USART receive queue
 *    and clears all semaphores.
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart1RxFlush(void)
{
#ifdef USART1_RX_INT_MODE
    volatile int8_t i8Dummy;
    
    IE2 &= ~URXIE1;                       // disable RX interrupts
    
    /* Clear all set semaphores */
    while (OSSemAccept(usart1RXSem));

    /* Reset buffers */
    usart1_rx_insert_idx = usart1_rx_extract_idx = 0;
    
    /* Clear receive errors with a dummy read */
    if ((U1RCTL & RXERR) == RXERR)
	i8Dummy = U1RXBUF;
    
    IE2 |= URXIE1;                        // enable RX interrupts
#endif
}

/******************************************************************************
 *
 * Function Name: usart1Getch()
 *
 * Description:  
 *    This function gets a character from the USART receive queue
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    character on success, -1 if no character is available
 *
 *****************************************************************************/
int usart1Getch(void)
{
    return usart1GetchTimeout(0);
}

/******************************************************************************
 *
 * Function Name: usart1GetchTimeout()
 *
 * Description:  
 *    This function gets a character from the USART receive queue
 *
 * Calling Sequence: 
 *    iTimeout = timeout in ticks to wait for received character
 *
 * Returns:
 *    character on success, -1 if no character is available
 *
 *****************************************************************************/
int usart1GetchTimeout(int iTimeout)
{
#ifdef USART1_RX_INT_MODE
    uint8_t ch;
    
    /* OSSemPend()  - wait for the ISR to POST */
    OSSemPend(usart1RXSem, iTimeout, &usart1RXSemErr);
    
    if (usart1_rx_insert_idx == usart1_rx_extract_idx) // check if character is available
	return -1;
    
    ch = usart1_rx_buffer[usart1_rx_extract_idx++]; // get character, bump pointer
    usart1_rx_extract_idx %= USART1_RX_BUFFER_SIZE; // limit the pointer
    return ch;
#else
    if (U1RCTL & RXERR)                   // check for errors
	U1RCTL &= ~(FE + PE + OE + BRK + RXERR); // clear error flags
    else if (IFG2 & URXIFG1)              // check if character is available
	return U1RXBUF;                      // return character
    
    return -1;
#endif
}

#ifdef USART1_RX_INT_MODE
/******************************************************************************
 *
 * Function Name: usart1RcvIsr(void)
 *
 * Description:  
 *    usart1 receive isr. Wakeup attribute makes gcc clear the SCG0,1 OSCOFF
 *    and CPUOFF bits in the SR just before a reti, thus the CPU stays out of 
 *    sleep mode with clocks on so the character can be received.
 *
 *    If RX Edge detection is enabled, it is automatically handled, if not
 *    then the code is not used. 
 * 
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/

interrupt (UART1RX_VECTOR) wakeup usart1RcvIsr(void)
{
    uint16_t temp;
    uint8_t ch;
    uint8_t err;

    /* Test to see if this interrupts was an edge detect or a
     * received character */
    if (((IFG2 & URXIFG1) == 0) && (U1TCTL & URXSE) != 0)
    {
	/* disable edge detection to clear interrupt, also stop sleeping
         * until start edge detection is enabled again */
	usart1DisableRXSE();
	uiSleep |= SLEEP_CHAR_RX1;
    }
    else /* received character */
    {
	/* Reading U1RXBUF clears the interrupt */
        err = U1RCTL;
        ch = U1RXBUF;

	/* Increment OSIntNesting to stop mid-ISR context switches */
	if (OSRunning == TRUE)
	    if (OSIntNesting < 255)
		OSIntNesting++;

	// check status register for receive errors
	if (err & RXERR)
	{
	    instrumentErrorCode |= IEC_USART1_ERROR;
	}
	else
	{
	    temp = (usart1_rx_insert_idx + 1) % USART1_RX_BUFFER_SIZE;
	    usart1_rx_buffer[usart1_rx_insert_idx] = ch;
	    
	    if (temp != usart1_rx_extract_idx)
		usart1_rx_insert_idx = temp;
	    
            if (ch == '\n')
            {
                /* Enable start edge detection here, this makes it synchronous
                 * with the incoming characters so as not to enable it part 
		 * the way through an incoming character.
                 * After enabling start edge detection its safe to go back
                 * to sleep (and not before).
                 */
                usart1EnableRXSE();
                uiSleep &= ~SLEEP_CHAR_RX1;
            }

	    /* Notify usart1Getch */
	    OSSemPost(usart1RXSem);
	}	
	
	/* Decrement OSIntNesting after servicing ISR
         * This prevents OSSemPost (above) from swapping tasks
         */
	if (OSIntNesting > 0)
	    OSIntNesting--;
    }

}
#endif

#ifdef USART1_TX_INT_MODE
/******************************************************************************
 *
 * Function Name: usart1XmtIsr(void)
 *
 * Description:  
 *    usart1 transmit isr
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
interrupt (UART1TX_VECTOR) usart1XmtIsr(void)
{
    if (usart1_tx_insert_idx != usart1_tx_extract_idx)
    {
	U1TXBUF = usart1_tx_buffer[usart1_tx_extract_idx++];
	usart1_tx_extract_idx %= USART1_TX_BUFFER_SIZE;
    }
    else
	usart1_tx_running = 0;              // clear running flag
}
#endif
#endif /* TARGET */
