/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *	Written by chris_bou <chris_bou@sts-cvs>
 *
 * This file contains functions used to to manipulate ucosii-usarts.
 */

#define USART0_RX_INT_MODE

#include <ppcc.h>

#include <signal.h>
#include <limits.h>
#include <stdint.h>

#include <usart0.h>
#include <ucosii.h>

#if TARGET
# include <io.h>

# ifdef USART0_INT_MODE
#  ifndef USART0_TX_INT_MODE
#   define USART0_TX_INT_MODE
#  endif
#  ifndef USART0_RX_INT_MODE
#   define USART0_RX_INT_MODE
#  endif
# endif

/* local vars */

#ifdef USART0_RX_INT_MODE
static uint8_t  usart0_rx_buffer[USART0_RX_BUFFER_SIZE];
static uint16_t usart0_rx_insert_idx, usart0_rx_extract_idx;
interrupt (UART0RX_VECTOR) wakeup usart0RcvIsr(void);
OS_EVENT *usart0RXSem;		/* UCOS - ECB */
static uint8_t usart0RXSemErr;
#endif
#ifdef USART0_TX_INT_MODE
static uint8_t  usart0_tx_buffer[USART0_TX_BUFFER_SIZE];
static uint16_t usart0_tx_insert_idx, usart0_tx_extract_idx;
static uint8_t  usart0_tx_running;
interrupt (UART0TX_VECTOR) usart0XmtIsr(void);
#endif


/******************************************************************************
 *
 * Function Name: usart0Init()
 *
 * Description:  
 *    This function initializes the USART for async mode
 *
 * Calling Sequence: 
 *    baudrate divisor - use USART0_BAUD_DIV0 macro in .h file
 *    baudrate modulation - use USART0_BAUD_MOD macro in .h file
 *    mode - see typical modes in .h file
 *
 * Returns:
 *    void
 *
 * NOTE: usart0Init(USART0_BAUD_DIV(9600), USART0_BAUD_MOD(9600), USART_8N1);
 *
 *****************************************************************************/
void usart0Init(uint16_t baudDiv, uint8_t baudMod, uint8_t mode)
{
    // enable USART0 module
    ME1 |= (UTXE0 | URXE0);

    // set Port 3 pins for USART0
    P3SEL |= (BIT4 | BIT5);

    // reset the USART
    U0CTL = SWRST;

    // Set the baud rate/character parameters..
    usart0Baud(baudDiv, baudMod, mode);

    // init receiver contol register
    U0RCTL &= ~(FE | PE | OE | BRK | URXEIE | URXWIE | RXWAKE | RXERR);

#ifdef USART0_TX_INT_MODE
    usart0_tx_extract_idx = usart0_tx_insert_idx = 0;
    usart0_tx_running = 0;

    /* enable transmit interrupts */
    IE1 |= UTXIE0;                       // enable TX interrupts    
#endif

#ifdef USART0_RX_INT_MODE
    /* only create the semaphore if it does not already exist 
     * and only init the data queues 
     */
    if (usart0RXSem == (OS_EVENT *)NULL)
    {
	/* Create a semaphore for RX - initialize to 0 for blocking */
	usart0RXSem = OSSemCreate(0);

	// initialize data queues
	usart0_rx_extract_idx = usart0_rx_insert_idx = 0;
    }
	
    // enable receiver interrupts
    IE1 |= URXIE0;
#endif
}

/******************************************************************************
 *
 * Function Name: usart0Disable()
 *
 * Description:  
 *    This function disables USART hardware, turns off the interrupts
 *    and deletes the ucosii semaphore.
 * 
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart0Disable(void)
{
    INT8U i8Err;

#ifdef USART0_RX_INT_MODE
    /* disable USART0 RX interrupts */
    IE1 &= ~URXIE0;
#endif
    
#ifdef USART0_TX_INT_MODE
    /* disable USART0 TX interrupts */
    IE1 &= ~UTXIE0;                         
#endif
    
    /* disable USART0 module */
    ME1 &= ~(UTXE0 | URXE0);

    usart0DisableRXSE();
    uiSleep &= ~SLEEP_CHAR_RX0;
    
#ifdef USART0_RX_INT_MODE
    /* Delete the semaphore - silently swallow i8Err */
    if (usart0RXSem != (OS_EVENT *)NULL)
	usart0RXSem = OSSemDel(usart0RXSem, OS_DEL_ALWAYS, &i8Err);
#endif
}

/******************************************************************************
 *
 * Function Name: usart0Baud()
 *
 * Description:  
 *    This function changes the baud rate/character parameters
 *
 * Calling Sequence: 
 *    baudrate divisor - use USART0_BAUD_DIV0 macro in .h file
 *    baudrate modulation - use USART0_BAUD_MOD macro in .h file
 *    mode - see typical modes in .h file
 *
 * Returns:
 *    void
 *
 * NOTE: usart0Init(USART0_BAUD_DIV(9600), USART0_BAUD_MOD(9600), USART_8N1);
 *
 *****************************************************************************/
void usart0Baud(uint16_t baudDiv, uint8_t baudMod, uint8_t mode)
{
    // set the number of characters and other
    // user specified operating parameters
    U0CTL = mode;

    // select the baudrate generator clock
    U0TCTL = USART0_BRSEL;

    // load the modulation & baudrate divisor registers
    U0MCTL = baudMod;
    U0BR1 = (uint8_t)(baudDiv >> 8);
    U0BR0 = (uint8_t)(baudDiv >> 0);    
}

/******************************************************************************
 *
 * Function Name: usart0Putch()
 *
 * Description:  
 *    This function puts a character into the USART output queue for
 *    transmission.
 *
 * Calling Sequence: 
 *    character to be transmitted
 *
 * Returns:
 *    ch on success, -1 on error (queue full, or transmitter not enabled)
 *
 *****************************************************************************/
int usart0Putch(char ch)
{
#ifdef USART0_TX_INT_MODE
    uint16_t temp;
    
    temp = (usart0_tx_insert_idx + 1) % USART0_TX_BUFFER_SIZE;
    
    if (temp == usart0_tx_extract_idx)
        return -1;                          // no room
    
    IE1 &= ~UTXIE0;                         // disable TX interrupts
    
    // check if in process of sending data
    if (usart0_tx_running)
    {
        // add to queue
        usart0_tx_buffer[usart0_tx_insert_idx] = (uint8_t)ch;
        usart0_tx_insert_idx = temp;
    }
    else
    {
        // set running flag and write to output register
        usart0_tx_running = 1;
        U0TXBUF = ch;
    }
    
    IE1 |= UTXIE0;                       // enable TX interrupts
#else
    /* Check that USart is enabled otherwise deadlock will occur */
    if ((ME1 & UTXE0) != UTXE0)
	return -1;
    
    while (!(IFG1 & UTXIFG0))            // wait for TX buffer to empty
        continue;                           // also either WDOG() or swap()
    
    U0TXBUF = ch;
    while (!(U0TCTL & TXEPT));
    
#endif
    return (uint8_t)ch;
}

/******************************************************************************
 *
 * Function Name: usart0TxEmpty()
 *
 * Description:
 *    This function returns the status of the USART transmit data
 *    registers.
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    TRUE - if both the tx holding & shift registers are empty
 *    FALSE - either the tx holding or shift register is not empty
 *
 *****************************************************************************/
int usart0TxEmpty(void)
{
    return (U0TCTL & TXEPT) == TXEPT;
}

/******************************************************************************
 *
 * Function Name: usart0TxFlush()
 *
 * Description:  
 *    This function removes all characters from the USART transmit queue
 *    (without transmitting them).
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart0TxFlush(void)
{
#ifdef USART0_TX_INT_MODE
    /* "Empty" the transmit buffer. */
    IE1 &= ~UTXIE0;                      // disable TX interrupts
    usart0_tx_insert_idx = usart0_tx_extract_idx = 0;
    IE1 |= UTXIE0;                       // enable TX interrupts
#endif
}

/******************************************************************************
 *
 * Function Name: usart0RxFlush()
 *
 * Description:  
 *    This function removes all characters from the USART receive queue
 *    and  clears all outstanding semaphores.
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart0RxFlush(void)
{
#ifdef USART0_RX_INT_MODE
    volatile int8_t i8Dummy;
    
    IE1 &= ~URXIE0;                       // disable RX interrupts
    
    /* Clear all set semaphores */
    while (OSSemAccept(usart0RXSem));

    /* Reset buffers */
    usart0_rx_insert_idx = usart0_rx_extract_idx = 0;

    /* Clear receive errors with a dummy read */
    if ((U0RCTL & RXERR) == RXERR)
	i8Dummy = U0RXBUF;
    
    IE1 |= URXIE0;                       // enable RX interrupts
#endif
}

/******************************************************************************
 *
 * Function Name: usart0Getch()
 *
 * Description:  
 *    This function gets a character from the USART receive queue
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    character on success, -1 if no character is available
 *
 *****************************************************************************/
int usart0Getch(void)
{
    return usart0GetchTimeout(0);
}

/******************************************************************************
 *
 * Function Name: usart0GetchTimeout()
 *
 * Description:  
 *    This function gets a character from the USART receive queue
 *
 * Calling Sequence: 
 *    INT16U - timeout in ticks to wait for character to arrive
 *
 * Returns:
 *    character on success, -1 if no character is available or semaphore broken
 *
 *****************************************************************************/
int usart0GetchTimeout(unsigned int uiTimeout)
{
#ifdef USART0_RX_INT_MODE
    uint8_t ch;

    /* OSSemPend()  - wait for the ISR to POST */
    OSSemPend(usart0RXSem, uiTimeout, &usart0RXSemErr);
    
    // check if character is available
    if (usart0_rx_insert_idx == usart0_rx_extract_idx) 
	return -1;

    ch = usart0_rx_buffer[usart0_rx_extract_idx++]; // get character, bump pointer
    usart0_rx_extract_idx %= USART0_RX_BUFFER_SIZE; // limit the pointer
    return ch;
#else
    if (U0RCTL & RXERR)                           // check for errors
        U0RCTL &= ~(FE | PE | OE | BRK | RXERR);  // clear error flags
    else if (IFG1 & URXIFG0)                      // check if character is available
        return U0RXBUF;                           // return character
	
    return -1;
#endif
}


#ifdef USART0_RX_INT_MODE
/******************************************************************************
 *
 * Function Name: usart0RcvIsr(void)
 *
 * Description:  
 *    usart0 receive isr.  Wakeup attribute makes gcc clear the SCG0,1 OSCOFF
 *    and CPUOFF bits in the SR just before a reti, thus the CPU stays out of 
 *    sleep mode with clocks on so the character can be received.
 * 
 *    If RX Edge detection is enabled, it is automatically handled, if not
 *    then the code is not used. 
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
interrupt (UART0RX_VECTOR) wakeup usart0RcvIsr(void)
{	
    uint16_t temp;
    uint8_t ch;
    uint8_t err;

    /* Test to see if this interrupts was an edge detect or a
     * received character */
    if (((IFG1 & URXIFG0) == 0) && ((U0TCTL & URXSE) != 0))
    {
	/* toggle edge detection to clear interrupt, also stop sleeping */
	usart0DisableRXSE();
	uiSleep |= SLEEP_CHAR_RX0;
    }
    else
    {
        /* Reading U0RXBUF clear the interrupt flag */
	err = U0RCTL;
	ch = U0RXBUF;
	
	/* Increment OSIntNesting to stop ISR context switches */
	if (OSRunning == TRUE)
	    if (OSIntNesting < 255)
		OSIntNesting++;
    
	// check status register for receive errors
	if (err & RXERR)
	{
	    instrumentErrorCode |= IEC_USART0_ERROR;
	} 
	else
	{
	    temp = (usart0_rx_insert_idx + 1) % USART0_RX_BUFFER_SIZE;
	    usart0_rx_buffer[usart0_rx_insert_idx] = ch;
	    
	    if (temp != usart0_rx_extract_idx)
		usart0_rx_insert_idx = temp;
	    
	    if (ch == '\n')
	    {
		/* Enable start edge detection here, this makes it synchronous
		 * with the incoming characters so as not to enable it 
		 * part the way through an incoming character.
		 * After enabling start edge detection its safe to go back
		 * to sleep (and not before).
		 */
		usart0EnableRXSE();
		uiSleep &= ~SLEEP_CHAR_RX0;
	    }
	    
	    /* Notify usart0Getch */
	    OSSemPost(usart0RXSem);
	}
    
	/* Decrement after servicing ISR */
	if (OSIntNesting > 0)
	    OSIntNesting--;
    }
}
#endif

#ifdef USART0_TX_INT_MODE
/******************************************************************************
 *
 * Function Name: usart0XmtIsr(void)
 *
 * Description:  
 *    usart0 transmit isr
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
interrupt (UART0TX_VECTOR) usart0XmtIsr(void)
{
    if (usart0_tx_insert_idx != usart0_tx_extract_idx)
    {
        U0TXBUF = usart0_tx_buffer[usart0_tx_extract_idx++];
        usart0_tx_extract_idx %= USART0_TX_BUFFER_SIZE;
    }
    else
        usart0_tx_running = 0;              // clear running flag
}
#endif
#endif /* TARGET */
