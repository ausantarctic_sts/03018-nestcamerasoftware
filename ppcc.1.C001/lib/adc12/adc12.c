/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by kym.newbery@aad.gov.au
 *
 * This file contains functions used to to manipulate adc12s.
 */

#ifdef TARGET
#include <io.h>
#endif

#include <ucosii.h>
#include <adc12.h>

#define ADC12SSEL_SMCLK (3<<3)

void adc12Init(void)
{
    /* I/O Port Pins must already have been configured */
}

void adc12PowerUp(int iADC12ClkDiv,
		  int iSampleHoldTime,
		  int iRefVoltage)
{
#ifdef HOST
    iADC12ClkDiv = iADC12ClkDiv;
    iSampleHoldTime = iSampleHoldTime;
    iRefVoltage = iRefVoltage;
#endif
#ifdef TARGET    

    ADC12CTL0 &= ~ENC;   /* ensure ENC is zero */

    /*
     * Configure Control register 0 
     *
     * Configure to:
     *    - Use external sample & hold time
     *    - Externally selected reference voltage
     *    - No interrupts
     */
    ADC12CTL0 = ((iSampleHoldTime & 0xF) << 8) | ADC12ON;

    /* select 2.5v ref if required, else 1v5 or VCC */
    if (iRefVoltage == REF_VOLTAGE_2V5)
	ADC12CTL0 |= REF2_5V;
    else
	ADC12CTL0 &= ~REF2_5V;
    
    /* Turn on the internal reference if we use 1v5 or 2v5 */
    if ((iRefVoltage == REF_VOLTAGE_2V5) ||
	(iRefVoltage == REF_VOLTAGE_1V5))
	ADC12CTL0 |= REFON;
    else
	ADC12CTL0 &= ~REFON;
        
    /*
     * Configure Control register 1
     */
    
    /* Configure to :
     *    - only use ADC12MEM0
     *    - Sample & hold source ADC12SC
     *    - external 3 bit divider select
     *    - ADC12 clock is ADC12OSC
     *    - Single Channel, single conversion
     */
    ADC12CTL1 = ((iADC12ClkDiv & 0x7) << 5) | SHP | ADC12SSEL_SMCLK;
    
    /*
     * Configure ADC12 Conversion memory control register 0
     */
    
    /* Based on the selected reference, set SREFx bits */
    if ((iRefVoltage == REF_VOLTAGE_2V5) ||
	(iRefVoltage == REF_VOLTAGE_1V5))
	ADC12MCTL0 = (1<<4);        /* for 1v5 or 2v5, VR+=VREF+ and VR-=AVSS */

    else if (iRefVoltage == REF_VOLTAGE_VEREF)
	ADC12MCTL0 = (2<<4);        /* for VEREF, VR+=VeREF+ and VR-=AVSS */ 
    else
	ADC12MCTL0 = 0;             /* for VCC, VR+=AVCC and VR-=AVSS */

    /*
     * delay 17ms for internal reference voltages to stabilise 
     */
    if ((iRefVoltage == REF_VOLTAGE_1V5) ||
	(iRefVoltage == REF_VOLTAGE_2V5))
	OSTimeDly(17/OS_TICK_PERIOD);
#endif
}

void adc12Shutdown(void)
{
#ifdef TARGET
    /* turn off reference & adc12 */
    ADC12CTL0 &= ~ENC;
    ADC12CTL0 &= ~(REFON | ADC12ON);
#endif
}

unsigned int adc12Read(int iChannel)
{
#ifdef TARGET    
    /* Configure the Input channel select */
    ADC12CTL0 &= ~ENC;
    ADC12MCTL0 = (ADC12MCTL0 & 0xF0) | (iChannel & 0xF);
    ADC12CTL0 |= ENC;
    
    /* Set ADC12SC to start conversion */
    ADC12CTL0 |= ADC12SC;
    
    /* Wait for end of conversion */
    while ((ADC12IFG & BIT0) == 0);

    return ADC12MEM0;
#endif
#ifdef HOST
    iChannel = iChannel;

    return 0;
#endif
}

unsigned int adc12ReadAverage(int iChannel, int iCount)
{    
#ifdef TARGET
    unsigned long ulTotal;
    int iSample;

    /* Configure the Input channel select */
    ADC12CTL0 &= ~ENC;
    ADC12MCTL0 = (ADC12MCTL0 & 0xF0) | (iChannel & 0xF);
    ADC12CTL0 |= ENC;
    
    /* Make multiple readings - and average */
    ulTotal = 0;
    for (iSample = 0; iSample < iCount; iSample++)
    {
	/* Set ADC12SC to start conversion */
	ADC12CTL0 |= ADC12SC;
    
	/* Wait for end of conversion */
	while ((ADC12IFG & BIT0) == 0);
	
	/* Add last value to total */
	ulTotal += ADC12MEM0;
    }
    
    return (unsigned int)(ulTotal / iCount);
#endif
#ifdef HOST
    iChannel = iChannel;
    iCount = iCount;
    
    return 0;
#endif
}
