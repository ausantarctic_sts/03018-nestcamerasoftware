/*
 */

#include <version.h>

const char FirmwareVersion[] = { VERSION };
const char FirmwareDate[] = {__TIME__, __DATE__};
