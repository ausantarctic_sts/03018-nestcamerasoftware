/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by kym.newbery@aad.gov.au
 *
 * This library implements and microsecond (us) delay using timerb. It has
 * the following requirements :
 * 
 *  - TimerB driven from SMCLK which is 1MHz
 *
 * It uses output compare CC2 register 
 * 
 */

#include <usdelay.h>

#ifdef TARGET
# include <io.h>
#endif

/*
 * Initialise timerb for the Sensor - 16 bit
 *
 */
void usDelayInit(void)
{
#ifdef TARGET
    /* Setup the timer b itself */
    TBCTL = TBCLR | ID_DIV1 | MC_CONT | TBSSEL_SMCLK | CNTL_0;
    TBCTL &= ~TBCLR;
    TBCTL |= MC_CONT; /* start in continuous mode */

    /* Setup CCR2 for usDelay. CCIFG is set when a compare match occurs */
    TBCCTL2 = 0;
#endif
}


/*
 * usdelay(unsigned int uiDelay)
 * 
 * Provides a blocking microsecond delay with timerb, up to a maximum of
 * 65535us (65ms) Timerb must be free-running. Uses capture register CC2 
 * and also assumes SMCLK = 1MHz
 * 
 */
void usDelay(unsigned int uiDelay)
{
#ifdef TARGET
    /* clear the CCIFG flag if already set */
    TBCCTL2 &= ~CCIFG;
    
    /* load the capture compare register */
    TBCCR2 = TBR + uiDelay;
    
    /* wait for the match */
    while ((TBCCTL2 & CCIFG) == 0);
#endif
}

