/*
 *	EFM - The "EFM" program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *	Written by Peter Jansen <peter.jansen@aad.gov.au>
 *
 * This file contains functions used to to manipulate the MSP430 AD converter
 */

#include <ad.h>

#ifdef TARGET
#include <io.h>
#endif

void adInit(void)
{
#ifdef TARGIT
    ADC12CTL0 = ADC12ON | REFON | REF2_5V | ADC12SHT_3 | SREF_1;
    ADC12CTL1 = 0;
    ADC12AE = (1 << 7) | (1 << 6) | (1 << 5);
#endif
}

unsigned int adRead(unsigned char channel)
{
#ifdef TARGET
    unsigned int d;
    
    ADC12CTL0 = ADC12CTL0 & ~ENC; /* Disable conversion */
    ADC12CTL1 = ((unsigned int)channel << 12); /* Select the channel */

    ADC12CTL0 = ADC12CTL0 | ENC; /* Enable the conversion */
    ADC12CTL0 = ADC12CTL0 | ADC12SC; /* Start the conversion */

    while (ADC12CTL0 & ADC12BUSY); /* Wait for the conversion */

    ADC12CTL0 = ADC12CTL0 & ~ENC; /* Disable conversion */

    d = ADC12MEM0;
    
    return d;
#endif
#ifdef HOST
    return channel;
#endif
}
