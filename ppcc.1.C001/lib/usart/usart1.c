/*
 *	msp430 - The msp430 program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *      Copyright 2001, R O SoftWare
 *	Written by Peter Jansen <peter_jan@sts-cvs>
 *
 * This file contains functions used to to manipulate usarts.
 */

#include <usart1.h>

#if TARGET
#include <io.h>
#include <signal.h>
#include <limits.h>
#include <sys/inttypes.h>

#ifdef USART1_INT_MODE
#ifndef USART1_TX_INT_MODE
#define USART1_TX_INT_MODE
#endif
#ifndef USART1_RX_INT_MODE
#define USART1_RX_INT_MODE
#endif
#endif

/* local vars */

#ifdef USART1_RX_INT_MODE
static uint8_t  usart1_rx_buffer[USART1_RX_BUFFER_SIZE];
static uint16_t usart1_rx_insert_idx, usart1_rx_extract_idx;
interrupt (UART0RX_VECTOR) usart1RcvIsr(void);
#endif
#ifdef USART1_TX_INT_MODE
static uint8_t  usart1_tx_buffer[USART1_TX_BUFFER_SIZE];
static uint16_t usart1_tx_insert_idx, usart1_tx_extract_idx;
static uint8_t  usart1_tx_running;
interrupt (UART0TX_VECTOR) usart1XmtIsr(void);
#endif


/******************************************************************************
 *
 * Function Name: usart1Init()
 *
 * Description:  
 *    This function initializes the USART for async mode
 *
 * Calling Sequence: 
 *    baudrate divisor - use USART1_BAUD_DIV0 macro in .h file
 *    baudrate modulation - use USART1_BAUD_MOD macro in .h file
 *    mode - see typical modes in .h file
 *
 * Returns:
 *    void
 *
 * NOTE: usart1Init(USART1_BAUD_DIV(9600), USART1_BAUD_MOD(9600), USART_8N1);
 *
 *****************************************************************************/
void usart1Init(uint16_t baudDiv, uint8_t baudMod, uint8_t mode)
{
  // enable USART1 module
  ME2 |= UTXE1 + URXE1;

  // set Port 3 pins for USART1
  P3SEL |= BV(6) + BV(7);

  // reset the USART
  UCTL1 = SWRST;

  // set the number of characters and other
  // user specified operating parameters
  UCTL1 = mode;

  // select the baudrate generator clock
  UTCTL1 = USART1_BRSEL;

  // load the modulation & baudrate divisor registers
  UMCTL1 = baudMod;
  UBR11 = (uint8_t)(baudDiv >> 8);
  UBR01 = (uint8_t)(baudDiv >> 0);

  // init receiver contol register
  URCTL1 = 0;

#ifdef USART1_TX_INT_MODE
  usart1_tx_extract_idx = usart1_tx_insert_idx = 0;
  usart1_tx_running = 0;
#endif

#ifdef USART1_RX_INT_MODE
  // initialize data queues
  usart1_rx_extract_idx = usart1_rx_insert_idx = 0;

  // enable receiver interrupts
  IE2 |= URXIE1;
#endif
}

/******************************************************************************
 *
 * Function Name: usart1Putch()
 *
 * Description:  
 *    This function puts a character into the USART output queue for
 *    transmission.
 *
 * Calling Sequence: 
 *    character to be transmitted
 *
 * Returns:
 *    ch on success, -1 on error (queue full)
 *
 *****************************************************************************/
int usart1Putch(char ch)
{
#ifdef USART1_TX_INT_MODE
  uint16_t temp;

  temp = (usart1_tx_insert_idx + 1) % USART1_TX_BUFFER_SIZE;

  if (temp == usart1_tx_extract_idx)
    return -1;                          // no room

  IE2 &= ~UTXIE1 ;                      // disable TX interrupts

  // check if in process of sending data
  if (usart1_tx_running)
    {
    // add to queue
    usart1_tx_buffer[usart1_tx_insert_idx] = (uint8_t)ch;
    usart1_tx_insert_idx = temp;
    }
  else
    {
    // set running flag and write to output register
    usart1_tx_running = 1;
    TXBUF1 = ch;
    }

  IE2 |= UTXIE1;                        // enable TX interrupts
#else
  while (!(IFG2 & UTXIFG1))             // wait for TX buffer to empty
    continue;                           // also either WDOG() or swap()

  TXBUF1 = ch;
#endif
  return (uint8_t)ch;
}

/******************************************************************************
 *
 * Function Name: usart1TxEmpty()
 *
 * Description:
 *    This function returns the status of the USART transmit data
 *    registers.
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    TRUE - if both the tx holding & shift registers are empty
 *    FALSE - either the tx holding or shift register is not empty
 *
 *****************************************************************************/
int usart1TxEmpty(void)
{
  return (UTCTL1 & TXEPT) == TXEPT;
}

/******************************************************************************
 *
 * Function Name: usart1TxFlush()
 *
 * Description:  
 *    This function removes all characters from the USART transmit queue
 *    (without transmitting them).
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart1TxFlush(void)
{
#ifdef USART1_TX_INT_MODE
  /* "Empty" the transmit buffer. */
  IE2 &= ~UTXIE1 ;                      // disable TX interrupts
  usart1_tx_insert_idx = usart1_tx_extract_idx = 0;
  IE2 |= UTXIE1;                        // enable TX interrupts
#endif
}

/******************************************************************************
 *
 * Function Name: usart1Getch()
 *
 * Description:  
 *    This function gets a character from the USART receive queue
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    character on success, -1 if no character is available
 *
 *****************************************************************************/
int usart1Getch(void)
{
#ifdef USART0_RX_INT_MODE
  uint8_t ch;

  if (usart1_rx_insert_idx == usart1_rx_extract_idx) // check if character is available
    return -1;

  ch = usart1_rx_buffer[usart1_rx_extract_idx++]; // get character, bump pointer
  usart1_rx_extract_idx %= USART1_RX_BUFFER_SIZE; // limit the pointer
  return ch;
#else
  int r;
  
  r = -1;

  if (URCTL1 & RXERR)                   // check for errors
  {
    URCTL1 &= ~(FE + PE + OE + BRK + RXERR); // clear error flags
  }
  else if (IFG2 & URXIFG1)              // check if character is available
  {
      r = RXBUF1;
  }

  return r;
#endif
}

#ifdef USART1_RX_INT_MODE
/******************************************************************************
 *
 * Function Name: usart1RcvIsr(void)
 *
 * Description:  
 *    usart1 receive isr
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
interrupt (UART1RX_VECTOR) usart1RcvIsr(void)
{
  uint16_t temp;
  volatile uint8_t dummy;

  // check status register for receive errors
  if (URCTL1 & RXERR)
    {
    // clear error flags by forcing a dummy read
    dummy = RXBUF1;
    return;
    }

  temp = (usart1_rx_insert_idx + 1) % USART1_RX_BUFFER_SIZE;
  usart1_rx_buffer[usart1_rx_insert_idx] = RXBUF1; // read the character

  if (temp != usart1_rx_extract_idx)
    usart1_rx_insert_idx = temp;
}
#endif

#ifdef USART1_TX_INT_MODE
/******************************************************************************
 *
 * Function Name: usart1XmtIsr(void)
 *
 * Description:  
 *    usart1 transmit isr
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
interrupt (UART1TX_VECTOR) usart1XmtIsr(void)
{
  if (usart1_tx_insert_idx != usart1_tx_extract_idx)
    {
    TXBUF1 = usart1_tx_buffer[usart1_tx_extract_idx++];
    usart1_tx_extract_idx %= USART1_TX_BUFFER_SIZE;
    }
  else
    usart1_tx_running = 0;              // clear running flag
}

#endif /* TARGET */

#endif
