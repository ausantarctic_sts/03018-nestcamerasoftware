/*
 *	msp430 - The msp430 program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *      Copyright 2001, R O SoftWare
 *	Written by Peter Jansen <peter_jan@sts-cvs>
 *
 * This file contains functions used to to manipulate usarts.
 */

#include <usart0.h>

#if TARGET
#include <io.h>
#include <signal.h>
#include <limits.h>
#include <sys/inttypes.h>

#ifdef USART0_INT_MODE
#ifndef USART0_TX_INT_MODE
#define USART0_TX_INT_MODE
#endif
#ifndef USART0_RX_INT_MODE
#define USART0_RX_INT_MODE
#endif
#endif

/* local vars */

#ifdef USART0_RX_INT_MODE
static uint8_t  usart0_rx_buffer[USART0_RX_BUFFER_SIZE];
static uint16_t usart0_rx_insert_idx, usart0_rx_extract_idx;
interrupt (UART0RX_VECTOR) usart0RcvIsr(void);
#endif
#ifdef USART0_TX_INT_MODE
static uint8_t  usart0_tx_buffer[USART0_TX_BUFFER_SIZE];
static uint16_t usart0_tx_insert_idx, usart0_tx_extract_idx;
static uint8_t  usart0_tx_running;
interrupt (UART0TX_VECTOR) usart0XmtIsr(void);
#endif


/******************************************************************************
 *
 * Function Name: usart0Init()
 *
 * Description:  
 *    This function initializes the USART for async mode
 *
 * Calling Sequence: 
 *    baudrate divisor - use USART0_BAUD_DIV0 macro in .h file
 *    baudrate modulation - use USART0_BAUD_MOD macro in .h file
 *    mode - see typical modes in .h file
 *
 * Returns:
 *    void
 *
 * NOTE: usart0Init(USART0_BAUD_DIV(9600), USART0_BAUD_MOD(9600), USART_8N1);
 *
 *****************************************************************************/
void usart0Init(uint16_t baudDiv, uint8_t baudMod, uint8_t mode)
{
  // enable USART0 module
  U0ME |= UTXE0 + URXE0;

  // set Port 3 pins for USART0
  P3SEL |= BV(4) + BV(5);

  // reset the USART
  UCTL0 = SWRST;

  // set the number of characters and other
  // user specified operating parameters
  UCTL0 = mode;

  // select the baudrate generator clock
  UTCTL0 = USART0_BRSEL;

  // load the modulation & baudrate divisor registers
  UMCTL0 = baudMod;
  UBR10 = (uint8_t)(baudDiv >> 8);
  UBR00 = (uint8_t)(baudDiv >> 0);

  // init receiver contol register
  URCTL0 = 0;

#ifdef USART0_TX_INT_MODE
  usart0_tx_extract_idx = usart0_tx_insert_idx = 0;
  usart0_tx_running = 0;
#endif

#ifdef USART0_RX_INT_MODE
  // initialize data queues
  usart0_rx_extract_idx = usart0_rx_insert_idx = 0;

  // enable receiver interrupts
  U0IE |= URXIE0;
#endif
}

/******************************************************************************
 *
 * Function Name: usart0Putch()
 *
 * Description:  
 *    This function puts a character into the USART output queue for
 *    transmission.
 *
 * Calling Sequence: 
 *    character to be transmitted
 *
 * Returns:
 *    ch on success, -1 on error (queue full)
 *
 *****************************************************************************/
int usart0Putch(char ch)
{
#ifdef USART0_TX_INT_MODE
  uint16_t temp;

  temp = (usart0_tx_insert_idx + 1) % USART0_TX_BUFFER_SIZE;

  if (temp == usart0_tx_extract_idx)
    return -1;                          // no room

  U0IE &= ~UTXIE0 ;                     // disable TX interrupts

  // check if in process of sending data
  if (usart0_tx_running)
    {
    // add to queue
    usart0_tx_buffer[usart0_tx_insert_idx] = (uint8_t)ch;
    usart0_tx_insert_idx = temp;
    }
  else
    {
    // set running flag and write to output register
    usart0_tx_running = 1;
    TXBUF0 = ch;
    }

  U0IE |= UTXIE0;                       // enable TX interrupts
#else
  while (!(U0IFG & UTXIFG0))            // wait for TX buffer to empty
    continue;                           // also either WDOG() or swap()

  TXBUF0 = ch;
#endif
  return (uint8_t)ch;
}

/******************************************************************************
 *
 * Function Name: usart0TxEmpty()
 *
 * Description:
 *    This function returns the status of the USART transmit data
 *    registers.
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    TRUE - if both the tx holding & shift registers are empty
 *    FALSE - either the tx holding or shift register is not empty
 *
 *****************************************************************************/
int usart0TxEmpty(void)
{
  return (UTCTL0 & TXEPT) == TXEPT;
}

/******************************************************************************
 *
 * Function Name: usart0TxFlush()
 *
 * Description:  
 *    This function removes all characters from the USART transmit queue
 *    (without transmitting them).
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart0TxFlush(void)
{
#ifdef USART0_TX_INT_MODE
  /* "Empty" the transmit buffer. */
  U0IE &= ~UTXIE0 ;                     // disable TX interrupts
  usart0_tx_insert_idx = usart0_tx_extract_idx = 0;
  U0IE |= UTXIE0;                       // enable TX interrupts
#endif
}

/******************************************************************************
 *
 * Function Name: usart0Getch()
 *
 * Description:  
 *    This function gets a character from the USART receive queue
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    character on success, -1 if no character is available
 *
 *****************************************************************************/
int usart0Getch(void)
{
#ifdef USART0_RX_INT_MODE
  uint8_t ch;

  if (usart0_rx_insert_idx == usart0_rx_extract_idx) // check if character is available
    return -1;

  ch = usart0_rx_buffer[usart0_rx_extract_idx++]; // get character, bump pointer
  usart0_rx_extract_idx %= USART0_RX_BUFFER_SIZE; // limit the pointer
  return ch;
#else
  if (URCTL0 & RXERR)                   // check for errors
    URCTL0 &= ~(FE + PE + OE + BRK + RXERR); // clear error flags
  else if (U0IFG & URXIFG0)             // check if character is available
  {
      return RXBUF0;                      // return character
  }

  return -1;
#endif
}

#ifdef USART0_RX_INT_MODE
/******************************************************************************
 *
 * Function Name: usart0RcvIsr(void)
 *
 * Description:  
 *    usart0 receive isr
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
interrupt (UART0RX_VECTOR) usart0RcvIsr(void)
{
  uint16_t temp;
  volatile uint8_t dummy;

  // check status register for receive errors
  if (URCTL0 & RXERR)
    {
    // clear error flags by forcing a dummy read
    dummy = RXBUF0;
    return;
    }

  temp = (usart0_rx_insert_idx + 1) % USART0_RX_BUFFER_SIZE;
  usart0_rx_buffer[usart0_rx_insert_idx] = RXBUF0; // read the character

  if (temp != usart0_rx_extract_idx)
    usart0_rx_insert_idx = temp;
}
#endif

#ifdef USART0_TX_INT_MODE
/******************************************************************************
 *
 * Function Name: usart0XmtIsr(void)
 *
 * Description:  
 *    usart0 transmit isr
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
interrupt (UART0TX_VECTOR) usart0XmtIsr(void)
{
  if (usart0_tx_insert_idx != usart0_tx_extract_idx)
    {
    TXBUF0 = usart0_tx_buffer[usart0_tx_extract_idx++];
    usart0_tx_extract_idx %= USART0_TX_BUFFER_SIZE;
    }
  else
    usart0_tx_running = 0;              // clear running flag
}
#endif

#endif /* TARGET */
