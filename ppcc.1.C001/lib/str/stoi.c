/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by peter.jansen@aad.gov.au
 *
 * This file contains functions used to contver string to decimal.
 */

#include <ctype.h>

#include <str.h>

unsigned char IsXDigit(char c)
{
    return (c <= 'f' && c >= 'a') || (c <= 'F' && c >= 'A') || (c <= '9' && c >= '0');
}

int stoi(register char *s, uint8_t base)
{
    uint8_t sign;
    char c;
    int  val;

    sign = 0;
    val = 0;
    while(isspace(*s)) s++;

    switch (*s)
    {
        case '-': sign++;
                  s++;
                  break;
        case '+': s++;
                  break;
        case '0': s++; /* we can ignore any leading zero's */
                  if ((*s) == 'x')
                  {
                      base = 16;
                      s++;
                  }
    }
    while(IsXDigit(c = *s++))
    {
        c = tolower(c);

        if(isdigit(c)) c -= '0';
        else c -= 'a' - (char)10;

        val = val * (int)base;
        val = val + (int)c;
    }

    if (sign != 0) return -val;

    return val;
}


