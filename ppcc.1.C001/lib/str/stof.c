/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by peter.jansen@aad.gov.au
 *
 * This file contains functions used to to manipulate stofs.
 */

#include <ctype.h>

#include <str.h>

#include <stdint.h>

unsigned char IsFDigit(char c)
{
    return (c <= 'f' && c >= 'a') || (c <= 'F' && c >= 'A') || (c <= '9' && c >= '0') || (c == '.');
}

float stof(register char *s, uint8_t base)
{
    uint8_t sign;
    char c;
    unsigned dec;
    float  v;

    sign = 0;
    v = 0;
    dec = 1;
    c = '\0';

    while(isspace(*s)) s++;

    switch (*s)
    {
        case '-': sign++;
                  s++;
                  break;
        case '+': s++;
                  break;
        case '0': s++; /* we can ignore any leading zero's */
                  if ((*s) == 'x')
                  {
                      base = 16;
                      s++;
                  }
    }

    while(IsFDigit(c = *s++))
    {
        c = tolower(c);
        
        if (c == '.')
        {
            base = 1;
            dec = 10;
        }
        else
        {

            if(isdigit(c)) c -= '0';
            else c -= 'a' - (char)10;

            v = v * (float)base;
            v = v + (float)c/dec;
            if (dec >= 10) dec = dec * 10;
        }
    }

    if (sign != 0) return -v;
    return v;
}
