/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by peter.jansen@aad.gov.au
 *
 * This file contains functions used to to manipulate strtokcps.
 */

#include <string.h>
#include <str.h>

/*
 * NAME
 *	strtokcp
 *
 * SYNOPSIS
 *	char strtokcp(in_string, delimiter_string, out_string, max_length_of_in_string);
 *
 * DESCRIPTION
 *	The strtokcp function is used to find delim in str
 *
 * RETURNS
 *	char indicating poisition within string or -1 of not found
 */

char strtokcp(char *str, const char *delim, char *cmd, size_t len)
{
    u_char i;
    char delimiters[8], *pdel = delimiters;

    /* terminate the delimiter string */
    strcpy(delimiters, delim);
    i = 0;
    while (*str && (i < len - 1))
    {
        while (*pdel != '\0') 
        {
            if (*str != *pdel++)
            {
                /* Not a delimiter */
                *cmd = *str;
            }
            else
            {
                /* Found a delimiter */
                *cmd = '\0';
                return (char) i;
            }
        }
        pdel = delimiters;
        str++;
        cmd++;
        i++;
    }
    /* Too long - truncate & fail if not 'string' terminator */
    *cmd = '\0';

    if (*str == '\0' || *str == '"')
    {
        return (char) i;
    }
    return -1;
}
