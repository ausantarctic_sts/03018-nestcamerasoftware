/*
 *  msp430 - The msp430 program.
 *  Copyright (C) 2003 Australian Antarctic Division
 *  Written by Peter Jansen <peter_jan@sts-cvs>
 *
 * This file contains functions used to to manipulate cmds.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>

#ifdef TARGET
#include <io.h>
#endif

#include <mspio.h>
#include <ppcc.h>
#include <nmea.h>
#include <cmd.h>

char Line[130];

static uint8_t linePos;
static uint8_t lineIndex = 0;
static int8_t esc = -1;
static uint8_t isstring;

static char *nextParam;

static inline char ctolower(char c) { return ((c) + (char)0x20 * (char)(((c) >= 'A') && ((c) <= 'Z'))) ;} 

extern int8_t CmdTableLength;

/* Find first parameter on command line */

char * getFirstParameter(register char *p)
{
    char * shuffle;

    isstring = 0;

    /* Skip over whitespace and commas */

    while ((*p == ' ') || (*p == '\t') || (*p == ','))
    {
        p++;
    }

    /* If its a " skip it and mark it as a string */

    if (*p == '\"')
    {
        isstring = 1;
        p++;
    }

    nextParam = p;
    shuffle = p;
    while ((((isstring == 0) && ((*nextParam != ' ') && (*nextParam != ','))) || 
            ((isstring == 1) && (*nextParam != '\"'))) && (*nextParam != '\0'))
    {
        if (*nextParam == '\\')
        {
            nextParam++;
            switch (*nextParam)
            {
                case '\0':
                    nextParam--;
                    break;
                case 'r':
                    *shuffle++ = '\r';
                    break;
                case 'n':
                    *shuffle++ = '\n';
                    break;
                case 't':
                    *shuffle++ = '\t';
                    break;
                default:
                    *shuffle++ = *nextParam;
            }
        }
        else
        {
            *shuffle = *nextParam;
            shuffle++;
        }

        nextParam++;
    }

    if (*nextParam != '\0') /* More to come */
    {
        *nextParam++ = '\0'; /* terminate the string */
        *shuffle++ = '\0';

        /* Skip over whitespace and commas */

        while ((*nextParam == ' ') || (*nextParam == '\t') || (*nextParam == ','))
        {
            nextParam++;
        }
    }

    return p;
}

/* 
 * Find the next parameter on command line 
 *
 * - parameters are separated by space or comma
 * - parameters requiring the use of space or comma must be enclosed in double 
 *   quotes
 * - 
 * 
 * Parameters:
 * NONE
 * 
 * Returns:
 * pointer to character following next space or comma (and double quote if found) 
 * 
 *
 * */

char * getNextParameter()
{
    if (*nextParam == 0)
        return nextParam;

    return getFirstParameter(nextParam);
}

static char Cmdstrnicmp(register char * s1, register const char * s2, size_t len)
{
    while ((ctolower(*s1) == ctolower(*s2)) & (*s1 != '\0') & (len > 1)) 
    {
        s1++;
        s2++;
        len--;
    }
    return  ctolower(*s1) - ctolower(*s2);
}


/* Find command in command table
 * returned -1 if no command is found
 */

int8_t FindCommand(char *p)
{ 
    int8_t i;
    int8_t Length;
    int8_t Found;

    Found = -2;
    if(NMEA_START_PACKET == p[0])  // is it a NMEA command
    {
        p = &p[7];                   // that's where the NMEA command sits
        Length = 3;
    }
    else
    {
        while(*p==' ' || (*p=='\t')) p++;  // skip over whitespace
        i = 0;
        while ((p[i] != '\0') && (p[i] != ' ') && (p[i] != ','))        // CB 8/01/04 added cmd,xx format 
        {
            i++;
        }
        Length = i;
    }

    for(i = 0; i < CmdTableLength; i++)
    {
        if ((char)0 == Cmdstrnicmp(p, CmdTab[i].Command, (size_t)Length))
        {
            if (Found == CMDERR_NOTFOUND) 
				Found = i;
            else 
				Found = CMDERR_NOCMD;
        }
    }

    return Found;
}

/* Command line handelling stuff */

// parses the current command line 'line' and calls the appropriate
// handler functions or sets error flags
// returns error code (<0) if errors occurred
// returns the index into the command table if successful
// fills in relevant fields in CmdDetails

static int8_t ParseCommandLine(void)
{
    int8_t length; // string length
    int8_t i;

    length = (int8_t)strlen(Line);

    if (CMD_COMMENT_CHAR == Line[0])  // is it a comment line?
		return CMD_COMMENT;
    
    if (NMEA_START_PACKET == Line[0]) // is it a NMEA command?
    { 
        if (length < 9)
            return (CMDERR_TOOSHORT);  // not enough characters

        if (NmeaCheckString(Line) < 1) 
			return (CMDERR_CHECKSUM);

        /* Check the NMEA Master ID and Slave ID */

        if (Line[1] != strMasterID[0]) 
			return(CMDERR_IDERROR);
        if (Line[2] != strMasterID[1]) 
			return(CMDERR_IDERROR);

        if ((Line[3] != '0') || (Line[4] != '0')) /* not all Address */
        {
            if (Line[3] != strSlaveID[0]) return(CMDERR_IDERROR);
            if (Line[4] != strSlaveID[1]) return(CMDERR_IDERROR);
        }
        
        /* Check the command format */
        
        if (NMEA_QUERY_CHAR != Line[5])   // next we check the query character is in the right spot
            return (CMDERR_NOQUERY);    // query Character not found

        if (',' != Line[6])   // The character after the Query Character must be a comma
            return (CMDERR_NOCOMMA);    // No comma after query Character

        // If we get to here we know it is a NMEA command, it is addressed to us from the
        // right Master and if it has a checksum entry the entry is correct.
        // Next we find out whether we can recognise the command.

    } /* If NMEA command */

    i = FindCommand(Line);
    if (i < 0)
        return CMDERR_NOTFOUND;     // no valid command found and something was entered

    return (i); // success : return index into CmdTab
}

/* Process the input and execute the command function */

int8_t Cmd(void)
{
    int ch;
    char c;
    int8_t ret;

    /* get a character */
    ch = getchar();

    ret = CMDERR_NOCMD;

    if (ch == -1) 
		return ret;

    c = (char)ch;

    /* Add in here processing for escape characters if you want them */
    /* Eg <ESC>[A is up arrow, <ESC>[D is back arrow */
	/*    <ESC>[B is down arror */

	if (esc != -1)
	{
        if ((c == 'O') || (c == '[')) esc = 1;
        else if ((esc == 1) && (c == 'A'))   // Up Arrow 
        {
            (void)putchar('\n');
            lineIndex = (uint8_t)strlen(Line); // get the length of the last line
            linePos = lineIndex;
            printf("%s", Line);
            esc = -1;
        }
		else if ((esc == 1) && (c == 'B'))  // Down Arrow
		{
			(void)putchar('\n');
			lineIndex = 0;
			linePos = 0;
			printf("%s", CMD_PROMPT);
			esc = -1;
		}
        else if ((esc == 1) && (c == 'D'))  // Back arrow 
        {
            esc = -1;
            if (linePos > 0) linePos--;
        }
        else if ((esc == 1) && (c == 'C')) // Forward arrow
        {
            if (linePos < lineIndex)
            {
                (void)putchar(Line[linePos]);
                linePos++;
            }
            esc = -1;
        }
        else esc = -1;
    }
    else
    {
        switch (c)
        {
            case '\b':
            case 127:
                /* FIXME: Need some more work here to cope with backspace in
                 * middle of a line
                 */
                if (lineIndex > strlen(CMD_PROMPT)) 
                	printf(" \b"); /* clear character */
					lineIndex--;

                if (linePos > strlen(CMD_PROMPT))
	                printf(" \b"); /* clear character */ 
					linePos--;

                break;
            case 27:
                esc = 0;
                break;
            case '\n':
#ifdef TARGET
	        /* WARNING WIL ROBINSON!! WARNING !!!                   */
	        /* On the UNIX host, commands are only linefeed         */
		    /* terminated but the target needs to swallow           */
		    /* linefeeds and use carriage returns for a terminator  */
	        /* because the target sees <CR><LF>                     */
	        break;
#endif
	    case '\r':
                /* expected NMEA termination <CR><LF> i.e. \r\n */
				ret = 0;
                if (lineIndex >= 1)
                {
					/* something was typed */
                    Line[lineIndex] = '\0';  // Terminate the string. Here we have the command line stored in 'Line'
                    ret = ParseCommandLine();
                    if (ret >= 0) 
                    {
						/* Emit CR/LF so cursor is on newline ready for command output */
						printf("\r\n");
						uiSleep |= SLEEP_COMMAND_ACTIVE;
                        CmdTab[(int)ret].Function(Line);
						uiSleep &= ~SLEEP_COMMAND_ACTIVE;
                    }
                }
                lineIndex = 0; // Reset the index
                linePos = 0;
                esc = -1;
                break;
            case NMEA_START_PACKET:
                lineIndex = 1; /* '$' received restart buffer */
                linePos = 1;
                Line[0] = NMEA_START_PACKET;
                break;
            default:
                /* Write character to line buffer if there is space */

                if (lineIndex < (uint8_t)(sizeof(Line)/sizeof(Line[0])))
                {
                    Line[linePos] = c;
                    if (linePos == lineIndex) lineIndex++;
                    linePos++;
                }
                break;
        } /* Switch (c) */
	
    } // if esc

   return ret;
}
