/*
 *	Copyright (C) 2004 Australian Antarctic Division
 * 
 * This file contains functions used to access the DS3234 SPI RTC
 * 
 * For revision 4 hardware - 2008/05/21
 * 
 */
#include <stdio.h>

#ifdef TARGET
#include <io.h>
#endif
#include <ds3234.h>
#include <ppcc.h>

#ifndef DEBUG
# define DEBUG 0
#endif

/*
 * BCD2BIN - Converts 2 x 4 bit Binary Coded Decimal to 8 bit Binary
 */
uint8_t BCD2BIN(uint8_t u8BCD)
{
    return ((u8BCD >> 4) * 10 + (u8BCD & 0xF)); 
}

/* 
 * BIN2BCD - Converts 8 bit Binary to 2 x 4 bit BCD
 */
uint8_t BIN2BCD(uint8_t u8BIN)
{
    return (((u8BIN / 10) << 4) | (u8BIN % 10));
}

/*
 * Powerup & init settings 
 */
uint8_t ds3234PowerUp(void)
{
    uint8_t u8Temp;
    uint8_t u8Error = 0;
    
#ifdef TARGET   
    /* Setup SPI */
    BICB(P2SEL, P2_RTC_CS);
    BISB(P2OUT, P2_RTC_CS);                           // set inactive
    BISB(P2DIR, P2_RTC_CS);                           // output
    
    BISB(P3SEL, P3_MOSI | P3_MISO | P3_SCLK);         // Enable SPI pins
    BISB(P3DIR, P3_MOSI | P3_SCLK);                   // outputs
    BICB(P3DIR, P3_MISO);                             // inputs    
    
    /* Setup the SPI controller */
    
    /* Set SWRST first */
    BISB(U0CTL, SWRST);
    
    /* Enable the module */
    BISB(ME1, USPIE0);
    
    /* Init the SPI port - clears LISTEN, I2C (if valid)
     * CHAR = 1 -> 8 bit data
     * SYNC = 1 -> SPI mode
     * MM = 1 -> UART is master
     * SWRST = 1 -> held in reset while configuring bits
     */
    U0CTL = CHAR | SYNC | MM | SWRST;
    
    /* Set Clock phase & slave select
     * CKPH = 0 -> Normal clocking scheme
     * CKPL = 0 -> Inactive level is low, Data out on rising edge
     *             Slave input data latched on falling edge of SCLK
     * SSEL = 2 -> Clock source is SMCLK (1MHz)
     * STC = 1  -> 3 pin SPI mode, STE disabled.
     * TXEPT = 1 -> Clear TX buffer flag
     */
    U0TCTL = SSEL_2 | STC | TXEPT;
    
    /* Set baud rate registers - smallest division factor = 2 
     * which results in SPI clock rate = 500kbps */
    U0BR0 = 0x02;
    U0BR1 = 0x00;
    U0MCTL = 0;   /* don't use modulation */
    
    /* Clear port reset - UART is now ready */
    BICB(U0CTL, SWRST);
    
    /* clear rx int flag without disturbing Watchdog IRQ flag
     * Applies ESPECIALLY to all bits in IFG1 */
    BICB(IFG1, URXIFG0);   
#endif    

    /* Check EOSC flag - must be low to operate normally */
    u8Temp = ds3234Read(DS3234_CTL);
    
    if ((u8Temp & DS3234_CTL_EOSC) == DS3234_CTL_EOSC)
    {
	/* clear EOSC bit and write back */
	u8Temp &= ~DS3234_CTL_EOSC;
	ds3234Write(DS3234_CTL, u8Temp);
	u8Error |= DS3234_CTL_EOSC;
    }
    
    /* Set CRATE1:0 to '11' to set the conversion rate to every 512 seconds */
    u8Temp = ds3234Read(DS3234_STA);
    u8Temp |= (DS3234_STA_CRATE1 | DS3234_STA_CRATE0);
    ds3234Write(DS3234_STA, u8Temp);
    
    /* Check OSF flag to indicate that clock/time might be faulty */
    if ((u8Temp & DS3234_STA_OSF) == DS3234_STA_OSF)
    {
	/* return error - but do not clear it
	 * error is cleared when clock is written properly
	 */
	u8Error |= DS3234_STA_OSF;
    }
    
    return u8Error;
}
    
/*
 * Transmit a byte / receive a byte - Assumes SPI device is ENABLED
 */
uint8_t ds3234TxRx(uint8_t u8TXD)
{
    uint8_t u8RXD;
    
#ifdef TARGET
    /* wait for transmitter to empty */
    while ((U0TCTL & TXEPT) == 0);
    
    /* Send the data byte */
    U0TXBUF = u8TXD;
    
    /* wait for receive to complete */
    while ((IFG1 & URXIFG0) == 0);
    
    /* Read the data and clear the flag */
    u8RXD = U0RXBUF;
    
    return u8RXD;
#endif
#ifdef HOST
    return u8RXD;
#endif
}
    
/*
 * Write a byte to a RTC address
 */
void ds3234Write(uint8_t u8Address, uint8_t u8Data)
{
#ifdef TARGET
    BICB(P2OUT, P2_RTC_CS);
#endif

    /* Write the address with MSB set to indicate write - ignore RX data */
    ds3234TxRx(u8Address | 0x80);
    
    /* Write the data - ignore received data */
    ds3234TxRx(u8Data);

#ifdef TARGET
    BISB(P2OUT, P2_RTC_CS);
#endif
}

/* 
 * Read a single byte from an RTC address
 */
uint8_t ds3234Read(uint8_t u8Address)
{
    uint8_t u8RXD;
    
#ifdef TARGET
    BICB(P2OUT, P2_RTC_CS);
#endif
    
    /* Write the address with MSB clear to indicate read - ignore the received data */
    ds3234TxRx(u8Address & 0x7F);
    
    /* Write dummy value, read the received data */
    u8RXD = ds3234TxRx(0xFF);

#ifdef TARGET
    BISB(P2OUT, P2_RTC_CS);
#endif

    return u8RXD;
}

/*
 * ds3234ReadRTC - Read RTC registers into the RTCTimeStruct
 * 
 * If the Hours value is detected as being in AM/PM mode, then
 * it is converted to 24 hour mode and written back.
 * 
 */
void ds3234BCDReadRTC(RTCTimeStruct *RTCTime)
{
    RTCTime->u8Second = ds3234Read(DS3234_SECONDS);
    RTCTime->u8Minute = ds3234Read(DS3234_MINUTES);
    RTCTime->u8Hour = ds3234Read(DS3234_HOURS);    
    RTCTime->u8DayOfMonth= ds3234Read(DS3234_DAYOFMONTH);
    RTCTime->u8Month = ds3234Read(DS3234_CENTMON) & 0x1F; // Mask off Century
    RTCTime->u8Year = ds3234Read(DS3234_YEAR);
}

/*
 * ds1305WriteRTC - Write RTC registers from RTCTimeStruct
 */
void ds3234BCDWriteRTC(RTCTimeStruct *RTCTime)
{
    ds3234Write(DS3234_YEAR, RTCTime->u8Year);
    ds3234Write(DS3234_CENTMON, RTCTime->u8Month);
    ds3234Write(DS3234_DAYOFMONTH, RTCTime->u8DayOfMonth);
    ds3234Write(DS3234_HOURS, RTCTime->u8Hour);
    ds3234Write(DS3234_MINUTES, RTCTime->u8Minute);
    ds3234Write(DS3234_SECONDS, RTCTime->u8Second);    
}

/* 
 * Read temperature register and return as float deg C
 * 
 * iConvert = 1 to initiate a manual conversion, 0 to just read the value
 */
float ds3234ReadTemperature(int iConvert,
			    int8_t *i8AgeOffset,
			    int *iRate)
{
    uint8_t u8Frac, u8Conv;
    int8_t i8Int;
    float fTemp;
    
    if (iConvert == 1)
    {
	u8Conv = ds3234Read(DS3234_CTL);
	ds3234Write(DS3234_CTL, u8Conv | DS3234_CTL_CONV);
	
	/* wait until CONV bit clear */
	do
	{
	    u8Conv = ds3234Read(DS3234_CTL);
	} while ((u8Conv & DS3234_CTL_CONV) == DS3234_CTL_CONV);
    }
    
    /* conversion finished - read temperature and convert */
    i8Int = ds3234Read(DS3234_TEMP_MSB);
    u8Frac = ds3234Read(DS3234_TEMP_LSB) >> 6;
    
    fTemp = (float)(i8Int) + u8Frac * 0.25;
    
    /* Read age register */
    *i8AgeOffset = (int8_t)ds3234Read(DS3234_AGEOFFSET);
    
    /* Read conversion rate from CRATE0/1 */
    *iRate = (int)(64 << ((ds3234Read(DS3234_STA) >> 4) & 0x3));
    
    return fTemp;
}

/* Read various sizes from NVRAM */
uint8_t  NVRAMu8Read(uint8_t u8Addr)
{
    ds3234Write(DS3234_SRAM_ADDR, u8Addr);
    return ds3234Read(DS3234_SRAM_DATA);
}

/* Data storage is big endian */
uint16_t NVRAMu16Read(uint8_t u8Addr)
{
    uint16_t u16;

    /* MSB */
    ds3234Write(DS3234_SRAM_ADDR, u8Addr);
    u16 = ds3234Read(DS3234_SRAM_DATA);
    
    /* LSB */
    ds3234Write(DS3234_SRAM_ADDR, u8Addr+1);
    u16 = (u16 << 8) | ds3234Read(DS3234_SRAM_DATA);

    return u16;
}

/* data storage is big endian, not word aligned */
extern uint32_t NVRAMu32Read(uint8_t u8Addr)
{
    uint32_t u32;

    /* MSB */
    ds3234Write(DS3234_SRAM_ADDR, u8Addr);
    u32 = ds3234Read(DS3234_SRAM_DATA);
    
    /* Upper middle */
    ds3234Write(DS3234_SRAM_ADDR, u8Addr+1);
    u32 = (u32 << 8) | ds3234Read(DS3234_SRAM_DATA);    

    /* lower middle */
    ds3234Write(DS3234_SRAM_ADDR, u8Addr+2);
    u32 = (u32 << 8) | ds3234Read(DS3234_SRAM_DATA);    

    /* LSB */
    ds3234Write(DS3234_SRAM_ADDR, u8Addr+3);
    u32 = (u32 << 8) | ds3234Read(DS3234_SRAM_DATA);    

    return u32;
}

void NVRAMu8Write(uint8_t u8Addr, uint8_t u8Data)
{
    ds3234Write(DS3234_SRAM_ADDR, u8Addr);
    ds3234Write(DS3234_SRAM_DATA, u8Data);
}

void NVRAMu16Write(uint8_t u8Addr, uint16_t u16Data)
{
    ds3234Write(DS3234_SRAM_ADDR, u8Addr);
    ds3234Write(DS3234_SRAM_DATA, (uint8_t)((u16Data >> 8) & 0xFF));

    ds3234Write(DS3234_SRAM_ADDR, u8Addr+1);
    ds3234Write(DS3234_SRAM_DATA, (uint8_t)(u16Data & 0xFF));
}

void NVRAMu32Write(uint8_t u8Addr, uint32_t u32Data)
{
    ds3234Write(DS3234_SRAM_ADDR, u8Addr);
    ds3234Write(DS3234_SRAM_DATA, (uint8_t)((u32Data >> 24) & 0xFF));

    ds3234Write(DS3234_SRAM_ADDR, u8Addr+1);
    ds3234Write(DS3234_SRAM_DATA, (uint8_t)((u32Data >> 16) & 0xFF));
    
    ds3234Write(DS3234_SRAM_ADDR, u8Addr+2);
    ds3234Write(DS3234_SRAM_DATA, (uint8_t)((u32Data >> 8) & 0xFF));

    ds3234Write(DS3234_SRAM_ADDR, u8Addr+3);
    ds3234Write(DS3234_SRAM_DATA, (uint8_t)(u32Data & 0xFF));
}
