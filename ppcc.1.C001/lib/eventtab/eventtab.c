/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by kym_new@localhost
 *
 * This file contains functions used to to manipulate eventtabs.
 */

#include <stdio.h>
#include <stdint.h>
#include <limits.h>

#include <ppcc.h>
#include <str.h>
#include <flash.h>

#include <eventtab.h>

#ifdef TARGET
#include <io.h>
#endif

#ifndef DEBUG
# define DEBUG 0
#endif

/* This data structure must be sized to be less than or equal to 
 * the available memory specified by the linker description file! */
eventTabSection eventTabEntryStruct eventTable[EVENTTAB_MAX_ENTRY];

/*
 * StrToBCDreturn BCD value based on input two digit char with wildcards
 * 
 * eg                        Return  u8Mask
 *    non-wildcard => '1'  = 01      00
 *                    '01' = 01      00
 *                    '11' = 11      00
 *    wildcard     => '*'  = 00      AA
 *                    '**' = 00      AA
 *                    '*1' = 01      A0
 *                    '1*' = 10      0A
 * 
 * Wildcards are transferred to BCD 0xA (ie out of range) to indicate
 * that it is a wildcard..
 * 
 * p      = 2 character string to convert
 * wc     = wildcard character
 * u8wc   = wildcard BCD value (should probably be 0xA .. 0xE)
 * u8Mask = Mask value to add to returned value ..
 * 
 * After calling this function, add the return value to the mask and store 
 * in the eventtab.
 * 
 */
uint8_t strToBCDwc(char *p, const char wc, uint8_t u8wc, uint8_t *u8Mask)
{
    *u8Mask = 0;
        
    if (p[1] == '\0')
    {
	/* 1 digit argument */
	if (p[0] == wc)
	{
	    *u8Mask = ((u8wc << 4 ) | u8wc);
	    return 0;
	}
	else
	    return ((uint8_t)stoi(p, 10));
    }
    else if (p[2] == '\0')
    {
	/* 2 digits */
	if (p[0] == wc)
	{
	    *u8Mask = (u8wc << 4);
	    p[0] = '0';
	}
	if (p[1] == wc)
	{
	    *u8Mask |= u8wc;
	    p[1] = '0';
	}
	return ((uint8_t)stoi(p,10));
    }
    else
	return ((uint8_t)0xFF);
}

/*
 * printBCDwc - prints out a 2 digit BCD number with wildcards (if present)
 *
 * u8BCD  - value from eventtab
 * wc     - char to output as wildcard
 * u8wc   - BCD wildcard value 
 *
 */
void printBCDwc(uint8_t u8BCD, const char wc, uint8_t u8wc)
{

    /* MSNybble */
    if ((u8BCD & (u8wc << 4)) == (u8wc << 4))
	putchar(wc);
    else
	printf("%0X", u8BCD >> 4);
    
    /* LSNybble */
    if ((u8BCD & u8wc) == u8wc)
	putchar(wc);
    else
	printf("%0X", u8BCD & 0xF);
}

/*
 * Erase the entire eventTable
 */
void eventTabErase(void)
{
    flashEraseFromLen((unsigned char *)&eventTable[0], sizeof(eventTable));
}

/*
 * Add a eventtab Entry in next available slot
 *
 * Returns -1 if none available otherwise slot number
 *
 */
int16_t eventTabAdd(eventTabEntryStruct *Entry)
{
    uint16_t u16Slot = 0;
    
    /* Find first slot that is deleted, erased or reached the end */
    while ((eventTable[u16Slot].u32Packed != EVENTTAB_ENTRY_DELETED) &&
	   (eventTable[u16Slot].u32Packed != EVENTTAB_ENTRY_ERASED) &&
	   (u16Slot < EVENTTAB_MAX_ENTRY))
    {
	u16Slot++;
    };
    
    /* if we found a free slot then program event */
    if (u16Slot < EVENTTAB_MAX_ENTRY) 
    {
	flashUpdate((unsigned char *)(&eventTable[u16Slot]), 
		    sizeof(eventTable[0]),
		    (unsigned char *)(Entry));
    }
    else
	return -1;
    
    return u16Slot;
}

/*
 * eventTabDelete - delete one entry by writing it to zero
 * 
 */
void eventTabDel(uint16_t u16Index)
{
    eventTabEntryStruct Entry;

    Entry.strCmdLine[0] = '\0';
    Entry.u32Packed = EVENTTAB_ENTRY_DELETED;
    
    if (u16Index < EVENTTAB_MAX_ENTRY)
    {
	flashUpdate((unsigned char *)(&eventTable[u16Index]),
		    sizeof(eventTable[0]),
		    (unsigned char *)(&Entry));
    }
}

/*
 * eventTabCompareEntry - does a compare of the time between 
 * two entries, taking into account wildcards.
 * 
 * Returns true (1) on match, false (0) otherwise
 * 
 */
uint8_t eventTabCompareEntry(eventTabEntryStruct *Entry1, 
			    eventTabEntryStruct *Entry2,
			    uint8_t u8wc)
{
    uint8_t u8Nybble;
    uint32_t u32p1 = Entry1->u32Packed;
    uint32_t u32p2 = Entry2->u32Packed;

#if DEBUG >= 1
    printf("eventTabCompareEntry 0x%08lX 0x%08lX\r\n", u32p1, u32p2);
#endif
    
    for (u8Nybble = 0; u8Nybble < 8; u8Nybble++)
    {
	if (((u32p1 & 0xF) == u8wc) ||
	    ((u32p2 & 0xF) == u8wc))
	{
	    // either entries have wildcard skip to next nybble
	}
	else if ((u32p1 & 0xF) != (u32p2 & 0xF))
	{
#if DEBUG >= 1
	    printf("eventTabCompareEntry : Entries not equal\r\n");
#endif
	    /* are nybbles equal?.. if not give up & return */
	    return 0;
	}
	    
	u32p1 >>= 4;
	u32p2 >>= 4;
    }
    
#if DEBUG >= 1
    printf("eventTabCompareEntry : Entries equal\r\n");
#endif
    return 1;
}

/*
 * eventTabFind - Locates the first entry in the eventtab matching
 * the supplied values, returns the index, or -1 if not found
 * 
 */
int16_t eventTabFind(eventTabEntryStruct *Entry, int iStartIndex)
{
    int16_t i16Index = iStartIndex;
    
    while ((i16Index < EVENTTAB_MAX_ENTRY) &&
	   (eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_ERASED))
    {
	if (eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_DELETED)
	{
	    if (eventTabCompareEntry(&eventTable[i16Index], Entry, EVENTTAB_BCD_WC))
	    {
#if DEBUG >= 1
		printf("eventTabFind : matched i16Index %d Entry1 0x%08lX Entry2 0x%08lX\r\n",
		       i16Index, eventTable[i16Index].u32Packed, Entry->u32Packed);
#endif
		break;
	    }
	    else
	    {
#if DEBUG >= 1
		printf("eventTabFind : not-matched i16Index %d Entry1 0x%08lX Entry2 0x%08lX\r\n",
		       i16Index, eventTable[i16Index].u32Packed, Entry->u32Packed);
#endif	    
	    }
	}
	i16Index++;
    }
    
    /* if we reached the end of the table, or all out of 
     * valid entries */
    if ((i16Index >= EVENTTAB_MAX_ENTRY) ||
	(eventTable[i16Index].u32Packed == EVENTTAB_ENTRY_ERASED))
	return -1;
    else
	return i16Index;
}

/*
 * eventTabCount - return the number of non erased/deleted entries 
 * in the eventTable
 */
uint16_t eventTabCount(void)
{
    uint16_t u16Count = 0;
    int16_t i16Index = 0;
    
    while ((i16Index < EVENTTAB_MAX_ENTRY) &&
	   (eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_ERASED))
    {
	if ((eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_DELETED) &&
	    (eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_ERASED))
	{
	    u16Count++;
	}
	
	i16Index++;
    }
    
    return u16Count;
}

/*
 * eventTabEmpty() - checks to see if eventTab is empty..
 */
int eventTabEmpty(void)
{
    int16_t i16Index;
    
    for (i16Index = 0; i16Index < EVENTTAB_MAX_ENTRY; i16Index++)
    {
	if ((eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_ERASED) &&
	    (eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_DELETED))
	    return 0;
    }
    return 1;
}
