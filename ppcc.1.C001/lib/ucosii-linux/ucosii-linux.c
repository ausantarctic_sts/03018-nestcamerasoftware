/* linuxucos.c    v0.2.0  01-Jan-02    David Poole davep@mbuf.com
 *
 * Portions copyright as follows:
 *
 *                            (c) Copyright 2001, Jean J. Labrosse, Weston, FL
 *                                           All Rights Reserved
 *
 * Simple wrapper around SysV and POSIX threads functions to mimic uCOS-II
 * features.  Has only been tested on RedHat Linux 7 and 7.2.  
 *
 * Currently supported features are:
 *
 * Semaphores:
 *  none
 *
 * Mutual Exclusion Semaphores:
 *  OSMutexAccept()
 *  OSMutexCreate()
 *  OSMutexDel()
 *  OSMutexPend()
 *  OSMutexPost()
 *
 * Event Flags:
 *  OSFlagAccept()
 *  OSFlagCreate()
 *  OSFlagPend()
 *  OSFlagPost()
 *
 * Message Mailboxes:
 *  none
 *
 * Message Queues:
 *  OSQAccept()
 *  OSQCreate()
 *  OSQDel()
 *  OSQPend()
 *  OSQPost()
 *
 * Memory Management:
 *  none
 *
 * Task Management:
 *  OSTaskCreate()
 *  OSTaskCreateExt()
 *
 * Time Management:
 *  OSTimeDly()
 *  OSTimeGet()
 *
 * Miscellaneous:
 *  OSInit();
 *  OSStart();
 *
 *
 * uCOS-II specific information:  I'm using a 1ms "tick" on Linux so
 * OS_TICKS_PER_SEC must be 1000.  
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/time.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <assert.h>

#ifndef SPLINT

#define LINUX_UCOS
#ifndef  OS_MASTER_FILE
#define  OS_GLOBALS
#include <ucosii.h>
#endif

// using a 1 ms tick on Linux
#define NANOSECONDS_PER_TICK 1000000l
#define SECONDS              1000

#if (OS_EVENT_EN > 0) && (OS_MAX_EVENTS > 0)
// Table of EVENT control blocks 
#define OS_EVENT_COUNT 99

OS_EVENT lnxOSEventTbl[OS_EVENT_COUNT];  
#endif

// mutex to protect the lnxOSEventTbl list
pthread_mutex_t oseventtbl_mutex = PTHREAD_MUTEX_INITIALIZER;

// Table of available OS_FLAG_GRP 
// #define OS_MAX_FLAGS 99

#if (OS_VERSION >= 251) && (OS_FLAG_EN > 0) && (OS_MAX_FLAGS > 0)
OS_FLAG_GRP lnxOSFlagTbl[OS_MAX_FLAGS];

// mutex to protect the lnxOSFlagTbl list
pthread_mutex_t osflagtbl_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

// uCOS-II flags are implemented using pthread's condition variables;
struct pthread_flag {
    pthread_mutex_t mutex;
    pthread_cond_t  cond;
};

// "blank" mutex and condition for initializing struct pthread_flag members
pthread_mutex_t blank_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t blank_cond = PTHREAD_COND_INITIALIZER;

// for debugging 
#define PRINTF  printf
#define PERROR  perror
#define ASSERT  assert

// returned when an interface function encounters
// an error; this should never happen
#define OS_ERR_INTERNAL  -1

// there are a few places where a lack of a timeout
// on SysV IPC mechanisms means a poll 
#define POLL_DELAY   10 // ticks

// sigh
#ifndef min
#define min(x,y)  ((x)<(y)?(x):(y))
#endif

struct sembuf sem_release = { 0,  1, IPC_NOWAIT };
struct sembuf sem_claim   = { 0, -1, IPC_NOWAIT };

// convert a uCOS-II 'prio' (used as a task ID as of 2.51)
// into the pthread_t id

pthread_t pthread_to_prio[OS_LOWEST_PRIO];

// time is measure from when the system "booted", ie
// when the program was started (see epoch_init())
struct timeval epoch;

void epoch_init( void )
{
    gettimeofday( &epoch, NULL );
}

INT32U get_ms_time( void )
{
    INT32U ms;
    struct timeval now;

    gettimeofday( &now, NULL );    
    ms = now.tv_sec * 1000 + now.tv_sec / NANOSECONDS_PER_TICK;

    return ms;
}

#if (OS_EVENT_EN > 0) && (OS_MAX_EVENTS > 0)
OS_EVENT *get_free_event( INT8U evttype )
{
    int i, perr;

    // lock the table 
    perr = pthread_mutex_lock( &oseventtbl_mutex );
    ASSERT( perr == 0 );
    
    // get a free OS_EVENT
    for( i=0 ; i<OS_EVENT_COUNT ; i++ ) {
        if( lnxOSEventTbl[i].OSEventType == OS_EVENT_TYPE_UNUSED ) {
            lnxOSEventTbl[i].OSEventType = evttype;

            // unlock the table
            perr = pthread_mutex_unlock( &oseventtbl_mutex );
            ASSERT( perr == 0 );

            return &lnxOSEventTbl[i];
        }
    }

    // unlock the table
    perr = pthread_mutex_unlock( &oseventtbl_mutex );
    ASSERT( perr == 0 );
    
    return NULL;
}
#endif

// *****************
//
// Time Functions
//
// *****************

void OSTimeDly( INT16U ticks )
{
    int ret;
    struct timespec req, rem;
    ldiv_t nanotime;

    // using a 1ms "tick" on Linux 
    nanotime = ldiv( ticks, SECONDS );
    req.tv_sec = nanotime.quot;
    req.tv_nsec = (nanotime.rem) * NANOSECONDS_PER_TICK;

    while( 1 ) {
        ret = nanosleep( &req, &rem );
        if( ret == 0 ) {
            // successfully finished the sleep 
            break;
        }
        assert( errno == EINTR );

        // at this point, we were interrupted by a signal 
        // recalculate how much we needed to sleep and do it again 
        memcpy( &req, &rem, sizeof(struct timespec) );
    }

}

INT32U OSTimeGet( void )
{
    // get_ms_time() returns ms since program start
    return get_ms_time();
}

// *****************
//
// Flag Functions
//  
// *****************
#if (OS_VERSION >= 251) && (OS_FLAG_EN > 0) && (OS_MAX_FLAGS > 0)
OS_FLAGS OS_FlagTest( OS_FLAG_GRP *pgrp, OS_FLAGS flags, INT8U wait_type, INT16U timeout, INT8U *err )
{
    OS_FLAGS      flags_cur;
    OS_FLAGS      flags_rdy;
    BOOLEAN       consume;
    int perr;
    struct pthread_flag *pflag;
    struct timeval now;
    struct timespec sleeptime;
    ldiv_t nanotime;

    if( pgrp == NULL ) {
        *err = OS_FLAG_INVALID_PGRP;
        return 0;
    }

    if( pgrp->OSFlagType != OS_EVENT_TYPE_FLAG ) {
        *err = OS_ERR_EVENT_TYPE;
        return 0;
    }

    // wait_type can be
    //      OS_FLAG_WAIT_CLR_ALL -- all bits in 'flags' will be clear
    //      OS_FLAG_WAIT_CLR_ANY -- any bits in 'flags' will be clear
    //      OS_FLAG_WAIT_SET_ALL -- all bits in 'flags' will be set
    //      OS_FLAG_WAIT_SET_ANY -- any bits in 'flags' will be clear
    

    // test the flags
    pflag = (struct pthread_flag *)pgrp->OSFlagWaitList;

    // lock the pthread condition
    perr = pthread_mutex_lock( &(pflag->mutex) );
    ASSERT( perr == 0 );
   
    // test 
    if (wait_type & OS_FLAG_CONSUME) {                     /* See if we need to consume the flags      */
        wait_type &= ~OS_FLAG_CONSUME;
        consume    = TRUE;
    } else {
        consume    = FALSE;
    }
    *err = OS_NO_ERR;

    /* timeout of 1 => no wait */
    /* timeout of 0 => wait forever */
    if( timeout > 1 ) {
        // our timeout is in 1ms "ticks" so convert to system nanoseconds
        nanotime = ldiv( timeout, SECONDS );
        gettimeofday( &now, NULL );
        sleeptime.tv_sec = now.tv_sec + nanotime.quot;
        sleeptime.tv_nsec = now.tv_usec + nanotime.rem * NANOSECONDS_PER_TICK;
    }

    *err = OS_NO_ERR;
    flags_cur = 0;

    switch( wait_type ) {
        case OS_FLAG_WAIT_SET_ALL :
            flags_rdy = pgrp->OSFlagFlags & flags;        /* Extract only the bits we want            */
            perr = 0;
            while (flags_rdy != flags && perr != ETIMEDOUT ) {                     
                if( timeout == 0 ) {
                    /* wait forever */
                    perr = pthread_cond_wait( &(pflag->cond), &(pflag->mutex) );
                }
                else if( timeout > 1 ) {
                    perr = pthread_cond_timedwait( &(pflag->cond), &(pflag->mutex), &sleeptime );
                }
                else {
                    *err  = OS_FLAG_ERR_NOT_RDY;
                    goto fail;
                }
                flags_rdy = pgrp->OSFlagFlags & flags;        
            }

            if( perr == ETIMEDOUT ) {
                *err = OS_TIMEOUT;
                goto fail;
            }

            if (consume == TRUE) {                    /* See if we need to consume the flags      */
                pgrp->OSFlagFlags &= ~flags_rdy;      /* Clear ONLY the flags that we wanted      */
            }

            flags_cur = pgrp->OSFlagFlags;                /* Will return the state of the group       */
            break;
            
        case OS_FLAG_WAIT_SET_ANY :
            flags_rdy = pgrp->OSFlagFlags & flags;        /* Extract only the bits we want            */
            perr = 0;

            while(flags_rdy == (OS_FLAGS)0 && perr != ETIMEDOUT ) {
                if( timeout == 0 ) {
                    /* wait forever */
                    perr = pthread_cond_wait( &(pflag->cond), &(pflag->mutex) );
                }
                else if( timeout > 1 ) {
                    perr = pthread_cond_timedwait( &(pflag->cond), &(pflag->mutex), &sleeptime );
                }
                else {
                    *err  = OS_FLAG_ERR_NOT_RDY;
                    goto fail;
                }
                flags_rdy = pgrp->OSFlagFlags & flags;        
            }

            if( perr == ETIMEDOUT ) {
                *err = OS_TIMEOUT;
                goto fail;
            }

            if (consume == TRUE) {                    /* See if we need to consume the flags      */
                pgrp->OSFlagFlags &= ~flags_rdy;      /* Clear ONLY the flags that we got         */
            }

            flags_cur = pgrp->OSFlagFlags;                /* Will return the state of the group       */
            break;

        case OS_FLAG_WAIT_CLR_ALL : 
            flags_rdy = ~pgrp->OSFlagFlags & flags;       /* Extract only the bits we want            */
            perr = 0;

            while( flags_rdy != flags && perr != ETIMEDOUT ) {                     
                if( timeout == 0 ) {
                    /* wait forever */
                    perr = pthread_cond_wait( &(pflag->cond), &(pflag->mutex) );
                }
                else if( timeout > 1 ) {
                    perr = pthread_cond_timedwait( &(pflag->cond), &(pflag->mutex), &sleeptime );
                }
                else {
                    *err  = OS_FLAG_ERR_NOT_RDY;
                    goto fail;
                }
                flags_rdy = ~pgrp->OSFlagFlags & flags;       
            }

            if( perr == ETIMEDOUT ) {
                *err = OS_TIMEOUT;
                goto fail;
            }

            if (consume == TRUE) {                    /* See if we need to consume the flags      */
                pgrp->OSFlagFlags |= flags_rdy;       /* Set ONLY the flags that we wanted        */
            }

            flags_cur = pgrp->OSFlagFlags;                /* Will return the state of the group       */
            break;

        case OS_FLAG_WAIT_CLR_ANY :
            flags_rdy = ~pgrp->OSFlagFlags & flags;       /* Extract only the bits we want            */
            perr = 0;

            while(flags_rdy == (OS_FLAGS)0 && perr != ETIMEDOUT ) {               
                if( timeout == 0 ) {
                    /* wait forever */
                    perr = pthread_cond_wait( &(pflag->cond), &(pflag->mutex) );
                }
                else if( timeout > 1 ) {
                    perr = pthread_cond_timedwait( &(pflag->cond), &(pflag->mutex), &sleeptime );
                }
                else {
                    *err  = OS_FLAG_ERR_NOT_RDY;
                    goto fail;
                }
                flags_rdy = ~pgrp->OSFlagFlags & flags;       
            }

            if( perr == ETIMEDOUT ) {
                *err = OS_TIMEOUT;
                goto fail;
            }

            if (consume == TRUE) {                    /* See if we need to consume the flags      */
                pgrp->OSFlagFlags |= flags_rdy;       /* Set ONLY the flags that we got           */
            }

            flags_cur = pgrp->OSFlagFlags;                /* Will return the state of the group       */
            break;

        default :
            ASSERT(0);
            flags_cur = (OS_FLAGS)0;
            *err      = OS_FLAG_ERR_WAIT_TYPE;
    }

fail:
    // unlock the pthread condition 
    perr = pthread_mutex_unlock( &(pflag->mutex) );
    ASSERT( perr == 0 );

    return flags_cur;
}

OS_FLAGS OSFlagAccept( OS_FLAG_GRP *pgrp, OS_FLAGS flags, INT8U wait_type, INT8U *err )
{
    OS_FLAGS flags_cur;

    /* timeout == 0 => wait forever so use 1 to indicate no wait.
     * If a caller passes in 1 deliberately to OSFlagPend(), the system
     * will (supposedly) only delay for 1 tick which is pretty much nothing
     * anyway.
     */
    flags_cur = OS_FlagTest( pgrp, flags, wait_type, 1/* no wait */, err );

    return flags_cur;
}

OS_FLAG_GRP *OSFlagCreate( OS_FLAGS flags, INT8U *err )
{
    int i, perr;
    struct pthread_flag *pflag;

    // lock the OSFlagTbl list 
    perr = pthread_mutex_lock( &osflagtbl_mutex );
    ASSERT( perr == 0 );
    
    // get a free OS_FLAG_GRP and initialize it
    for( i=0  ; i<OS_MAX_FLAGS ; i++ ) {
        if( lnxOSFlagTbl[i].OSFlagType == 0 ) {
            lnxOSFlagTbl[i].OSFlagType = OS_EVENT_TYPE_FLAG;
            lnxOSFlagTbl[i].OSFlagFlags = flags;
            pflag = (struct pthread_flag *)malloc(sizeof(struct pthread_flag));
            if( !pflag ) {
                goto fail;
            }

            memcpy( &(pflag->mutex), &blank_mutex, sizeof(pthread_mutex_t));
            memcpy( &(pflag->cond), &blank_cond, sizeof(pthread_cond_t));

            // "borrow" the task list pointer
            lnxOSFlagTbl[i].OSFlagWaitList = (void *)pflag;

            // unlock the OSFlagTbl list
            perr = pthread_mutex_unlock( &osflagtbl_mutex );
            ASSERT( perr == 0 );
            
            *err = OS_NO_ERR;
            return &lnxOSFlagTbl[i];
        }
    }

fail:
    // unlock the OSFlagTbl list
    perr = pthread_mutex_unlock( &osflagtbl_mutex );
    ASSERT( perr == 0 );

    *err = OS_FLAG_GRP_DEPLETED;
    return NULL;
}

#if 0
OS_FLAG_GRP *OSFlagDel( OS_FLAG_GRP *pgrp, INT8U opt, INT8U *err )
{
    if( pgrp == NULL ) {
        *err = OS_FLAG_INVALID_PGRP;
        return 0;
    }

    if( pgrp->OSFlagType != OS_EVENT_TYPE_FLAG ) {
        *err = OS_ERR_EVENT_TYPE;
        return 0;
    }


    return 0;
}
#endif

OS_FLAGS OSFlagPend( OS_FLAG_GRP *pgrp, OS_FLAGS flags, INT8U wait_type, INT16U timeout, INT8U *err )
{
    OS_FLAGS flags_cur;

    /* timeout == 0 => wait forever */
    flags_cur = OS_FlagTest( pgrp, flags, wait_type, timeout, err );

    return flags_cur;
}

OS_FLAGS OSFlagPost( OS_FLAG_GRP *pgrp, OS_FLAGS flags, INT8U opt, INT8U *err )
{
    OS_FLAGS      flags_cur;
    int perr;
    struct pthread_flag *pflag;

    if( pgrp == NULL ) {
        *err = OS_FLAG_INVALID_PGRP;
        return 0;
    }

    if( pgrp->OSFlagType != OS_EVENT_TYPE_FLAG ) {
        *err = OS_ERR_EVENT_TYPE;
        return 0;
    }

    // test the flags
    pflag = (struct pthread_flag *)pgrp->OSFlagWaitList;

    // lock the pthread condition
    perr = pthread_mutex_lock( &(pflag->mutex) );
    ASSERT( perr == 0 );
   
    switch (opt) {
        case OS_FLAG_CLR:
             pgrp->OSFlagFlags &= ~flags;            /* Clear the flags specified in the group         */
             break;
             
        case OS_FLAG_SET:
             pgrp->OSFlagFlags |=  flags;            /* Set   the flags specified in the group         */
             break;
          
        default:
             ASSERT(0);
             *err = OS_FLAG_INVALID_OPT;
             flags_cur = 0;
             goto fail;
    }

    // wake everyone up; the waiting thread(s) are responsible for
    // testing the condition (not terribly efficient but works ok
    // -- when in doubt use brute force)
    perr = pthread_cond_broadcast( &(pflag->cond) );
    ASSERT( perr == 0 );

    *err      = OS_NO_ERR;
    flags_cur = pgrp->OSFlagFlags;
    
fail:
    // unlock the pthread condition 
    perr = pthread_mutex_unlock( &(pflag->mutex) );
    ASSERT( perr == 0 );

    return flags_cur;
}
#endif

// *****************
//
//  Message Queues
//  
// *****************

#if (OS_Q_EN > 0) && (OS_MAX_QS > 0)
struct ptrmsg {
    long mtype;
    void *ptr;
};

#if OS_Q_ACCEPT_EN > 0
void *OSQAccept(OS_EVENT *pevent)
{
    int err, msgid;
    struct ptrmsg ipcmsg;
    void *ptr;

    msgid = (int)pevent->OSEventPtr;

    // get a message
    err = msgrcv( msgid, (void *)&ipcmsg, sizeof(void *), 0, IPC_NOWAIT ); 
    if( err < 0 ) {
        if( errno == ENOMSG ) {
            // would have timed out -- nothing to receive
            return NULL;
        }

        // shouldn't happen 
        PERROR( "msgrcv() failed in OSQAccept()" );
        return NULL;
    }

    // at this point, we got something
    ptr = ipcmsg.ptr;

    return ptr;
}
#endif

OS_EVENT *OSQCreate(void **start, INT16U size)
{
    int msgid;
    OS_EVENT *evt;

    evt = get_free_event(OS_EVENT_TYPE_Q);
    if( evt == NULL ) {
        return NULL;
    }

    msgid = msgget( IPC_PRIVATE, 0600 );
    if( msgid == -1 ) {
        // shouldn't happen 
        PERROR( "msgget() failed in OSQCreate()" ); 
        evt->OSEventType = OS_EVENT_TYPE_UNUSED;
        return NULL;
    }

    evt->OSEventPtr = (void *)msgid;
    
    return evt;
}

OS_EVENT *OSQDel(OS_EVENT *pevent, INT8U opt, INT8U *err)
{
    int msgid;

    if( pevent->OSEventType != OS_EVENT_TYPE_Q ) {
        *err = OS_ERR_EVENT_TYPE;
        return NULL;
    }

    msgid = (int)pevent->OSEventPtr;

    if( msgctl( msgid, IPC_RMID, NULL ) ) {
        // shouldn't happen 
        PERROR( "msgctl() failed OSQDel()" );
        *err = OS_ERR_INTERNAL;
        return NULL;
    }

    // free this event structure
    pevent->OSEventType = OS_EVENT_TYPE_UNUSED;

    *err = OS_NO_ERR;
    return NULL;
}


void  *OSQPend(OS_EVENT *pevent, INT16U timeout, INT8U *err)
{
    INT16U next_sleep;
    void *ptr;

    // see OSMutexPend() for comments about polling
    while( 1 ) {
        if( (ptr=OSQAccept( pevent )) != NULL ) {
            *err = OS_NO_ERR;
            return ptr;
        }

        next_sleep = min(timeout,POLL_DELAY);
        OSTimeDly( next_sleep );
        timeout -= next_sleep;
        if( timeout == 0 ) {
            // failed to claim
            *err = OS_TIMEOUT;
            break;
        }
    }

    return NULL;
}

INT8U OSQPost(OS_EVENT *pevent, void *msg )
{
    int msgid;
    struct ptrmsg ipcmsg; 

    if( pevent->OSEventType != OS_EVENT_TYPE_Q ) {
        return OS_ERR_EVENT_TYPE;
    }

    msgid = (int)pevent->OSEventPtr;

    ipcmsg.mtype = 1;
    ipcmsg.ptr = msg;

    if( msgsnd( msgid, (void *)&ipcmsg, sizeof(void *), IPC_NOWAIT ) < 0 ) {
        // shouldn't happen 
        PERROR( "msgsnd() failed in OSQPost" );
        return OS_ERR_INTERNAL;
    }

    return OS_NO_ERR;
}
#endif

// *****************
//
// Semaphore (Mutex)
//
// *****************

#if OS_MUTEX_EN > 0

#if OS_MUTEX_ACCEPT_EN > 0
INT8U OSMutexAccept(OS_EVENT *pevent, INT8U *err)
{
    int semid;

    // if the mutex is available, claim it and return 1
    // otherwise, return 0
    // errors are returned through *err

    if( pevent == NULL ) {
        *err = OS_ERR_PEVENT_NULL;
        return 0;
    }
    
    if( pevent->OSEventType != OS_EVENT_TYPE_SEM ) {
        *err = OS_ERR_EVENT_TYPE;
        return 0;
    }

    semid = (int)pevent->OSEventPtr;

    // attempt to claim the semaphore with no wait
    if( semop( semid, &sem_claim, 1 ) < 0 ) {
        if( errno == EAGAIN ) {
            // semaphore busy 
            *err = OS_NO_ERR;
            return 0;
        }

        // shouldn't happen 
        // FIXME -- perhaps detect EINTR return from semop()
        PERROR( "semop() failed in OSMutexAccept()" );
        *err = OS_ERR_INTERNAL;
        return 0;
    }

    // we claimed the semaphore!
    *err = OS_NO_ERR;
    return 1;
}
#endif

OS_EVENT *OSMutexCreate(INT8U prio, INT8U *err)
{
    int semid;
    OS_EVENT *evt;

    evt = get_free_event( OS_EVENT_TYPE_SEM );
    if( evt == NULL ) {
        *err = OS_ERR_PEVENT_NULL;
        return NULL;
    }

    semid = semget( IPC_PRIVATE, 1, 0600 );
    if( semid < 0 ) {
        // shouldn't happen 
        PERROR( "semget() failed in OSMutexCreate()" );
        *err = OS_ERR_INTERNAL;
        return NULL;
    }


    if( semctl( semid, 0, SETVAL, 1) < 0 ) {
        // shouldn't happen 
        PERROR( "semctl() failed in OSMutexCreate()" );
        *err = OS_ERR_INTERNAL;
        return NULL;
    }

    *err = OS_NO_ERR;
    evt->OSEventPtr = (void *)semid;

    return evt;
}

#if OS_MUTEX_DEL_EN > 0
OS_EVENT *OSMutexDel(OS_EVENT *pevent, INT8U opt, INT8U *err)
{
    int semid;

    if( pevent == NULL ) {
        *err = OS_ERR_PEVENT_NULL;
        return 0;
    }
    
    if( pevent->OSEventType != OS_EVENT_TYPE_SEM ) {
        *err = OS_ERR_EVENT_TYPE;
        return NULL;
    }

    semid = (int)pevent->OSEventPtr;

    if( semctl( semid, 0, IPC_RMID, 0 ) < 0 ) {
        // shouldn't happen
        *err = OS_ERR_INTERNAL;
        return NULL;
    }

    // free this event structure
    pevent->OSEventType = OS_EVENT_TYPE_UNUSED;

    *err = OS_NO_ERR;
    return NULL;
}
#endif

void OSMutexPend(OS_EVENT *pevent, INT16U timeout, INT8U *err)
{
    INT16U next_sleep;

    // AFAIK there is no such thing as a timeout on a 
    // SysV IPC.  I could use a SIGALRM to wake up the
    // sleeping semop() but I'm nervous about interfering
    // with an application's possible use of that signal.
    //
    // So let's just poll and be happy.
    // 
    // FIXME -- need way to sleep with timeout on semop()
    
    while( 1 ) {
        if( OSMutexAccept( pevent, err ) ) {
            return;
        }

        next_sleep = min(timeout,POLL_DELAY);
        OSTimeDly( next_sleep );
        timeout -= next_sleep;
        if( timeout == 0 ) {
            // failed to claim
            *err = OS_TIMEOUT;
            break;
        }

    }
}

INT8U OSMutexPost(OS_EVENT *pevent)
{
    int semid;

    if( pevent == NULL ) {
        return OS_ERR_PEVENT_NULL;
    }
    
    if( pevent->OSEventType != OS_EVENT_TYPE_SEM ) {
        return OS_ERR_EVENT_TYPE;
    }

    semid = (int)pevent->OSEventPtr;

    // attempt to release the semaphore with no wait
    if( semop( semid, &sem_release, 1 ) < 0 ) {
        // shouldn't happen
        PERROR( "semop() failed in OSMutexPost()" );
        return OS_ERR_INTERNAL; 
    }

    return OS_NO_ERR;
}
#endif

// *****************
//
// Task functions
//
// *****************


INT8U OSTaskCreate( THREAD (*task)(void *pd), void *pdata, OS_STK *ptos, INT8U prio)
{
    int err;

    if( prio > OS_LOWEST_PRIO ) {
        return OS_PRIO_INVALID;
    }

    if( pthread_to_prio[prio] != (pthread_t)0 ) {
        return OS_PRIO_EXIST;
    }

    err = pthread_create( &pthread_to_prio[prio], NULL, task, pdata );
    if( err != 0 ) {
        // shouldn't happen
        PERROR( "pthread_create() failed in OSTaskCreate()" );
        return OS_ERR_INTERNAL;
    }

    return OS_NO_ERR;
}

INT8U OSTaskCreateExt( THREAD (*task)(void *pd),
                      void   *pdata,
                      OS_STK *ptos,
                      INT8U   prio,
                      INT16U  id,
                      OS_STK *pbos,
                      INT32U  stk_size,
                      void   *pext,
                      INT16U  opt)
{
    return OSTaskCreate( task, pdata, ptos, prio );
}


// *****************
//
// Base functions
//
// *****************

void OSInit( void )
{
    int perr;

    epoch_init();

#if (OS_EVENT_EN > 0) && (OS_MAX_EVENTS > 0)
    memset( lnxOSEventTbl, 0, sizeof(lnxOSEventTbl) );
#endif

    // initialize the tables' mutexes
    perr = pthread_mutex_init( &oseventtbl_mutex, NULL );
    ASSERT( perr == 0 );

#if (OS_VERSION >= 251) && (OS_FLAG_EN > 0) && (OS_MAX_FLAGS > 0)
    perr = pthread_mutex_init( &osflagtbl_mutex, NULL );
    ASSERT( perr == 0 );
#endif
}

void OSStart( void )
{
    for(;;);
    // nothing necessary??
}

#endif // SPLINT
