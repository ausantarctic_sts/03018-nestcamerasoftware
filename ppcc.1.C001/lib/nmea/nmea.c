/*
 *	msp430 - The msp430 program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *	Written by Peter Jansen <peter_jan@sts-cvs>
 *
 * This file contains functions used to to manipulate nmeas.
 */

#include <stdio.h>
#include <str.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>

#include <ppcc.h>
#include <mspio.h>
#include <nmea.h>
#include <usart0.h>

uint8_t NmeaCheckSum;

/* Global Master/Slave ID's - set to defaults */
char strMasterID[3] = "00";
char strSlaveID[3]  = "01";

void NmeaPrintf(const char *fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    vuprintf(NmeaPutch, fmt, ap);
    va_end(ap);
}

void NmeaPacketStart(char *cmd)
{
    NmeaCheckSum = (uint8_t)'$';
    NmeaPrintf("$%s%s", strSlaveID, cmd);
}

void NmeaPacketEnd()
{
    NmeaPrintf(",0x%04X", instrumentErrorCode);
    NmeaPrintf("*%02X\r\n", NmeaCheckSum);
}

int8_t NmeaCheckString(char *ch)
{
    uint8_t checkSum;
    int8_t retval;
    int8_t isString;
    char *p;
    char *checkChar;

    retval = 0; /* Assume non valid NMEA string */
    isString = 0;

    if (*ch == NMEA_START_PACKET)
    { 
        checkSum = 0;
        retval = 1; /* Assume no check sum */

        p = ch + 1; /* Skip the Packet Start character */

        checkChar = ch + strlen(ch) - 3; /* this is where is should be */
        
        /* Check for a valid check sum */
        while ((*p != '\0'))
        {
            if (isString == 1)
            {
                if (*p == '\\')
                {
                    checkSum ^= *p++;
                }
                else if (*p == '"')
                {
                    isString = 0;
                }
            }
            else if (*p == '"')
            {
                isString = 1;
            }

            if (*p != '\0')
            {
                checkSum ^= *p++;
            }

            if ((p == checkChar) && (isString == 0))
            {
                if (*p == NMEA_START_CHECKSUM)
                {
                    checkSum -= (uint8_t)stoi(p+1, 16);
                    if (checkSum == 0)
                    {
                        retval = 2; /* Have a check sum */
                    }
                    else
                    {
                        retval = -1; /* Invalid check sum */
                    }
                }
                break;
            }
        }
    } /* Valid NMEA Start */

    return retval;
}   
