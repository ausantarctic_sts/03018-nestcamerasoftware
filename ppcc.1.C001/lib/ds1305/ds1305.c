/*
 *	Copyright (C) 2004 Australian Antarctic Division
 * 
 * This file contains functions used to access the DS1305 SPI RTC
 */
#include <stdio.h>

#ifdef TARGET
#include <io.h>
#endif

#include <ds1305.h>
#include <ppcc.h>

#ifndef DEBUG
# define DEBUG 0
#endif

/*
 * BCD2BIN - Converts 2 x 4 bit Binary Coded Decimal to 8 bit Binary
 */
uint8_t BCD2BIN(uint8_t u8BCD)
{
    return ((u8BCD >> 4) * 10 + (u8BCD & 0xF)); 
}

/* 
 * BIN2BCD - Converts 8 bit Binary to 2 x 4 bit BCD
 */
uint8_t BIN2BCD(uint8_t u8BIN)
{
    return (((u8BIN / 10) << 4) | (u8BIN % 10));
}

/*
 * Powerup & init settings 
 */
uint8_t ds1305PowerUp(void)
{
    uint8_t u8Temp;
    uint8_t u8Error = 0;
    
#ifdef TARGET   
    /* Setup SPI */
    P2OUT &= ~(P2_RTC_CE);                            // set inactive
    P2DIR |= P2_RTC_CE;                               // outputs
    
    P3SEL |= (P3_MOSI | P3_MISO | P3_SCLK);           // Enable SPI pins
    P3DIR |= (P3_MOSI | P3_SCLK);                     // outputs
    P3DIR &= ~P3_MISO;                                // inputs    
    
    /* Set SWRST first */
    U0CTL |= SWRST;
    
    /* Enable the module */
    ME1 |= USPIE0;
    
    /* Init the SPI port - clears LISTEN, I2C (if valid)
     * CHAR = 1 -> 8 bit data
     * SYNC = 1 -> SPI mode
     * MM = 1 -> UART is master
     * SWRST = 1 -> held in reset while configuring bits
     */
    U0CTL = CHAR | SYNC | MM | SWRST;
    
    /* Set Clock phase & slave select
     * CKPH = 0 -> Normal clocking scheme
     * CKPL = 0 -> Inactive level is low, Data out on rising edge
     *             Input data latched on rising edge
     * SSEL = 2 -> Clock source is SMCLK (1MHz)
     * STC = 1  -> 3 pin SPI mode, STE disabled.
     * TXEPT = 1 -> Clear TX buffer flag
     */
    U0TCTL = SSEL_2 | STC | TXEPT;
    
    /* Set baud rate registers - smallest division factor = 2 
     * which results in SPI clock rate = 500kbps */
    U0BR0 = 0x02;
    U0BR1 = 0x00;
    U0MCTL = 0;   /* don't use modulation */
    
    /* Clear port reset - UART is now ready */
    U0CTL &= ~SWRST;
    
    /* clear rx int flag without disturbing Watchdog IRQ flag
     * Applies ESPECIALLY to all bits in IFG1 */
    BICB(IFG1, URXIFG0);   
#endif    
    
    /* Read the control register */
    u8Temp = ds1305Read(DS1305_CONTROL);

    /* If DS1305 is write protected, then unprotect it and set an error */
    if ((u8Temp & DS1305_CTL_WP) == DS1305_CTL_WP)
    {
	ds1305Write(DS1305_CONTROL, u8Temp & ~DS1305_CTL_WP);
	u8Error |= DS1305_CTL_WP;
    }

    /* Re-Read the control register */
    u8Temp = ds1305Read(DS1305_CONTROL);

    /* If Oscillator is not enabled, set an error */
    if ((u8Temp & DS1305_CTL_EOSC) == DS1305_CTL_EOSC)
    {
	u8Error |= DS1305_CTL_EOSC;
    }
    
    return u8Error;
}
    
/*
 * Transmit a byte / receive a byte - Assumes SPI device is ENABLED
 */
uint8_t ds1305TxRx(uint8_t u8TXD)
{
    uint8_t u8RXD;
    
#ifdef TARGET
    /* wait for transmitter to empty */
    while ((U0TCTL & TXEPT) == 0);
    
    /* Send the data byte */
    U0TXBUF = u8TXD;
    
    /* wait for receive to complete */
    while ((IFG1 & URXIFG0) == 0);
    
    /* Read the data and clear the flag */
    u8RXD = U0RXBUF;
    
    return u8RXD;
#endif
#ifdef HOST
    return u8RXD;
#endif
}
    
/*
 * Write a byte to a RTC address
 */
void ds1305Write(uint8_t u8Address, uint8_t u8Data)
{
#ifdef TARGET
    P2OUT |= P2_RTC_CE;
#endif

    /* Write the address with MSB set to indicate write - ignore RX data */
    ds1305TxRx(u8Address | 0x80);
    
    /* Write the data - ignore received data */
    ds1305TxRx(u8Data);

#ifdef TARGET
    P2OUT &= ~P2_RTC_CE;
#endif
}

/* 
 * Read a single byte from an RTC address
 */
uint8_t ds1305Read(uint8_t u8Address)
{
    uint8_t u8RXD;
    
#ifdef TARGET
    P2OUT |= P2_RTC_CE;
#endif
    
    /* Write the address, ignore the received data */
    ds1305TxRx(u8Address);
    
    /* Write dummy value, read the received data */
    u8RXD = ds1305TxRx(0xFF);

#ifdef TARGET
    P2OUT &= ~P2_RTC_CE;
#endif

    return u8RXD;
}

/*
 * ds1305ReadRTC - Read RTC registers into the RTCTimeStruct
 * 
 * If the Hours value is detected as being in AM/PM mode, then
 * it is converted to 24 hour mode and written back.
 * 
 */
void ds1305BCDReadRTC(RTCTimeStruct *RTCTime)
{
    uint8_t u8Temp;
    
    RTCTime->u8Second = ds1305Read(DS1305_SECOND);
    RTCTime->u8Minute = ds1305Read(DS1305_MINUTE);
    u8Temp = ds1305Read(DS1305_HOUR);

    /* Check that RTC is not in AM/PM Mode. Convert to 24Hr
     * mode if it is.
     */
    if ((u8Temp & DS1305_HOUR_12) == DS1305_HOUR_12)   
    {
	/* If Bit 6 set -> AM/PM mode */
	RTCTime->u8Hour = BCD2BIN(u8Temp & 0x1F);
	
	/* Bit 5 set indicates PM */
	if ((u8Temp & DS1305_HOUR_PM) == DS1305_HOUR_PM)  
	    RTCTime->u8Hour += 12;
	
	/* Write back hours to put it into 24 hour mode */
	ds1305Write(DS1305_HOUR, BIN2BCD(RTCTime->u8Hour));
	RTCTime->u8Hour = BIN2BCD(RTCTime->u8Hour);
    }
    else
	/* Already in 24 Hour mode */
	RTCTime->u8Hour = u8Temp;
    
    RTCTime->u8DayOfMonth= ds1305Read(DS1305_DAYOFMONTH);
    RTCTime->u8Month = ds1305Read(DS1305_MONTH);
    RTCTime->u8Year = ds1305Read(DS1305_YEAR);
}

/*
 * ds1305WriteRTC - Write RTC registers from RTCTimeStruct
 */
void ds1305BCDWriteRTC(RTCTimeStruct *RTCTime)
{
    ds1305Write(DS1305_YEAR, RTCTime->u8Year);
    ds1305Write(DS1305_MONTH, RTCTime->u8Month);
    ds1305Write(DS1305_DAYOFMONTH, RTCTime->u8DayOfMonth);
    ds1305Write(DS1305_HOUR, RTCTime->u8Hour);
    ds1305Write(DS1305_MINUTE, RTCTime->u8Minute);
    ds1305Write(DS1305_SECOND, RTCTime->u8Second);
}

/* Read various sizes from NVRAM */
uint8_t  NVRAMu8Read(uint8_t u8Addr)
{
    return ds1305Read(DS1305_RAM_START + u8Addr);
}

uint16_t NVRAMu16Read(uint8_t u8Addr)
{
    uint16_t u16;

    u16 = ds1305Read(DS1305_RAM_START + u8Addr + 0);
    u16 = (u16 << 8) | ds1305Read(DS1305_RAM_START + u8Addr + 1);

    return u16;
}

extern uint32_t NVRAMu32Read(uint8_t u8Addr)
{
    uint32_t u32;
    
    u32 = ds1305Read(DS1305_RAM_START + u8Addr + 0);
    u32 = (u32 << 8) | ds1305Read(DS1305_RAM_START + u8Addr + 1);
    u32 = (u32 << 8) | ds1305Read(DS1305_RAM_START + u8Addr + 2);
    u32 = (u32 << 8) | ds1305Read(DS1305_RAM_START + u8Addr + 3);
    
    return u32;
}

void NVRAMu8Write(uint8_t u8Addr, uint8_t u8Data)
{
    ds1305Write(DS1305_RAM_START + u8Addr, u8Data);
}

void NVRAMu16Write(uint8_t u8Addr, uint16_t u16Data)
{
    ds1305Write(DS1305_RAM_START + u8Addr + 1, (uint8_t)(u16Data & 0xFF));
    ds1305Write(DS1305_RAM_START + u8Addr + 0, (uint8_t)(u16Data >> 8));
}

void NVRAMu32Write(uint8_t u8Addr, uint32_t u32Data)
{
    ds1305Write(DS1305_RAM_START + u8Addr + 3, (uint8_t)(u32Data & 0xFF));
    ds1305Write(DS1305_RAM_START + u8Addr + 2, (uint8_t)((u32Data >> 8) & 0xFF));
    ds1305Write(DS1305_RAM_START + u8Addr + 1, (uint8_t)((u32Data >> 16) & 0xFF));
    ds1305Write(DS1305_RAM_START + u8Addr + 0, (uint8_t)((u32Data >> 24) & 0xFF));    
}
