/*
 *  EFM - The "EFM" program.
 *  Copyright (C) 2003 Australian Antarctic Division
 *  Written by peter.jansen@aad.gov.au
 *
 * This file contains functions used to to manipulate flashs.
 */

#include <stdio.h>

#include <ppcc.h>
#include <ucosii.h>
#include <flash.h>

#ifdef TARGET
#include <io.h>
#include <signal.h>
#include <string.h>
#endif

#ifdef TARGET
unsigned char scratch[512] __attribute__((section(".flashScratch")));
#endif
#ifdef HOST
unsigned char scratch[512];
#endif

#ifndef DEBUG
# define DEBUG 0
#endif

extern unsigned char *_etext;
extern unsigned char *__intelboot_start;

/*
 * NAME
 *  flashErase
 *
 * SYNOPSIS
 *  void flashErase(void);
 *
 * DESCRIPTION
 *  The flashErase function is used to erase a flash segment
 *
 * RETURNS
 *  void
 */

void flashErase(unsigned char mode, unsigned char *addr)
{    
#if OS_CRITICAL_METHOD == 3
    OS_CPU_SR cpu_sr;
#endif

#if DEBUG >= 3
    printf("flashErase %p\r\n", addr);
#endif

    if (mode != ERASE)
    {
        return;
    }

    OS_ENTER_CRITICAL();
#ifdef TARGET    

    /* Set flash timing generator to use SMCLK (1Mhz) and divide by
     * 3 to get Ftg = 333KHz which is within the 257KHz .. 476KHz
     * requirements
     */
    FCTL2 = FWKEY | FSSEL_SMCLK | FN0 | FN1;

    FCTL1 = FWKEY | mode;
    *addr = 0xff; /* dummy write */
    FCTL1 = FWKEY;
    
    while (FCTL3 & BUSY); /* Wait for write */
        
#endif
    /* Invalidate DCO tuner control because we might have been
     * waiting for a period longer than one OS tick */
    DCOTune.u8Control = 0;
    OS_EXIT_CRITICAL();
}

/*
 * NAME
 *  flashUnlock
 *
 * SYNOPSIS
 *  void flashUnlock(uint16_t FWKEY);
 *
 * DESCRIPTION
 *  Unlock the flash Memory, a reset is assumed to lock the flash again
 *  also setup the flash timing generator.
 *  This function must be called with the correct flash KEY so as to keep
 *  the unlock code separate to reduce the single function call chance of
 *  corrupting the flash.
 *
 * RETURNS
 *  void
 */

void flashUnlock(uint16_t key)
{
    /* Set flash timing generator to use SMCLK (1MHz) and divide by
     * 3 to get Ftg = 333KHz which is within the 257KHz .. 476KHz
     * requirements
     */
#ifdef TARGET
    FCTL2 = FWKEY | FSSEL_SMCLK | FN0 | FN1;
#endif

#ifdef TARGET
    FCTL3 = key; /* Lock = 0 */
#endif
#ifdef HOST
    key = key; /* compiler waring */
#endif
}

int8_t flashLocked(void)
{
#ifdef TARGET
    return ((FCTL3 & LOCK) != 0);
#endif
#ifdef HOST
    return 0;
#endif
}


/*
 * NAME
 *  flashWriteByte
 *
 * SYNOPSIS
 *  unsigned char flashWriteByte(void);
 *
 * DESCRIPTION
 *  The flashWriteByte function is used to write a byte to a flash segment
 *
 * RETURNS
 *  false if written data not equals actual data
 */

unsigned char flashWriteByte(unsigned char *addr, unsigned char data)
{
#if OS_CRITICAL_METHOD == 3
     OS_CPU_SR cpu_sr;
#endif

    OS_ENTER_CRITICAL();

#ifdef TARGET
    FCTL1 = FWKEY | WRT;
    *addr = data; /* dummy write */
    FCTL1 = FWKEY;
    
    while (FCTL3 & BUSY); /* Wait for write */
        
#endif
#ifdef HOST
    *addr = data;
#endif

    /* Invalidate DCO tuner control because we might have been
     * waiting for a period longer than one OS tick */
    DCOTune.u8Control = 0;
    OS_EXIT_CRITICAL();

    return (data != *addr);
}

/*
 * NAME
 *  flashWriteWord
 *
 * SYNOPSIS
 *  unsigned char flashWriteWord(void);
 *
 * DESCRIPTION
 *  The flashWriteWord function is used to write a word to a flash segment
 *
 * RETURNS
 *  false if written data not equal to the actual data
 */

unsigned char flashWriteWord(unsigned int *addr, unsigned int data)
{
#if OS_CRITICAL_METHOD == 3
    OS_CPU_SR cpu_sr;
#endif

    OS_ENTER_CRITICAL();

#ifdef TARGET
    FCTL1 = FWKEY | WRT;
    *addr = data; /* dummy write */
    FCTL1 = FWKEY;
    
    while (FCTL3 & BUSY); /* Wait for write */
        
#endif
#ifdef HOST
    *addr = data;
#endif

    /* Invalidate DCO tuner control because we might have been
     * waiting for a period longer than one OS tick */
    DCOTune.u8Control = 0;
    OS_EXIT_CRITICAL();

    return (data != *addr);
}


/*
 * NAME
 *  flashWriteBlock
 *
 * SYNOPSIS
 *  unsigned char flashWriteBlock(unsigned char *addr, unsigned int len, unsigned char *data);
 *
 * DESCRIPTION
 *  The flashWriteBlock function is used to write a block to a flash segment
 *
 * RETURNS
 *  unsigned char - flag set if a write failed
 */

unsigned char flashWriteBlock(unsigned char *addr, unsigned int len, unsigned char *data)
{
    unsigned char ret;

#if DEBUG >= 3
    printf("flashWriteBlock from %p length %u to %p\r\n",  data, len, addr);
#endif

    ret = 0;
    while(len > 0)
    {
        ret = ret | flashWriteByte(addr, *data);
        len--;
        addr++;
        data++;
    }

    return ret;
}

void flashEraseFromLen(unsigned char *start, size_t len)
{
    unsigned char *tempBlock;
    unsigned char *blockStart, *blockEnd;
    unsigned char *end;

    /* check for a valid length */
    if (len == 0) return;

#ifdef TARGET
    /* Check for a valid address */
    if (((start >= (unsigned char *)&__intelboot_start) 
                && (start <= (unsigned char *)&_etext)) || 
            ((start + len) > (unsigned char *)0xffe0))
    {
#if DEBUG >= 1        
        printf("flashEraseFromLen: address in text or interrupt vectors\r\n");
#endif
        return;
    }
#endif

    tempBlock = scratch;
    flashErase(ERASE, tempBlock);

    blockStart = (unsigned char *)((unsigned int)start & 0xfe00); /* Start of the block */
    blockEnd = blockStart + 0x1ff;
    end = start + len - 1;
#ifdef HOST
    blockStart = start;
    blockEnd = end;
#endif

#if DEBUG >= 1
    printf("flashEraseFromTo start %p end %p blockStart %p blockEnd %p len %d\r\n", start, end, blockStart, blockEnd, len);
#endif

    /* Copy from the block to be erased into the tempory block */
    
    flashWriteBlock(tempBlock, start - blockStart, blockStart); /* before the area */
    while (end > blockEnd)
    {
        flashErase(ERASE, blockStart);
        flashWriteBlock(blockStart, 0x200, tempBlock);

        /* Calculate the address of the next block */

        blockStart += 0x200;
        blockEnd += 0x200;
#if DEBUG >= 3
        printf("flashEraseFromTo blockStart %p blockEnd %p\r\n", blockStart, blockEnd);
#endif
        
        flashErase(ERASE, tempBlock);
    }
    flashWriteBlock(tempBlock + (end - blockStart) + 1, blockEnd - end, end + 1); /* after the new area */

    flashErase(ERASE, blockStart);

    /* Now copy the data back */

#ifdef TARGET
    /* for TARGET we re-write the hole block */
    len = 0x200;
#endif
    
    flashWriteBlock(blockStart, len, tempBlock);
}

void flashUpdate(unsigned char *addr, size_t len, unsigned char *data)
{
    unsigned char *new;
    unsigned char erased;
    unsigned int i;
   
#if DEBUG >= 1        
    printf("flashUpdate from %p length %u to %p\r\n",  data, len, addr);
#endif

#ifdef TARGET
    /* Check for a valid address */
    if (((addr >= (unsigned char *)&__intelboot_start) 
                && (addr <= (unsigned char *)&_etext)) || 
            ((addr + len) > (unsigned char *)0xffe0))
    {
#if DEBUG >= 1        
        printf("flashUpdate: address in text or interrupt vectors\r\n");
        printf("flashUpdate: address %p %p %p %p\r\n", addr, &__intelboot_start, &_etext, addr+len);
#endif
        return;
    }
#endif

    /* check if data is already erased */

    new = addr;
    erased = 0;
    for(i = 0; i < len; i++)
    {
        erased |= ~(*new);
        new++;
    }

    if (erased != 0)
    {
        flashEraseFromLen(addr, len);
    }

#if DEBUG >= 3
    printf("flashUpdate : write data %p %d %p\r\n", addr, len, data);
#endif
    
    flashWriteBlock(addr, len, data); /* just write the data */

    return;
} 
