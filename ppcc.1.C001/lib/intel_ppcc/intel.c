/*
 * Intel hex file receiving program for msp430
 *
 * Copyright 2002 Australian Antarctic Division
 * Peter Jansen
 *
 * 20 September 2003
 * 29 September 2004 - Modified for Penguin Population Camera Controller
 * 06 October 2004   - Renamed to Intelboot
 * 09 September 2005 - Fixed reference to usart1
 * 18 August 2008    - Updated for Revision 4/5 Hardware
 *                   - do not use XT2 (DCO only)
 * 21 August 2008    - Add DCO tuning with 32khz LFXTAL on PPCC
 *                     without XT2 and to remove DCO variations
 */

#include <stdio.h>
#include <signal.h>
#include <ppcc.h>

#include <usart1.h> /* For its macros */

#ifdef TARGET
#include <io.h>
#endif

#include <intel.h>

#ifdef TARGET
#define INTELBOOT __attribute__((section(".intelboot")))
#endif
#ifdef HOST
#define INTELBOOT
#endif

/* Comms setup */

unsigned char DataBuffer[16];
unsigned char Length;

#ifdef TARGET
register unsigned char i asm("r11");
register unsigned char Type asm("r10");
register unsigned char State asm("r9");
register unsigned char *Address asm("r8");
register unsigned int  *AddressW asm("r5");
register unsigned char Count asm("r7");
register unsigned char CheckSum asm("r6");

/* Variables that come from the linker script */

extern unsigned int *__text_start;
extern unsigned int *__intelboot_start, *__intelboot_end;

#endif
#ifdef HOST
unsigned char i, Type, State, *Address;
unsigned char Count, CheckSum;
#endif

/*
 * intelStartupDCOTune - Tunes the DCO at startup.
 * 
 * This routine is specifically for the intel bootloader ONLY!
 * We can't use the main one, or the main variables because
 * they are erased
 * 
 * Uses the watchdog as an interval timer without interrupts to
 * measure the DCO which is driven by SMCLK. It then sets the DCO 
 * control bits accordingly.
 * 
 * Assumes 
 *   1. SMCLK = DCO which drives TimerB
 *   2. ACLK  = 32768KHz
 *   3. TimerB has not been configured
 *   4. Watchdog has not been configured
 *   
 * Performs DCO measurement/adjustment until the the last STABLE_COUNT 
 * consecutive error values are less than the ERROR_LIMIT, then it quits
 * leaving the global DCO tune values to be picked up by the main
 * ucosii time tick hook tune routine.
 * 
 */
INTELBOOT void intelStartupDCOTune(void)
{
#ifdef TARGET        
    int iStable;
    unsigned int uiTimerB;
    
    /* Clear Watchdog timer flag */
    IFG1 &= ~WDTIFG;
    
    /* configure watchdog as interval timer, 16ms timeout */
    WDTCTL = WDT_ADLY_16;
    
    /* Setup the timer b, driven by SMCLK, free running */
    TBCTL = TBCLR | ID_DIV1 | MC_CONT | TBSSEL_SMCLK | CNTL_0;
    TBCTL &= ~TBCLR;
    TBCTL |= MC_CONT;

    /* init control variables */
    DCOTune.i16Error = 32767;
    DCOTune.u8Control = 0;
    iStable = 0;
    
    while (iStable < DCOTUNE_STABLE_STARTUP)
    {
	/* wait for watchdog timeout */
	while ((IFG1 & WDTIFG) == 0);
	
	/* clear watchdog timer flag */
	IFG1 &= ~WDTIFG;
	
	/* grab timerb */
	uiTimerB = TBR;

	/* if we have captured two timers in a row, calculate the period */
	if (DCOTune.u8Control > 0)
	    DCOTune.u16Period = uiTimerB - DCOTune.u16Last;
	DCOTune.u16Last = uiTimerB;
        
	/* If we have two consecutive captures and thus a valid period,
	 * we can perform a DCOTune
	 */
	if (DCOTune.u8Control > 1)
	{	    
	    /* calc error */
	    DCOTune.i16Error = DCOTune.u16Period - (SMCLK * WDTCLKDIV)/ACLK;
	    
	    /* if the error was below our threshold increment the 
	     * stable count, otherwise reset and try again
	     */
	    if (DCOTune.i16Error < DCOTUNE_ERROR_MAX)
		iStable++;
	    else
		iStable = 0;
	    
	    /* perform the feedback part of the control loop */
	    DCOTune.i16NewDCO = DCOCTL - (DCOTune.i16Error/64);
	    DCOTune.u8Rsel = BCSCTL1 & (RSEL0 | RSEL1 | RSEL2);
	    
	    if (DCOTune.i16NewDCO < 0)
	    {
		if (DCOTune.u8Rsel > 0)
		{
		    BCSCTL1--;
		    DCOCTL = 0x60;
		}
	    }
	    else if (DCOTune.i16NewDCO > 255)
	    {
		if (DCOTune.u8Rsel < 7)
		{
		    BCSCTL1++;
		    DCOCTL = 0x60;
		}
	    }
	    else
	    {
		DCOCTL = (unsigned char)(DCOTune.i16NewDCO & 0xFF);
	    }
	}
    
	if (DCOTune.u8Control < 4)
	    DCOTune.u8Control++;    
    }   

    /* invalidate the DCO tune because it could be awhile
     * before the main ucosii time tick hook DCO tune is activated
     */
    DCOTune.u8Control = 0;    
#endif
}


/* Put a byte out the serial port, or stdout in the UNIX case */

INTELBOOT void PutByte(unsigned char ch)
{
#ifdef TARGET
    while (!(IFG2 & UTXIFG1)); /* wait for TX buffer to empty */

    TXBUF1 = ch;

    while ((UTCTL1 & TXEPT) == 0);
#endif
#ifdef HOST
  putchar(ch);
#endif
}

/* Get a byte from the serial port, or stdin in the UNIX case */

INTELBOOT unsigned char GetByte(void)
{
#ifdef TARGET
    while ((IFG2 & URXIFG1) == 0); /* Wait for a character */

    return RXBUF1;
#endif
#ifdef HOST
    return (unsigned char)getchar();
#endif
}

INTELBOOT void PutS(char *string)
{
    while (*string != '\0')
    {
        PutByte(*string++);
    }
}

/* Get a hex byte from the serial port, and increment the check sum */

INTELBOOT unsigned char GetHex(void)
{
    unsigned char bh, byte;

    bh = GetByte() - '0';
    if (bh > 10) bh = bh - ('A' - '0' - 10);

    byte = GetByte() - '0';
    if (byte  > 10) byte = byte - ('A' - '0' - 10);

    byte = (bh << 4) + byte;

    CheckSum = CheckSum + byte;

    return byte;
}

/* Write data to memory */

INTELBOOT void WriteBuffer(void)
{
#ifdef TARGET
    Count = 0;

    while (Count < Length)      /* while there is more to write... */    
    {	
        if (Address < (unsigned char *)0xfffe)
        {
            FCTL1 = FWKEY | WRT; /* WRT = 1 */
            *Address = DataBuffer[Count]; /* Write the data */
            FCTL1 = FWKEY; /* WRT = 0 */
        }
        Address++;
        Count++;
    }
#endif /* TARGET */
#ifdef HOST
    Count = 0;

    printf("Writing to 0x%p Data ", Address);
    while (Count < Length)
    {
        printf("0x%02x%02x ", DataBuffer[Count+1], DataBuffer[Count]);
        Count += 2;
    }
    printf("\n");
#endif
}


#ifdef TARGET
INTELBOOT interrupt(RESET_VECTOR) __attribute((naked)) start_routine(void)
{
    if (__text_start == (unsigned int *)0x40b2)
    {
        asm("mov #__text_start, R0"); /* Jump to normal start routine */
    }

    FCTL3 = FWKEY; /* Unlock flash */

    WDTCTL = WDTPW | WDTHOLD;          // stop Watchdog
    
    asm("mov #__stack, r1"); /* Load the stack pointer */

    // PPCC Revision 5 doesn't have XT2, so need to use DCO at 1MHz
    // Perform a DCO tune using the 32Khz XTAL as a reference
    intelStartupDCOTune();
        
    /* Serial initialization */
    UCTL1 = SWRST; /* Reset the USART */
    UCTL1 = USART_8N1; /* Set the number of chars etc */
    UTCTL1 = USART1_BRSEL; /* Set the baud rate clock */

    /* load the modulation & baudrate divisor registers */
    UMCTL1 = USART1_BAUD_MOD(CONSOLE_BAUD); 
    UBR11 = (uint8_t)(USART1_BAUD_DIV(CONSOLE_BAUD) >> 8);
    UBR01 = (uint8_t)(USART1_BAUD_DIV(CONSOLE_BAUD) >> 0);

    ME2 |= UTXE1 + URXE1; /* Enable USART 1 */
    P3SEL |= BV(6) + BV(7); /* Set Port 3 for USART 1 */
    URCTL1 = 0; /* Init the Receiver control register */

    PutS("\nIntel Download (PPCC P1-RS232) " __DATE__ " " __TIME__);

    Download();
}
#endif

#ifdef TARGET
INTELBOOT void __attribute__ ((naked)) Download(void)
#endif
#ifdef HOST
void Download(void)
#endif
{
#ifdef TARGET 
    dint(); /* Disable any old interrupts */
    IE1 = 0;

    asm("mov #__stack, r1"); /* Load the stack pointer */

    IE1 = 0; /* Disable NMI, OF, ACCV */
    WDTCTL = WDTPW | WDTHOLD; /* Disable the watchdog */

    /* Set flash timing generator to use SMCLK (1 MHz) and divide by
     * 3 to get Ftg = 333 kHz which is within the 257 kHz .. 476 kHz
     * requirements
     */
#ifdef TARGET
    FCTL2 = FWKEY | FSSEL_SMCLK | FN1 | FN0;
#endif

    /* Erase all segments of flash */

    PutS("\r\nErasing, Please wait ... ");

    Address = (unsigned char*)0x1000; /* info memory A */

    FCTL1 = FWKEY | ERASE;
    *Address = 0; /* Dummy write */
    FCTL1 = FWKEY;

    Address = (unsigned char*)0x1080; /* info memory B */

    FCTL1 = FWKEY | ERASE;
    *Address = 0; /* Dummy write */
    FCTL1 = FWKEY;

    /* Erase the code and stored tabled */
    
    /* Assumes that text section immediately follows bootloader 
     * and bootloader is located at the bottom of flash memmory */

    for (Address = (unsigned char *)&__text_start;
         Address >= (unsigned char *)&__text_start; /* ie until it rolls over */
         Address = (unsigned char *)((unsigned int)Address + 256))
    {
        FCTL1 = FWKEY | ERASE;
        *Address = 0; /* Dummy write */
        FCTL1 = FWKEY;
    }

    /* Restore the reset vector to start above */

    AddressW = (unsigned int *)0xfffe;
    FCTL1 = FWKEY | WRT; /* WRT = 1 */
    *AddressW = (unsigned int)start_routine; /* Write the data */
    FCTL1 = FWKEY; /* WRT = 0 */

#endif
#ifdef HOST
    printf("Intel Download\n");
#endif
    PutS("Send Intel Hex file now!\r\n");

    /* Read the Intel Hex file from serial port */     

    State = 0;

    while (State == 0)
    {
        /* Wait for ':' */

        while (GetByte() != ':');

        /* Get Count */

        CheckSum = 0; /* Reset the check sum for this line */
        Count = GetHex(); /* Also increments the checksum */
#ifdef HOST
        printf("Count %d\n", Count);
#endif
        /* Get address */

        Address = (unsigned char *)(GetHex() << 8); /* Get high byte */
        Address = (unsigned char *)((unsigned int)Address + GetHex());
#ifdef HOST
        printf("Address 0x%p\n", Address);
#endif            

        /* Get Type */

        Type = GetHex();
#ifdef HOST
        printf("Type %d\n", Type);
#endif

        /* Get Data */

        i = 0;
        while (i < Count)
        {
            DataBuffer[i] = GetHex(); /* Get the data */
#ifdef HOST
            /* printf("Data %d 0x%02x\n", i, DataBuffer[i]); */
#endif            
            i++;
        }

        /* Get Check Sum this updates the CheckSum value */

        (void)GetHex();
#ifdef HOST
        printf("Checksum calculated %d\n", CheckSum);
#endif            
        if (CheckSum != 0)
        {
            State = 2;
        }
        else
        {
            switch (Type)
            {
                case 0: /* Data Record */
                    Length = Count;
#ifdef TARGET
                    if ((Address < (unsigned char *)&__intelboot_start) || 
			(Address > (unsigned char *)&__intelboot_end))
#endif
                    {
                        WriteBuffer(); /* Write data to memory */
                        PutByte('.');
                    }
                    break;
                case 1: /* End of file record */
                    State = 1;
                    break;
            }
        }
    }

    if (State == 1)
    {
        PutS("\n\rOK\n\r");
    }
    else
    {
        PutS("\n\rERR\r\n");
    }
#ifdef TARGET
    WDTCTL = WDTPW; /* Enable WD */
    WDTCTL = 0; /* this will force a reset */
    while(1);
#endif
}

