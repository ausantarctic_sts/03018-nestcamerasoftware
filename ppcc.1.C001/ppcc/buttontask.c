/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by kym_new@localhost
 *
 * This file contains functions used to to manipulate buttonss.
 */

#include <stdint.h>
#include <limits.h>

#include <ppcc.h>
#include <ucosii.h>

#include <ppcc/main.h>
#include <ppcc/servo.h>
#include <ppcc/camera.h>
#include <ppcc/util.h>
#include <ppcc/buttontask.h>

#ifdef TARGET
# include <signal.h>
#endif

#define BUTTON1 1
#define BUTTON2 2
#define DEBOUNCE_COUNT OS_TICKS_PER_SEC
#define BUTTON_CLOSED 1
#define BUTTON_OPEN   0

OS_STK ButtonTaskStk[BUTTON_TASK_STK_SIZE];
OS_EVENT *ButtonEventSem;
volatile uint8_t u8B1State;
volatile uint8_t u8B2State;
volatile unsigned int uiB1Count;
volatile unsigned int uiB2Count;

/*
 * Check button press - debounce and set semaphore if pressed
 * 
 */
void buttonCheck(void)
{
#ifdef TARGET
    /* Check button levels - pressed if low */
    if ((P1IN & P1_BUTTON1) == P1_BUTTON1)  // B1 OPEN
    {
	if (u8B1State == BUTTON_OPEN)
	    uiB1Count = 0;
	else if (u8B1State == BUTTON_CLOSED)
	    uiB1Count++;
    }
    else  // B1 CLOSED
    {
	if (u8B1State == BUTTON_OPEN)
	    uiB1Count++;
	else if (u8B1State == BUTTON_CLOSED)
	    uiB1Count = 0;
    }
        
    /* Has B1 counter reached threshold? */
    if (uiB1Count >= DEBOUNCE_COUNT)
    {
	if (u8B1State == BUTTON_OPEN)
	    u8B1State = BUTTON_CLOSED;
	else if (u8B1State == BUTTON_CLOSED)
	    u8B1State = BUTTON_OPEN;
	
	uiB1Count = 0;
	
	/* Set semaphore */
	OSSemPost(ButtonEventSem);
    }

    /************************************/
    
    /* Check button levels - pressed if low */
    if ((P1IN & P1_BUTTON2) == P1_BUTTON2)  // B2 OPEN
    {
	if (u8B2State == BUTTON_OPEN)
	    uiB2Count = 0;
	else if (u8B2State == BUTTON_CLOSED)
	    uiB2Count++;
    }
    else  // B2 CLOSED
    {
	if (u8B2State == BUTTON_OPEN)
	    uiB2Count++;
	else if (u8B2State == BUTTON_CLOSED)
	    uiB2Count = 0;
    }
        
    /* Has B2 counter reached threshold? */
    if (uiB2Count >= DEBOUNCE_COUNT)
    {
	if (u8B2State == BUTTON_OPEN)
	    u8B2State = BUTTON_CLOSED;
	else if (u8B2State == BUTTON_CLOSED)
	    u8B2State = BUTTON_OPEN;
	
	uiB2Count = 0;
	
	/* Set semaphore */
	OSSemPost(ButtonEventSem);
    }

#endif
}

void ButtonTask(void *pdata)
{
#ifdef HOST
    pdata = pdata;
#endif
    
    int iError;
    INT8U u8Err;
    
#ifdef TARGET
    /* Setup I/O Port pins as inputs */
    P1DIR &= ~(P1_BUTTON1 | P1_BUTTON2);
    P1SEL &= ~(P1_BUTTON1 | P1_BUTTON2);

    /* Ensure interupts _NOT_ set */
    P1IE &= ~(P1_BUTTON1 | P1_BUTTON2);
    
    /* Clear counters */
    uiB1Count = 0;
    uiB2Count = 0;
    
    /* Initialise button state to current values */
    if ((P1IN & P1_BUTTON1) == P1_BUTTON1)
	u8B1State = BUTTON_OPEN;
    else
	u8B1State = BUTTON_CLOSED;
    
    if ((P1IN & P1_BUTTON2) == P1_BUTTON2)
	u8B2State = BUTTON_OPEN;
    else
	u8B2State = BUTTON_CLOSED;
    
#endif
    
    /* Create semaphore */
#ifdef TARGET
    ButtonEventSem = OSSemCreate(0); /* Blocking */
#endif
    
    while(1)
    {
#ifdef TARGET
	/* Wait forever for a button event */
	OSSemPend(ButtonEventSem, 0, &u8Err);
#endif
	iError = CC_NO_ERROR;
	
	if (u8B1State == BUTTON_CLOSED)
	{	
	    /* BUTTON 1 - Does a full picture cycle */		
	    OSMutexPend(HardwareMutex, 0, &u8Err);
	    
	    iError = controlCamera(CC_STAGE_POWERON_DELAY);
	    
	    if ((iError == CC_NO_ERROR) &&
		(controlCamera(CC_STAGE_QUERY) == CC_CAMERA_ON))
	    {
		iError = controlServo(SERVO_ON);
		
		if (iError == CC_NO_ERROR)
		{
		    iError = controlServo(SERVO_OPEN);
		    
		    if (iError == CC_NO_ERROR)
			iError |= controlCamera(CC_STAGE_SHOOT_DELAY);
		}
	    }
	    
	    /* Turn off camera */
	    iError |= controlCamera(CC_STAGE_POWEROFF);
	    
	    /* Close shutter */
	    iError |= controlServo(SERVO_CLOSE);
	    
	    /* turn off servo */
	    iError |= controlServo(SERVO_OFF);
	    
	    OSMutexPost(HardwareMutex);
	}
	    
	if (u8B2State == BUTTON_CLOSED)
	{
	    /* BUTTON 2 - DOES A POWER TOGGLE */	    
	    OSMutexPend(HardwareMutex, 0, &u8Err);
#ifdef TARGET
	    if (CC_CAMERA_ON == controlCamera(CC_STAGE_QUERY_POWER))
		controlCamera(CC_STAGE_POWEROFF);
	    else
		controlCamera(CC_STAGE_POWERON);
#endif	    
	    OSMutexPost(HardwareMutex);
	}
    }
}
