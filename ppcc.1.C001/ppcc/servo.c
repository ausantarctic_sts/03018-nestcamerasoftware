/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2005 Australian Antarctic Division
 *	Written by kym_new@localhost
 *
 * This file contains functions used to to manipulate servos.
 * 
 * Updated 2008-05-20 : For Revision 4 hardware (no servo sensing)
 *                      Servo control signal is inverted due to buffer
 * 
 */

#include <ppcc/servo.h>

#include <ppcc.h>
#include <ucosii.h>
#include <flash.h>
#include <adc12.h>

#include <ppcc/main.h>
#include <ppcc/util.h>

#ifndef DEBUG
# define DEBUG 0
#endif

#define SERVO_PAUSE_ON        20  /* delays in OS_TICKS */
#define SERVO_PAUSE_OFF       OS_TICKS_PER_SEC 
#define SERVO_PAUSE_STEP       2
#define SERVO_CLOSE_BACKUP    30  /* amount to backoff from close/opens */
#define SERVO_FAULT_BACKUP   200  /* amount to backoff from faults */

// Initialise timer A which drivers the servo output on Port 1.6 (TA1)
void servoInit(void)
{
#ifdef TARGET
    // Setup Timer A to generate a 50Hz signal,
    // with output compare to drive a active high PWM signal 
    // with pulse length from 0.9ms to 2.1ms
    
    // TASSELx = 10 : Source timer A from SMCLK (1MHz)
    // IDx = 00     : Input divider by 1
    // MCx = 01     : repeat up mode to TACCR0
    TACTL = TASSEL1 | MC0;
	
    // Configure rollover at 20ms, eg 20,000 x 1us
    TACCR0 = 20000;
    
    // Set Output TA1 to go high when TAR > TACCR1
    // (signal is inverted by external mosfet)
    TACCTL1 = OUTMOD_3;
    
    // Set output PWM to closed position.
    TACCR1 = configTable.iServoClosed;
#endif
}

/*
 * Controls VServo 
 * 
 */
int setVServo(int8_t i8Level)
{        
#ifdef TARGET
    if (i8Level == SERVO_OFF)
    {
	BICB(P6OUT, P6_VSERVO_ENABLE);
    }
    else if (i8Level == SERVO_ON)
    {
	/* check input level first - VInput must be above minimum */
	if (readVInput() > configTable.fVInputServo)
	{
	    BISB(P6OUT, P6_VSERVO_ENABLE);
	    instrumentErrorCode &= ~IEC_VINPUT_FAULT;
	}
	else
	{
	    instrumentErrorCode |= IEC_VINPUT_FAULT;
	    return SERVO_VINPUT_ERROR;
	}
    }
#endif
    return SERVO_NO_ERROR;
}

/*
 * Controls power to the servo.
 * 
 */
int servoPower(int iPower)
{
    int iError;
    
    if (iPower == SERVO_ON)
    {
	/* Stop Sleeping */
	uiSleep |= SLEEP_SERVO_ACTIVE;

#ifdef TARGET	
	/* Enable signal & make output */
	BISB(P1SEL, P1_SERVO_SIGNAL);
	BISB(P1DIR, P1_SERVO_SIGNAL);
#endif
	OSTimeDly(SERVO_PAUSE_ON);
	
	/* turn on VServo */
	iError = setVServo(SERVO_ON);
    }
    else /* turn off */
    {
	/* turn off vservo */
	iError = setVServo(SERVO_OFF);
	
	OSTimeDly(SERVO_PAUSE_OFF);

#ifdef TARGET		
	/* Turn off PWM & Make signal an output low */
	BICB(P1SEL, P1_SERVO_SIGNAL);
	BICB(P1OUT, P1_SERVO_SIGNAL);
	BISB(P1DIR, P1_SERVO_SIGNAL);
#endif
	/* Re-enable Sleeping */
	uiSleep &= ~SLEEP_SERVO_ACTIVE;
    }
    
    return iError;
}

/*
 * Sets the pulse width of the servo control output.
 * 
 * uiWidth is an unsigned int which is width of pulse in microseconds
 * 
 */ 
void servoWidth(unsigned int uiWidth)
{
#ifdef TARGET
    TACCR1 = uiWidth;
#endif
}

/*
 * Returns the current servo pulse width
 */
unsigned int servoRead(void)
{
#ifdef TARGET
    return (TACCR1 & 0xFFFF);
#endif
#ifdef HOST
    return 0;
#endif
}

/*
 * Control servo - updated for Rev 5 hardware without shutter
 * position detection
 */
int controlServo(int iOpenClose)
{   
    int iServoWidth;
    int iError = SERVO_NO_ERROR;
    
    if (iOpenClose == SERVO_ON)
    {
	iError = servoPower(SERVO_ON);
    }
    else if (iOpenClose == SERVO_OFF)
    {
	iError = servoPower(SERVO_OFF);
    }
    else if (iOpenClose == SERVO_CLOSE)
    {
	iError = servoPower(SERVO_ON);
	
	if (iError == SERVO_NO_ERROR)
	{
	    iServoWidth = servoRead();
	    
	    /* move from open to over-closed to
	     * let the bristles settle properly */
	    do 
	    {
		servoWidth(iServoWidth);
		iServoWidth += configTable.iServoStep;
		OSTimeDly(SERVO_PAUSE_STEP);
	    } while (iServoWidth <= configTable.iServoOver);
	    
	    /* move back to properly closed */
	    do 
	    {
		servoWidth(iServoWidth);
		iServoWidth -= configTable.iServoStep;
		OSTimeDly(SERVO_PAUSE_STEP);
	    } while (iServoWidth >= configTable.iServoClosed);
	}
	
	iError = servoPower(SERVO_OFF);
    }
    else if (iOpenClose == SERVO_OPEN)
    {
	iError = servoPower(SERVO_ON);
	
	iServoWidth = servoRead();
	
	/* move to open position */
	do 
	{
	    servoWidth(iServoWidth);
	    iServoWidth -= configTable.iServoStep;
	    OSTimeDly(SERVO_PAUSE_STEP);
	} while (iServoWidth >= configTable.iServoOpen);
	
	iError |= servoPower(SERVO_OFF);
    }
    else if (iOpenClose == SERVO_SETTLE)
    {
	iError = servoPower(SERVO_ON);
	
	if (iError == SERVO_NO_ERROR)
	{
	    iServoWidth = servoRead();
	    	    
	    /* move from open to over-closed to
	     * let the bristles settle properly */
	    do 
	    {
		servoWidth(iServoWidth);
		iServoWidth += configTable.iServoStep;
		OSTimeDly(SERVO_PAUSE_STEP);
	    } while (iServoWidth <= configTable.iServoOver);
	    
	    /* move back to slightly open */
	    do 
	    {
		servoWidth(iServoWidth);
		iServoWidth -= configTable.iServoStep;
		OSTimeDly(SERVO_PAUSE_STEP);
	    } while (iServoWidth >= (configTable.iServoClosed -
				     (configTable.iServoOver - 
				      configTable.iServoClosed)));
	    
	    /* move back to properly closed */
	    do 
	    {
		servoWidth(iServoWidth);
		iServoWidth += configTable.iServoStep;
		OSTimeDly(SERVO_PAUSE_STEP);
	    } while (iServoWidth <= configTable.iServoClosed);
	}
		
	iError |= servoPower(SERVO_OFF);
    }

    
    
    return iError;
}
