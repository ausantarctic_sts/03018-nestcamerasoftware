/*
 *      Copyright (C) 2004/2005 Australian Antarctic Division
 *
 * This file contains the functions to drive commands.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <float.h>

#include <ppcc.h>
#include <ucosii.h>
#include <version.h>
#include <usart0.h>
#include <usart1.h>
#include <flash.h>
#include <mspio.h>
#include <adc12.h>
#include <nmea.h>
#include <str.h>
#include <cmd.h>
#include <intel.h>
#include <ds3234.h>
#include <eventtab.h>

#include <ppcc/main.h>
#include <ppcc/cmd.h>
#include <ppcc/util.h>
#include <ppcc/servo.h>
#include <ppcc/camera.h>

#ifdef TARGET
# include <signal.h>
#endif

#ifndef DEBUG
# define DEBUG 0
#endif

/* The main command table, define your command here and a function to 
 * handle it. Nmea commands defined in a separate file sensor/nmea.c */
#ifdef HOST
void quitCmd(char *strArgs);
#endif
void configCmd(char *strArgs);
void cameraCmd(char *strArgs);
void clockCmd(char *strArgs);
void dipswCmd(char *strArgs);
void eventTabCmd(char *strArgs);
void helpCmd(char *strArgs);
void inStatCmd(char *strArgs);
void IECCmd(char *strArgs);
void intelCmd(char *strArgs);
void ledCmd(char *strArgs);
void messageCmd(char *strArgs);
void topCmd(char *strArgs);
void relayCmd(char *strArgs);
void serialCmd(char *strArgs);
void sleepCmd(char *strArgs);
void setIOCmd(char *strArgs);
void syncCmd(char *strArgs);
void tempCmd(char *strArgs);
void unlockCmd(char *strArgs);
void versionCmd(char *strArgs);
void servoCmd(char *strArgs);
void pictureCmd(char *strArgs);

const CmdTable CmdTab[] =
{
    /* Human interface commands */
    {"config",     configCmd,    "Sets camera control parameters"},
    {"camera",     cameraCmd,    "Controls camera operation"},
    {"clock",      clockCmd,     "Show/set the clock"},
    {"dipsw",      dipswCmd,     "Show dip switch status"},
    {"event",      eventTabCmd,  "Show/set the event table"},
    {"iec",        IECCmd,       "Show/set Instrument Error Code"},
    {"instat",     inStatCmd,    "Shows status of input signals"},
    {"intel",      intelCmd,     "Start the INTEL hex downloader"},
    {"led",        ledCmd,       "Turn on/off/flash the LED's"},
    {"mesg",       messageCmd,   "Controls messages on startup"},
    {"picture",    pictureCmd,   "Open shutter and take a picture"},
    {"relay",      relayCmd,     "Control latching relay"},
    {"setio",      setIOCmd,     "Sets numerous output signals"},
    {"sleep",      sleepCmd,     "Set/show low power sleep modes"},
    {"serial",     serialCmd,    "Show/set the serial number"},
    {"servo",      servoCmd,     "Control Servo motor"},
    {"sync",       syncCmd,      "Synchronise OSTime to the clock"},
    {"top",        topCmd,       "Show system status & counters"},
    {"unlock",     unlockCmd,    "Unlock the flash"},
    {"version",    versionCmd,   "Show firmware version"},
    {"help",       helpCmd,      "Show this help"},
    {"?",          helpCmd,      "Show this help"},
#ifdef HOST
    {"quit",       quitCmd,      "Quit (host only)"},
#endif
};

const char CmdTableLength = sizeof(CmdTab)/sizeof(CmdTable);

#ifdef HOST
/* Quit command - generally not needed in Embedded systems */
void quitCmd(char *strArgs)
{
    printf("Quit command exiting ...\n");

    exit(0);
}
#endif

void IECCmd(char *strArgs)
{
    char *p;
    unsigned int uiBit;
    
    p = getFirstParameter(strArgs);
    p = getNextParameter();
    
    if (*p != '\0')
    {
    	instrumentErrorCode = stoi(p, 10);
    } 
    else
	    printf("Usage : iec <value>\r\n");
    
    printf("instrumentErrorCode = 0x%04X\r\n", instrumentErrorCode);

    if (instrumentErrorCode)
    {	
	    printf("Critical Flags :\r\n");
	
    	uiBit = 0x8000;
	
	do
	{
	    if (uiBit == IEC_CRITICAL)
		printf("Non-Critical Flags :\r\n");
	    
	    if ((instrumentErrorCode & uiBit) == uiBit)
	    {
		printf("0x%04X:", uiBit);
		
		switch (uiBit) 
		{
		case IEC_EVENTTAB_NOENTRIES : printf("IEC_EVENTTAB_NOENTRIES"); break;
		case IEC_SERVO_FAULT : printf("IEC_SERVO_FAULT"); break;
		case IEC_SPARE3 : printf("IEC_SPARE3"); break;
		case IEC_CAMERA_INACTIVE : printf("IEC_CAMERA_INACTIVE"); break;
		case IEC_EVENT_DISABLE : printf("IEC_EVENT_DISABLE"); break; 
		case IEC_VINPUT_FAULT : printf("IEC_VINPUT_FAULT"); break;
		case IEC_RTC_OSCFAULT : printf("IEC_RTC_OSCFAULT"); break;
		case IEC_OSTIME_UNSYNC : printf("IEC_OSTIME_UNSYNC"); break;
		case IEC_WRONG_PARAMETER_NO : printf("IEC_WRONG_PARAMETER_NO"); break;
		case IEC_UNKNOWN_COMMAND : printf("IEC_UNKNOWN_COMMAND"); break;
		case IEC_DCO_BIG_ERROR : printf("IEC_DCO_BIG_ERROR"); break;
		case IEC_SPARE2 : printf("IEC_SPARE2"); break;
		case IEC_SPARE1 : printf("IEC_SPARE1"); break;
		case IEC_USART1_ERROR : printf("IEC_USART1_ERROR"); break;
		case IEC_USART0_ERROR : printf("IEC_USART0_ERROR"); break;
		case IEC_RESET : printf("IEC_RESET"); break;
		default : printf("Unknown IEC Flag"); break;
		}
		printf("\r\n");
	    }
	    
	    uiBit >>= 1;
	    
	} while (uiBit > 0);
    }
}

void versionCmd(char *strArgs)
{
    printf("ucosII-ppcc V%s %s\r\n", FirmwareVersion, FirmwareDate);
}

void helpCmd(char *strArgs)
{
    int i;

    printf("Command\tDescription\r\n");

    for (i=0;i<CmdTableLength;i++)
    {
        printf("%s\t%s\r\n", CmdTab[i].Command, CmdTab[i].HelpMsg);
    }
}

void topCmd(char *strArgs)
{
#ifdef TARGET        
    INT8U u8Err;
    OS_STK_DATA OSStackData;
    
    putchar('\n');
    printf("OSTime %lu\r\n", OSTimeGet());
    printf("Id         Task Name Used Free  Count\r\n");

    u8Err = OSTaskStkChk(EVENT_TASK_PRIORITY, &OSStackData);
    if (u8Err == OS_NO_ERR)
    {
	printf("%2d %17s %4lu %4lu %6u\r\n", 
	       EVENT_TASK_ID, 
	       EVENT_TASK_NAME,
	       OSStackData.OSUsed,  
	       OSStackData.OSFree, 
	       TaskUserData[EVENT_TASK_ID].TaskCtr);
    }
    else
	printf("Error %d querying %s\r\n", u8Err, EVENT_TASK_NAME);
    
    u8Err = OSTaskStkChk(CMD_TASK_PRIORITY, &OSStackData);
    if (u8Err == OS_NO_ERR)
    {
	printf("%2d %17s %4lu %4lu %6u\r\n", 
	       CMD_TASK_ID,
	       CMD_TASK_NAME,
	       OSStackData.OSUsed,  
	       OSStackData.OSFree, 
	       TaskUserData[CMD_TASK_ID].TaskCtr);
    }
    else
	printf("Error %d querying %s\r\n", u8Err, CMD_TASK_NAME);

    u8Err = OSTaskStkChk(BUTTON_TASK_PRIORITY, &OSStackData);
    if (u8Err == OS_NO_ERR)
    {
	    printf("%2d %17s %4lu %4lu %6u\r\n", 
	       BUTTON_TASK_ID,
	       BUTTON_TASK_NAME,
	       OSStackData.OSUsed,  
	       OSStackData.OSFree, 
	       TaskUserData[BUTTON_TASK_ID].TaskCtr);
    }
    else
	printf("Error %d querying %s\r\n", u8Err, BUTTON_TASK_NAME);
    
    u8Err = OSTaskStkChk(IDLE_TASK_PRIORITY, &OSStackData);
    if (u8Err == OS_NO_ERR)
    {
	    printf("   %17s %4lu %4lu\r\n",
	       IDLE_TASK_NAME,
	       OSStackData.OSUsed,  
	       OSStackData.OSFree);
    }
    else
	printf("Error %d querying %s\r\n", u8Err, IDLE_TASK_NAME);    
    
#endif
#ifdef HOST
    printf("Not working on host\r\n");
#endif        
}

/*
 * Control latching relay
 */
void relayCmd(char *strArgs)
{
    INT8U u8Err;
    char *p;
    char cOption = '\0';
    
    p = getFirstParameter(strArgs);
    p = getNextParameter();
    
    if (*p != '\0')
	cOption = *p;
    
    if (cOption == '\0')
    {
	    printf("Usage : relay <option>\r\n");
	    printf("Available options : \r\n");
	    printf(" 0 - reset\r\n");
	    printf(" 1 - set\r\n");
	    printf(" r - read\r\n");
	    printf(" t - toggle\r\n");
    }
    else
    {	
    	switch(cOption)
	    {
	    case '0' : // reset
	        {
		    OSMutexPend(HardwareMutex, 0, &u8Err);
		    relayReset();	
		    OSMutexPost(HardwareMutex);
	        }; break;
	    case '1' : // set
	        {
		    OSMutexPend(HardwareMutex, 0, &u8Err);
		    relaySet();
		    OSMutexPost(HardwareMutex);
	        }; break;
	    case 'r' : // read
	        {
		    if (relayRead() != 0)
		        printf("set\r\n");
		    else
		        printf("reset\r\n");
		
	        }; break;	    
	    case 't' : // toggle
	        {
		    if (relayRead() == 0)
		        relaySet();
		    else
		        relayReset();
	        }; break;
	    default : 
	        printf("Usage : relay <0|1|r|t>\r\n");
	        break;
	    }	
    }    
}

/* 
 * Sets/Clears general output signals
 */
void setIOCmd(char *strArgs)
{
#ifdef TARGET
    INT8U u8Err;
    
    char *p;
    char cOption = '\0';
    char cLevel = '\0';
    
    p = getFirstParameter(strArgs);
    p = getNextParameter();
    
    if (*p != '\0')
    {
	cOption = *p;
	p = getNextParameter();
	
	if (*p != '\0')
	    cLevel = *p;
    }
    
    if (cOption == '\0')
    {
	    printf("Usage : setio <option> <0|1>\r\n");
	    printf("Available options : (use with caution!)\r\n");
	    printf(" 1 - Camera Shutter   %d\r\n", ((P1IN & P1_CAMERA_SHUTTER) == P1_CAMERA_SHUTTER ? 1 : 0));
	    printf(" 2 - Camera Focus     %d\r\n", ((P1IN & P1_CAMERA_FOCUS) == P1_CAMERA_FOCUS ? 1 : 0));
	    printf(" 3 - P5.5/Green LED   %d\r\n", ((P5IN & P5_LED_GREEN) == P5_LED_GREEN ? 1 : 0));
	    printf(" 4 - P5.6/Red LED     %d\r\n", ((P5IN & P5_LED_RED) == P5_LED_RED ? 1 : 0));
	    printf(" 5 - VCAMERA_ENABLE   %d\r\n", ((P6IN & P6_VCAMERA_ENABLE) == P6_VCAMERA_ENABLE ? 1 : 0));
	    printf(" 6 - VSERVO_ENABLE    %d\r\n", ((P6IN & P6_VSERVO_ENABLE) == P6_VSERVO_ENABLE ? 1 : 0));
    }
    else
    {
	OSMutexPend(HardwareMutex, 0, &u8Err);
#endif

#ifdef TARGET
	switch(cOption)
	{
	case '1' : // Camera Shutter
	    {
		if (cLevel == '0')
		    BICB(P1OUT, P1_CAMERA_SHUTTER);
		else if (cLevel == '1')
		    BISB(P1OUT, P1_CAMERA_SHUTTER);
	    }; break;
	case '2' : // Camera Focus
	    {
		if (cLevel == '0')
		    BICB(P1OUT, P1_CAMERA_FOCUS);
		else if (cLevel == '1')
		    BISB(P1OUT, P1_CAMERA_FOCUS);
	    }; break;
	case '3' : // P5.5/GREEN LED
	    {
		if (cLevel == '0')
		    BICB(P5OUT, P5_LED_GREEN);
		else if (cLevel == '1')
		    BISB(P5OUT, P5_LED_GREEN);
		else if (cLevel == '\0')
		    printf("P5_LED_GREEN (P5.5) = %d\r\n", ((P5IN & P5_LED_GREEN) == P5_LED_GREEN) ? 1 : 0);
	    }; break;
	case '4' : // P5.6/RED LED
	    {
		if (cLevel == '0')
		    BICB(P5OUT, P5_LED_RED);
		else if (cLevel == '1')
		    BISB(P5OUT, P5_LED_RED);
		else if (cLevel == '\0')
		    printf("P5_LED_RED (P5.6) = %d\r\n", ((P5IN & P5_LED_RED) == P5_LED_RED) ? 1 : 0);
	    }; break;
	case '5' : // VCAMERA_ENABLE
	    {
		if (cLevel == '0')
		    BICB(P6OUT, P6_VCAMERA_ENABLE);
		else if (cLevel == '1')
		    BISB(P6OUT, P6_VCAMERA_ENABLE);
		else if (cLevel == '\0')
		    printf("VCAMERA_ENABLE = %d\r\n", ((P6IN & P6_VCAMERA_ENABLE) == P6_VCAMERA_ENABLE) ? 1 : 0);
	    }; break;
	case '6' : // VSERVO_ENABLE
	    {
		if (cLevel == '0')
		    BICB(P6OUT, P6_VSERVO_ENABLE);
		else if (cLevel == '1')
		    BISB(P6OUT, P6_VSERVO_ENABLE);
		else if (cLevel == '\0')
		    printf("VSERVO_ENABLE = %d\r\n", ((P6IN & P6_VSERVO_ENABLE) == P6_VSERVO_ENABLE) ? 1 : 0);
	    }; break;
	default : 
	    printf("Usage : setio <option> <0|1>\r\n");
	    break;
	}

	OSMutexPost(HardwareMutex);
    }
#endif	
}

/* 
 * Displays Analog & Digital input channel values
 */
void inStatCmd(char *strArgs)
{
    INT8U u8Err;
    
    /* Print out Analog inputs */
    printf("Analog Inputs :\r\n");
    OSMutexPend(HardwareMutex, 0, &u8Err);
    printf("VInput           = %02.3fV\r\n", readVInput());
    printf("VCamera          = %02.3fV\r\n", readVCamera());
    printf("VServo           = %02.3fV\r\n", readVServo());
    printf("Temperature      = %02.0f degC\r\n", readMSP430Temp());
    OSMutexPost(HardwareMutex);
    
#ifdef TARGET    
    /* Print out digital inputs */
    printf("Digital Inputs :\r\n");
    printf("BUTTON1          = %d\r\n", ((P1IN & P1_BUTTON1) == P1_BUTTON1 ? 1 : 0));
    printf("BUTTON2          = %d\r\n", ((P1IN & P1_BUTTON2) == P1_BUTTON2 ? 1 : 0));
    printf("CAMERA_SLEEP     = %d\r\n", ((P1IN & P1_CAMERA_SLEEP) == P1_CAMERA_SLEEP ? 1 : 0));
    printf("VCAMERA_PGOOD    = %d\r\n", ((P4IN & P4_VCAMERA_PGOOD) == P4_VCAMERA_PGOOD) ? 1 : 0);
    printf("VSERVO_PGOOD     = %d\r\n", ((P4IN & P4_VSERVO_PGOOD) == P4_VSERVO_PGOOD) ? 1 : 0);
    
    printf("Config Switches (1 = on, 0 = off)\r\n");
    printf("Switch 1         = %d\r\n", dipSwitch(1));
    printf("Switch 2         = %d\r\n", dipSwitch(2));
    printf("Switch 3         = %d\r\n", dipSwitch(3));
    printf("Switch 4         = %d\r\n", dipSwitch(4));
#endif
}

/* 
 * Toggles powerup / debugging messages on & off 
 * 
 */
void messageCmd(char *strArgs)
{
    char *p;
    unsigned char i;
    
    p = getFirstParameter(strArgs);
    p = getNextParameter(); /* find the next param if there is one */

    if ((*p == '0') || (*p == '1'))
    {
        if (flashLocked())
            printf("Unlock flash first\r\n");

	i = *p - '0';
	flashUpdate((unsigned char *)(&configTable.u8Messages), 1, &i);
    }
    else
	printf("usage : messages <0|1>\r\n");

    if (configTable.u8Messages == 1)
	printf("Messages turned on\r\n");
    else
	printf("Messages turned off\r\n");
}

/*
 *  Write to the flash based serial number/string
 */
void serialCmd(char *strArgs)
{
    char *p;

    p = getFirstParameter(strArgs);
    p = getNextParameter();

    if (*p != '\0')
    {
        if (flashLocked())
            printf("Unlock flash first\r\n");

	/* truncate string if greater than available storage */
	if (strlen(p) >= SERIALNUMBER_STRLEN)
	{
            p[SERIALNUMBER_STRLEN-1] = '\0';
	    printf("Warning : Exceeded max %d characters\r\n", SERIALNUMBER_STRLEN - 1);
	}
        flashUpdate((unsigned char *)(&configTable.strSerialNumber), 
		    strlen(p)+1, (unsigned char *)p);
    }

    printf("Serial Number : \"%s\"\r\n", configTable.strSerialNumber);
}

void intelCmd(char *strArgs)
{
    if (flashLocked())
    {
        printf("Unlock flash first\r\n");
    }
    else
    {
        Download();
    }
}

void sleepCmd(char *strArgs)
{
    char *p;

    p = getFirstParameter(strArgs);
    p = getNextParameter();

    switch (tolower(*p))
    {
        case '0':
            uiSleep &= ~SLEEP_MANUAL_CONTROL;
            break;
        case '1':
            uiSleep |= SLEEP_MANUAL_CONTROL;
            break;
        case 'x':
            uiSleep ^= SLEEP_MANUAL_CONTROL;
            break;
        case 0:
            break;
        default:
            printf("Usage sleep <0|1|x>\r\n");
            break;
    }

    printf("uiSleep = 0x%04x\r\n", uiSleep);

    if (uiSleep)
	printf("Flags :\r\n");
    
    if (uiSleep & SLEEP_FORCE_DCOTUNE) printf(" SLEEP_FORCE_DCOTUNE\r\n");
    if (uiSleep & SLEEP_COMMAND_ACTIVE) printf(" SLEEP_COMMAND_ACTIVE\r\n");
    if (uiSleep & SLEEP_EVENTTASK_ACTIVE) printf(" SLEEP_EVENTTASK_ACTIVE\r\n");
    if (uiSleep & SLEEP_STARTUP) printf(" SLEEP_STARTUP\r\n");
    if (uiSleep & SLEEP_CHAR_RX1) printf(" SLEEP_CHAR_RX1\r\n");
    if (uiSleep & SLEEP_CHAR_RX0) printf(" SLEEP_CHAR_RX0\r\n");
    if (uiSleep & SLEEP_SERVO_ACTIVE) printf(" SLEEP_SERVO_ACTIVE\r\n");
    if (uiSleep & SLEEP_MANUAL_CONTROL) printf(" SLEEP_MANUAL_CONTROL\r\n");
}

void unlockCmd(char *strArgs)
{
    char *p;
    unsigned int key;

    key = FWKEY;
    
    p = getFirstParameter(strArgs);
    p = getNextParameter();

    if (strcmp(p, "password") == 0)
    {
        printf("Unlocking flash ... \r\n");
        flashUnlock(key);
    }
    else if (*p != '\0')
    {
        flashUnlock(key | LOCK);
    }

#ifdef TARGET
    if (flashLocked())
    {
        printf("Flash locked type 'unlock password' to unlock\r\n");
    }
    else
#endif
        printf("Flash unlocked\r\n");
}

/*
 * Sets the internal RTC
 */
void clockCmd(char *strArgs)
{
    char *p;
    INT8U u8Err;
    uint8_t u8Temp, u8Val;
    int iLoop;
    float fTemp;
    int8_t i8AgeOffset;
    int iRate;
    RTCTimeStruct RTCTime;
    
    p = getFirstParameter(strArgs);
    p = getNextParameter(); 
        
    switch(*p)
    {
    case 'p' : // Print clock
	printClock();
	break;
    case 's' : // Set clock
	{
	    p = getNextParameter(); 
	    
	    if (*p != '\0')
	    {
		/* Year */
		if (*p != '\0')
		{
		    u8Temp = (uint8_t)stoi(p, 10);
		    if (u8Temp <= 99)
			RTCTime.u8Year = BIN2BCD(u8Temp);
		    else
		    {
			printf("Invalid year %d, use 00 .. 99\r\n", u8Temp);
			u8Err = 1;
		    }
		}
		else
		    u8Err = 1;
		
		/* Month */
		p = getNextParameter();
		if (*p != '\0')
		{
		    u8Temp = (uint8_t)stoi(p, 10);
		    if ((u8Temp >= 1) && (u8Temp <= 12))
			RTCTime.u8Month = BIN2BCD(u8Temp);
		    else
		    {
			printf("Invalid month %d, use 1 .. 12\r\n", u8Temp);
			u8Err = 1;
		    }
		}
		else
		    u8Err = 1;
		
		/* Day */
		p = getNextParameter();
		if (*p != '\0')
		{
		    u8Temp = (uint8_t)stoi(p, 10);
		    if ((u8Temp >= 1) && (u8Temp <= 31))
			RTCTime.u8DayOfMonth = BIN2BCD(u8Temp);
		    else
		    {
			printf("Invalid day of month %d, use 1 .. 31\r\n", u8Temp);
			u8Err = 1;
		    }
		}
		else
		    u8Err = 1;
		
		/* Hour */
		p = getNextParameter();
		if (*p != '\0')
		{
		    u8Temp = (uint8_t)stoi(p, 10);
		    if (u8Temp <= 23)
			RTCTime.u8Hour = BIN2BCD(u8Temp);
		    else
		    {
			printf("Invalid hour %d, use 00 .. 23\r\n", u8Temp);
			u8Err = 1;
		    }
		}
		else
		    u8Err = 1;
		
		/* Minute */
		p = getNextParameter();
		if (*p != '\0')
		{
		    u8Temp = (uint8_t)stoi(p, 10);
		    if (u8Temp <= 59)
			RTCTime.u8Minute = BIN2BCD(u8Temp);
		    else
		    {
			printf("Invalid minute %d, use 00 .. 59\r\n", u8Temp);
			u8Err = 1;
		    }
		}
		else
		    u8Err = 1;
		
		/* Second */
		p = getNextParameter();
		if (*p != '\0')
		{
		    u8Temp = (uint8_t)stoi(p, 10);
		    if (u8Temp <= 59)
			RTCTime.u8Second = BIN2BCD(u8Temp);
		    else
		    {
			printf("Invalid second %d, use 00 .. 59\r\n", u8Temp);
			u8Err = 1;
		    }
		}
		else
		    u8Err = 1;
		
		if (u8Err != 1)
		{
		    OSMutexPend(RTCMutex, 0, &u8Err);
		    /* Write the time */
		    ds3234BCDWriteRTC(&RTCTime);
		    
		    /* clear OSF flag to indicate time has been set */
		    u8Temp = ds3234Read(DS3234_STA);
		    ds3234Write(DS3234_STA, u8Temp & ~DS3234_STA_OSF);
		    
		    OSMutexPost(RTCMutex);
		    
		    printf("Clock has been set to %02X/%02X/%02X %02X:%02X:%02X\r\n",
			   RTCTime.u8Year,
			   RTCTime.u8Month,
			   RTCTime.u8DayOfMonth,
			   RTCTime.u8Hour,
			   RTCTime.u8Minute,
			   RTCTime.u8Second);
		    
		    /* clear instrument error code */
		    instrumentErrorCode &= ~IEC_RTC_OSCFAULT;
		    
		    /* Synchronise OSTime to RTC */
		    if (!syncOSToRTC(OSTIME_SYNC_LIMIT, OSTIME_SYNC_RETRIES))
			printf("Unable to synchronise OSTIME to RTC. Clock/Oscillator problem.\r\n");
		}
		else
		    printf("Invalid or insufficient parameters. Clock not set\r\n");
	    }
	    else
		goto CLOCK_USAGE;
	}
	break;
    case 'r' : // read/write registers
	{
	    unsigned int uiAddr = UINT_MAX;
	    unsigned int uiData = UINT_MAX;
	    
	    p = getNextParameter(); 
	    
	    if (*p != '\0')
	    {
		uiAddr = (unsigned int)stoi(p, 10);
		
		p = getNextParameter();
		if (*p != '\0')
		    uiData = (unsigned int)stoi(p, 10);
		
		if ((uiAddr >= 0) &&
		    (uiAddr <= DS3234_SRAM_DATA) &&
		    (uiData >= 0) &&
		    (uiData <= 0xFF))
		{
		    OSMutexPend(RTCMutex, 0, &u8Err);
		    ds3234Write(uiAddr, uiData);
		    OSMutexPost(RTCMutex);
		    printf("\r\n");
		}
		else
		{
		    printf("Address/data out of range (0x00 - 0xFF)\r\n");
		}
	    }
	    else  /* Dump registers with no arguments */
	    {
		printf("ADDR 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F");
		
		/* Registers */
		for (u8Temp = 0; u8Temp <= DS3234_TEMP_LSB; u8Temp++)
		{
		    if ((u8Temp % 0x10) == 0)
			printf("\r\n%04X ", u8Temp);
		    OSMutexPend(RTCMutex, 0, &u8Err);
		    printf("%02X ", (int)ds3234Read(u8Temp));
		    OSMutexPost(RTCMutex);
		}
		printf("\r\n");
	    }
	}
	break;	
    case 'n' : // write to NVRAM
	{
	    unsigned int uiAddr = UINT_MAX;
	    unsigned int uiData = UINT_MAX;
	    
	    p = getNextParameter(); 
	    
	    if (*p != '\0')
	    {
		uiAddr = (unsigned int)stoi(p, 10);
		
		p = getNextParameter();
		if (*p != '\0')
		    uiData = (unsigned int)stoi(p, 10);
		
		if ((uiAddr >= 0) &&
		    (uiAddr <= DS3234_SRAM_MAX) &&
		    (uiData >= 0) &&
		    (uiData <= 0xFF))
		{
		    OSMutexPend(RTCMutex, 0, &u8Err);
		    ds3234Write(DS3234_SRAM_ADDR, uiAddr);
		    ds3234Write(DS3234_SRAM_DATA, uiData);
		    OSMutexPost(RTCMutex);
		    printf("\r\n");
		}
		else
		{
		    printf("Address or data out of range (0x00 - 0xFF)\r\n");
		}
	    }
	    else
	    {
		/* Dump NVRAM only */
		printf("ADDR 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F");

		for (iLoop = 0x00; iLoop <= DS3234_SRAM_MAX; iLoop++)
		{
		    if ((iLoop % 0x10) == 0)
			printf("\r\n%04X ", iLoop);
		    OSMutexPend(RTCMutex, 0, &u8Err);
		    ds3234Write(DS3234_SRAM_ADDR, (INT8U)(iLoop & 0xFF));
		    printf("%02X ", (int)ds3234Read(DS3234_SRAM_DATA));
		    OSMutexPost(RTCMutex);
		}
		printf("\r\n");		
	    }
	}
	break;	
    case 'i' : // Init clock NVRAM
	{
	    p = getNextParameter();
	    
	    if (*p != '\0')
	    {
		u8Val = (uint8_t)stoi(p, 10);
		
		OSMutexPend(RTCMutex, 0, &u8Err);
		for (iLoop = 0x00; iLoop <= DS3234_SRAM_MAX; iLoop++)
		{
		    ds3234Write(DS3234_SRAM_ADDR, (INT8U)(iLoop & 0xFF));
		    ds3234Write(DS3234_SRAM_DATA, u8Val);
		}
		OSMutexPost(RTCMutex);
		printf("\r\n");
	    }
	}
	break;
    case 't' : // Read temperature
	{
	    p = getNextParameter();

	    OSMutexPend(RTCMutex, 0, &u8Err);
	    if (*p == 'i')
		fTemp = ds3234ReadTemperature(1, &i8AgeOffset, &iRate);
	    else
		fTemp = ds3234ReadTemperature(0, &i8AgeOffset, &iRate);
	    OSMutexPost(RTCMutex);	
	    
	    printf("Temp,%0.2f,Age,%d,Rate,%d\r\n", fTemp, (int)i8AgeOffset, iRate);
	} break;
    default : 
	{
	    CLOCK_USAGE:
	    printf("Usage : clock <a|p|s|r|n|i|t>\r\n");
	    printf(" p                               - Print clock\r\n");
	    printf(" s <YY> <MM> <DD> <HH> <MM> <SS> - Set clock\r\n");
	    printf(" r <Addr> <Data>                 - Read/Write registers\r\n");
	    printf(" n <Addr> <Data>                 - Read/Write NVRAM\r\n");
	    printf(" i <value>                       - Init all NVRAM to a value\r\n");
	    printf(" t [i]                           - Print clock temp, age and rate.\r\n");
	    printf("                                   supply 'i' to force conversion\r\n");
	}
	break;
    }
    
    if ((configTable.u8Messages == 1) && (instrumentErrorCode & IEC_RTC_OSCFAULT))
	printf("**ERROR** Please set clock with 'clock s' command\r\n");
}

/* 
 * Configure the camera/shutter timing/settings
 */
void configCmd(char *strArgs)
{
    char *p;
    char cOption = '\0';
    char cValue = '\0';
    float fTemp;
    unsigned int uiTemp;
    INT8U u8Err;
    
    p = getFirstParameter(strArgs);
    p = getNextParameter();
    
    if (*p != '\0')
    {
	cOption = *p;
	p = getNextParameter();
	
	if (*p != '\0')
	    cValue = *p;
    }
    
    if ((cOption == '\0') && (cValue == '\0'))
    {
		// print all values
		printf("Usage: config <number> <value>\r\n\r\n");
		printf("Current Configuration\r\n");
		printf("----------------------------------------\r\n");
		printf(" 1) Turn on delay              = %0.1fs\r\n", configTable.fTurnOnDelay);
		printf(" 2) Expose & Write delay       = %0.1fs\r\n", configTable.fExposeWriteDelay);
		printf(" 3) Shutter/Focus pulse length = %0.1fs\r\n", configTable.fPulseLength);
		printf(" 4) VInput limit for camera    = %0.1fv\r\n", configTable.fVInputCamera);
		if (configTable.uiCameraOffTimer == 0)
    		printf(" 5) Turn off timer             = 0m (disabled)\r\n");
		else
			printf(" 5) Turn off timer             = %1dm\r\n", configTable.uiCameraOffTimer);
		printf(" 6) VInput limit for servo     = %0.1fv\r\n",  configTable.fVInputServo);
		printf(" 7) Servo step size            = %d\r\n", configTable.iServoStep);
		printf(" 8) Servo open position        = %d\r\n", configTable.iServoOpen);
		printf(" 9) Servo closed position      = %d\r\n", configTable.iServoClosed);
		printf(" a) Servo over position        = %d\r\n", configTable.iServoOver);
		printf(" b) BIST VInput min            = %0.1fv\r\n", configTable.fVInputMin);
		printf(" c) BIST VInput max            = %0.1fv\r\n", configTable.fVInputMax);
		printf(" d) BIST VServo min            = %0.1fv\r\n", configTable.fVServoMin);
		printf(" e) BIST VServo max            = %0.1fv\r\n", configTable.fVServoMax);
		printf(" f) BIST VCamera min           = %0.1fv\r\n", configTable.fVCameraMin);
		printf(" g) BIST VCamera max           = %0.1fv\r\n", configTable.fVCameraMax);
		printf(" h) Camera clock recharge      = %0.1fs\r\n", configTable.fClockPowerPulseLength);
    }
    else if  ((cOption != '\0') && (cValue == '\0')) 
    {
		// read back single values
		switch(toupper(cOption))
		{
			case '1' : printf("Turn On Delay,%0.1fs\r\n", configTable.fTurnOnDelay); break; 
			case '2' : printf("Expose & Write delay,%0.1fs\r\n", configTable.fExposeWriteDelay); break;
			case '3' : printf("Shutter/Focus pulse length,%0.1fs\r\n", configTable.fPulseLength); break;
			case '4' : printf("VInput limit for camera,%0.1fv\r\n",configTable.fVInputCamera); break;
			case '5' : printf("Turn Off Timer,%d\r\n", configTable.uiCameraOffTimer); break;
			case '6' : printf("VInput limit for servo,%0.1fv\r\n", configTable.fVInputServo); break;
			case '7' : printf("Servo step size,%d\r\n", configTable.iServoStep); break;
			case '8' : printf("Servo open position,%d\r\n", configTable.iServoOpen); break;
			case '9' : printf("Servo closed position,%d\r\n", configTable.iServoClosed); break;
			case 'A' : printf("Servo over position,%d\r\n", configTable.iServoOver); break;
			case 'B' : printf("BIST VInput min,%0.1fv\r\n", configTable.fVInputMin); break;
			case 'C' : printf("BIST VInput mmax,%0.1fv\r\n", configTable.fVInputMax); break;
			case 'D' : printf("BIST VServo min,%0.1fv\r\n", configTable.fVInputMin); break;
			case 'E' : printf("BIST VServo max,%0.1fv\r\n", configTable.fVInputMax); break;
			case 'F' : printf("BIST VCamera min,%0.1fv\r\n", configTable.fVInputMin); break;
			case 'G' : printf("BIST VCamera max,%0.1fv\r\n", configTable.fVInputMax); break;
			case 'H' : printf("Camera clock recharge,%0.1fs\r\n", configTable.fClockPowerPulseLength); break;
			default : printf("Unknown parameter,%c\r\n", cOption); break;
		}
    }
    else
    {
		if (flashLocked())
		    printf("Unlock flash first\r\n");
		else
		{
		    OSMutexPend(HardwareMutex, 0, &u8Err);
	    
		    switch(toupper(cOption))
		    {
		    case '1' : // Turn on delay
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fTurnOnDelay), 
					sizeof(configTable.fTurnOnDelay), 
					(unsigned char *)&fTemp);
			}; break;
		    case '2' : // Expose & Write picture delay
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fExposeWriteDelay), 
					sizeof(configTable.fExposeWriteDelay), 
					(unsigned char *)&fTemp);
			}; break;
		    case '3' : // Shutter/Focus Pulse length
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fPulseLength),
					sizeof(configTable.fPulseLength), 
					(unsigned char *)&fTemp);
			}; break;
		    case '4' : // VInput limit for camera
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fVInputCamera),
					sizeof(configTable.fVInputCamera), 
					(unsigned char *)&fTemp);
			}; break;
		    case '5' : // Camera Off Timer
			{
			    uiTemp = stoi(p, 10);
			    flashUpdate((unsigned char *)(&configTable.uiCameraOffTimer),
					sizeof(configTable.uiCameraOffTimer),
					(unsigned char *)&uiTemp);
			}; break;
		    case '6' : // VInput limit for servo
			{
		    fTemp = stof(p, 10);
		    flashUpdate((unsigned char *)(&configTable.fVInputServo),
				sizeof(configTable.fVInputServo), 
				(unsigned char *)&fTemp);
			}; break;
		    case '7' : // Servo step size
			{
			    uiTemp = stoi(p, 10);
			    flashUpdate((unsigned char *)(&configTable.iServoStep),
					sizeof(configTable.iServoStep),
					(unsigned char *)&uiTemp);
			}; break;
		    case '8' : // Servo initial open position
			{
			    uiTemp = stoi(p, 10);
			    flashUpdate((unsigned char *)(&configTable.iServoOpen),
					sizeof(configTable.iServoOpen),
					(unsigned char *)&uiTemp);
			}; break;
		    case '9' : // Servo initial closed position
			{
			    uiTemp = stoi(p, 10);
			    flashUpdate((unsigned char *)(&configTable.iServoClosed),
					sizeof(configTable.iServoClosed),
					(unsigned char *)&uiTemp);
			}; break;
		    case 'A' : // Servo initial closed position
			{
			    uiTemp = stoi(p, 10);
			    flashUpdate((unsigned char *)(&configTable.iServoOver),
					sizeof(configTable.iServoOver),
					(unsigned char *)&uiTemp);
			}; break;		
		    case 'B' : // VInput min for BIST
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fVInputMin),
					sizeof(configTable.fVInputMin), 
					(unsigned char *)&fTemp);
			}; break;
		    case 'C' : // VInput max for BIST
			{
		    fTemp = stof(p, 10);
		    flashUpdate((unsigned char *)(&configTable.fVInputMax),
				sizeof(configTable.fVInputMax), 
				(unsigned char *)&fTemp);
			}; break;
		    case 'D' : // VServo min for BIST
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fVServoMin),
					sizeof(configTable.fVServoMin), 
					(unsigned char *)&fTemp);
			}; break;
		    case 'E' : // VServo max for BIST
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fVServoMax),
					sizeof(configTable.fVServoMax), 
					(unsigned char *)&fTemp);
			}; break;
		    case 'F' : // VCamera min for BIST
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fVCameraMin),
					sizeof(configTable.fVCameraMin), 
					(unsigned char *)&fTemp);
			}; break;
		    case 'G' : // VCamera max for BIST
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fVCameraMax),
					sizeof(configTable.fVCameraMax), 
					(unsigned char *)&fTemp);
			}; break;
		    case 'H' : // Clock Power Pulse length
			{
			    fTemp = stof(p, 10);
			    flashUpdate((unsigned char *)(&configTable.fClockPowerPulseLength),
					sizeof(configTable.fClockPowerPulseLength), 
					(unsigned char *)&fTemp);
			}; break;
	
		    default : 
				printf("Unknown option '%c'\r\n", cOption);
				break;	
		    }
		    OSMutexPost(HardwareMutex);
		}
    }
}

/*
 * controls the servo motor
 */
void servoCmd(char *strArgs)
{
    unsigned int uiWidth;
    INT8U u8Err;
    char *p;
    
    p = getFirstParameter(strArgs);
    p = getNextParameter();
	
    switch (*p)
    {
	    case 'w' :
		{
		    p = getNextParameter();
	
		    if (p != '\0')
		    {
				uiWidth = stoi(p, 10);
		    
				if ((uiWidth > SERVO_WIDTH_MAX) || (uiWidth < SERVO_WIDTH_MIN))
				{
			    	printf("Servo width outside valid range %d < width < %d\r\n",
					   SERVO_WIDTH_MIN, SERVO_WIDTH_MAX);
			    	break;
				}
				else
				{
			    	OSMutexPend(HardwareMutex, 0, &u8Err);
			    	servoWidth(uiWidth);
			    	OSMutexPost(HardwareMutex);
			    	printf("set width to %d\r\n", uiWidth);
				}
		    }
		    else
				printf("Unknown width parameter\r\n");
		}
		break;
    case '1' :
		{
		    OSMutexPend(HardwareMutex, 0, &u8Err);
		    controlServo(SERVO_ON);
		    OSMutexPost(HardwareMutex);
		}
		break;
    case '0' :
		{
		    OSMutexPend(HardwareMutex, 0, &u8Err);
		    controlServo(SERVO_OFF);
		    OSMutexPost(HardwareMutex);
		}
		break;
    case 'c' :
		{
		    OSMutexPend(HardwareMutex, 0, &u8Err);
		    controlServo(SERVO_CLOSE);
		    OSMutexPost(HardwareMutex);
		}
		break;
    case 'o' :
		{
		    OSMutexPend(HardwareMutex, 0, &u8Err);
		    controlServo(SERVO_OPEN);
		    OSMutexPost(HardwareMutex);
		}
		break;	
    case 'r' : 
		{
#ifdef TARGET
		    if ((P6OUT & P6_VSERVO_ENABLE) == 0)
				printf("Servo power is off\r\n");
		    else
				printf("Servo power is on\r\n");
			
			printf("Current pulse width = %d\r\n", servoRead());
#endif
		}
		break;
	case 's' :
		{
		    OSMutexPend(HardwareMutex, 0, &u8Err);
		    controlServo(SERVO_SETTLE);
		    OSMutexPost(HardwareMutex);
		}
		break;		
    default : 
		{
		    printf("Usage : servo <1|0|c|o|w|r>\r\n");
		    printf(" 1 - Servo power on\r\n");
		    printf(" 0 - Servo power off\r\n");
		    printf(" c - Close shutter\r\n");
		    printf(" o - Open shutter\r\n");
		    printf(" w - Set control pulse width (%d < width < %d)\r\n",
			   SERVO_WIDTH_MIN, SERVO_WIDTH_MAX);
		    printf(" r - Read & display current settings\r\n");
		    printf(" s - Settle bristles (also closes shutter)\r\n");
		}
		break;
    }
}


/*
 * Controls the eventTable - add/print/delete/erase/etc
 */
void eventTabCmd(char *strArgs)
{
    int16_t iLast;
    int16_t i16Index;
    uint8_t u8Err = 0;
    uint8_t u8Temp = 0;
    uint8_t u8Mask = 0;
    eventTabEntryStruct eventTabEntry;
    char *p;
    
    p = getFirstParameter(strArgs);
    p = getNextParameter();
	
    switch (*p)
    {
    case 'e' : // Erase the entire table
	if (flashLocked())
	{
	    printf("Unlock flash first\r\n");
	}
	else
	{
	    OSMutexPend(HardwareMutex, 0, &u8Err);
	    printf("Erasing event Table - ");
	    eventTabErase();
	    printf("Erased\r\n");
	    OSMutexPost(HardwareMutex);
	    
	    OSMutexPend(RTCMutex, 0, &u8Err);
	    /* set NVRAM index to reset value */
	    NVRAMu16Write(NVRAM_LASTEVENT_INDEX, 0xFFFF);
	    
	    /* erase counters */
	    NVRAMu32Write(NVRAM_LASTEVENT_DATETIME, 0);
	    NVRAMu16Write(NVRAM_EVENT_COUNT, 0);
	    OSMutexPost(RTCMutex);
	    
	    instrumentErrorCode |= IEC_EVENTTAB_NOENTRIES;
	}
	break;
    case 'p' : // print the table
	{
	    i16Index = 0;

	    if ((IPCGlobals.u8Flags & IPC_EVENT_DISABLE) == IPC_EVENT_DISABLE)
		printf("WARNING : Events are currently disabled\r\n");
	    
	    /* Print number of entries available */
	    printf("%d used, %d available.\r\n", eventTabCount(), EVENTTAB_MAX_ENTRY);
	    
	    OSMutexPend(RTCMutex, 0, &u8Err);
	    
	    iLast = NVRAMu16Read(NVRAM_LASTEVENT_INDEX);
	    printf("Last event index = %d\r\n", iLast);
	    
	    eventTabEntry.u32Packed = NVRAMu32Read(NVRAM_LASTEVENT_DATETIME);
	    printf("Last event date/time = %02X/%02X %02X:%02X\r\n",
		   eventTabEntry.BCD.u8Month, eventTabEntry.BCD.u8DayOfMonth,
		   eventTabEntry.BCD.u8Hour, eventTabEntry.BCD.u8Minute);
	    
	    printf("Events Count = %u\r\n", NVRAMu16Read(NVRAM_EVENT_COUNT));
	    OSMutexPost(RTCMutex);
	    
	    if (!eventTabEmpty())
	    {
		printf("Index L MM/DD HH:MM Command\r\n");
		
		while ((i16Index < EVENTTAB_MAX_ENTRY) &&
		       (eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_ERASED))
		{
		    if (eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_DELETED)
		    {
			printf("%5d ", i16Index);

			/* if this entry was the last used by event, mark it */
			if (i16Index == iLast)
			    printf("=>");
			else
			    printf("  ");
			
			printBCDwc(eventTable[i16Index].BCD.u8Month, EVENTTAB_CHAR_WC, EVENTTAB_BCD_WC);
			putchar('/');
			printBCDwc(eventTable[i16Index].BCD.u8DayOfMonth, EVENTTAB_CHAR_WC, EVENTTAB_BCD_WC);
			putchar(' ');
			printBCDwc(eventTable[i16Index].BCD.u8Hour, EVENTTAB_CHAR_WC, EVENTTAB_BCD_WC);
			putchar(':');
			printBCDwc(eventTable[i16Index].BCD.u8Minute, EVENTTAB_CHAR_WC, EVENTTAB_BCD_WC);

			/* Print commandline here ... */
			
			printf(" \"%s\"", eventTable[i16Index].strCmdLine);
			
#if DEBUG >= 1
			printf(" 0x%08lX 0x%04lX", eventTable[i16Index].u32Packed);
#endif
			printf("\r\n");
		    }
		    i16Index++;
		}
	    }
	    else
		printf("No Entries\r\n");
	}
	break;
    case 'd' : // Delete a single entry
	if (flashLocked())
	{
	    printf("Unlock flash first\r\n");
	}
	else
	{
	    p = getNextParameter();
	    
	    if (*p != '\0')
	    {
		i16Index = stoi(p, 10);
		if (eventTable[i16Index].u32Packed != EVENTTAB_ENTRY_ERASED)
		{
		    OSMutexPend(HardwareMutex, 0, &u8Err);
		    eventTabDel(i16Index);
		    OSMutexPost(HardwareMutex);
		    
		    OSMutexPend(RTCMutex, 0, &u8Err);
		    NVRAMu16Write(NVRAM_LASTEVENT_INDEX, 0xFFFF);
		    OSMutexPost(RTCMutex);
		    
		    printf("Deleted entry %d\r\n", i16Index);
		}
		
		/* If all are deleted then indicate a fault */
		if (eventTabEmpty())
		{
		    printf("Event table is empty\r\n");
		    instrumentErrorCode |= IEC_EVENTTAB_NOENTRIES;
		}
	    }
	}
	break;
    case 'a' : // Add a single entry
	if (flashLocked())
	{
	    printf("Unlock flash first\r\n");
	}
	else
	{
	    eventTabEntry.u32Packed = EVENTTAB_ENTRY_ERASED;

	    p = getNextParameter();
	    
	    if (*p != '\0')
	    {
		/* Month */
		if (*p != 0)
		{
		    u8Temp = strToBCDwc(p, EVENTTAB_CHAR_WC, EVENTTAB_BCD_WC, &u8Mask);
		    if (((u8Mask == 0) && ((u8Temp == 0) || (u8Temp > 12))) ||
			((u8Mask != 0) && (u8Temp > 12)))
		    {
			printf("Invalid month. Allowable range is 1..12 or *\r\n");
			u8Err = 1;
		    }
		    else
			eventTabEntry.BCD.u8Month = BIN2BCD(u8Temp) + u8Mask;
		}
		else
		    u8Err = 1;
		
		/* Day */
		p = getNextParameter();
		if (*p != 0)
		{
		    u8Temp = strToBCDwc(p, EVENTTAB_CHAR_WC, EVENTTAB_BCD_WC, &u8Mask);
		    if (((u8Mask == 0) && ((u8Temp == 0) || (u8Temp > 31))) ||
			((u8Mask != 0) && (u8Temp > 31)))
		    {
			printf("Invalid day of month. Allowable range is 1..31 or *\r\n");
			u8Err = 1;
		    }
		    else
			eventTabEntry.BCD.u8DayOfMonth = BIN2BCD(u8Temp) + u8Mask;
		}
		
		/* Hour */
		p = getNextParameter();
		if (*p != 0)
		{
		    u8Temp = strToBCDwc(p, EVENTTAB_CHAR_WC, EVENTTAB_BCD_WC, &u8Mask);
		    if (u8Temp <= 23)
			eventTabEntry.BCD.u8Hour = BIN2BCD(u8Temp) + u8Mask;
		    else
		    {
			printf("Invalid hour. Allowable range is 0..23 or *\r\n", u8Temp);
			u8Err = 1;
		    }
		}
		else
		    u8Err = 1;
		
		/* Minute */
		p = getNextParameter();
		if (*p != 0)
		{
		    u8Temp = strToBCDwc(p, EVENTTAB_CHAR_WC, EVENTTAB_BCD_WC, &u8Mask);
		    if (u8Temp <= 59)
			eventTabEntry.BCD.u8Minute = BIN2BCD(u8Temp) + u8Mask;
		    else
		    {
			printf("Invalid minute. Allowable range is 0..59 or *\r\n", u8Temp);
			u8Err = 1;
		    }
		}
		else
		    u8Err = 1;
		
		/* Command */
		p = getNextParameter();
		if (*p != 0)
		{
		    if (strlen(p) < EVENTTAB_CMDLINE_LEN)
		    {
			if (FindCommand(p) <= -1)
			{
			    printf("ERROR : Unknown command '%s'. Try using quotes (\") around the command.\r\n", p);
			    u8Err = 1;
			}
			else	
			    strcpy(eventTabEntry.strCmdLine, p);
		    }
		    else
		    {
			printf("ERROR : Command line too long. Limited to %d characters\r\n", EVENTTAB_CMDLINE_LEN);
			u8Err = 1;
		    }
		}
		else
		    u8Err = 1;
		
		if (u8Err != 1)
		{
		    OSMutexPend(HardwareMutex, 0, &u8Err);
		    i16Index = eventTabAdd(&eventTabEntry);
		    OSMutexPost(HardwareMutex);
		    		    
		    instrumentErrorCode &= ~IEC_EVENTTAB_NOENTRIES;

		    if (i16Index == -1)
			printf("ERROR : no more space available in event table\r\n");
		    else
		    {
			OSMutexPend(RTCMutex, 0, &u8Err);
			NVRAMu16Write(NVRAM_LASTEVENT_INDEX, 0xFFFF);
			OSMutexPost(RTCMutex);
		    }
		}
		else
		    printf("ERROR : Invalid or missing parameters\r\n");
	    }
	}
	break;	
    case 'r' :
	{
	    /* Reset NVRAM Counters */
	    OSMutexPend(RTCMutex, 0, &u8Err);
	    NVRAMu32Write(NVRAM_LASTEVENT_DATETIME, 0);
	    NVRAMu16Write(NVRAM_EVENT_COUNT, 0);
	    OSMutexPost(RTCMutex);
	}
	break;
    default :
	{
	    printf("Usage : event <e|p|d|a|r>\r\n");
	    printf(" a <Month> <Day> <Hour> <Minute> \"<Command>\" - add entry\r\n");
	    printf(" d <Index>     - delete entry\r\n");
	    printf(" e             - erase table\r\n");
	    printf(" p             - print table\r\n");
	    printf(" r             - reset counters\r\n");
	}
    }
    
    if ((configTable.u8Messages == 1) && (instrumentErrorCode & IEC_OSTIME_UNSYNC))
	printf("**ERROR** Events are disabled. Please set clock with the 'clock s' command.\r\n");
}

/*
 * sync clocks
 */
void syncCmd(char *strArgs)
{
    if (syncOSToRTC(OSTIME_SYNC_LIMIT, OSTIME_SYNC_RETRIES))
	printf("OSTime Synchronised to RTC to within %ds\r\n", OSTIME_SYNC_LIMIT);
    else
	printf("Unable to synchronise OSTime to RTC. Clock/Oscillator problem.\r\n");
}

/*
 * Camera - Controls turning on/off/toggle/taking pictures with the camera
 */
void cameraCmd(char *strArgs)
{
    int iReturn = CC_NO_ERROR;
    INT8U u8Err;
    char *p;
    
    p = getFirstParameter(strArgs);
    p = getNextParameter();
    
    switch(*p) {

    	case '0' : // Turn off camera
		{
	    	OSMutexPend(HardwareMutex, 0, &u8Err);
	    	iReturn = controlCamera(CC_STAGE_POWEROFF);
	    	OSMutexPost(HardwareMutex);
		} break;

   		case '1' : // Turn on camera
		{
	    	OSMutexPend(HardwareMutex, 0, &u8Err);
	    	iReturn = controlCamera(CC_STAGE_POWERON);
	    	OSMutexPost(HardwareMutex);
		} break;

		case 'f' : /* Full wakeup/picture/shutdown */
		{	
	    	OSMutexPend(HardwareMutex, 0, &u8Err);
	    	iReturn = controlCamera(CC_STAGE_POWERON_DELAY);
	    	if (iReturn == CC_NO_ERROR) 
	    	{
			iReturn = controlCamera(CC_STAGE_SHOOT_DELAY);
		
		 	if (iReturn == CC_NO_ERROR)
		 		iReturn = controlCamera(CC_STAGE_POWEROFF);
	    	}	
	    	OSMutexPost(HardwareMutex);
		} break;
	
	   	case 's' : /* Shoot a frame - pulse focus & shutter */
		{
	    	OSMutexPend(HardwareMutex, 0, &u8Err);
	    	iReturn = controlCamera(CC_STAGE_SHOOT);
	    	OSMutexPost(HardwareMutex);
		} break;
	
		case 'p' : /* pulse camera power on/off quickly to retain clock */
		{
	    	OSMutexPend(HardwareMutex, 0, &u8Err);
	    	iReturn = controlCamera(CC_PULSE);
	    	OSMutexPost(HardwareMutex);
		} break;		
    	default : 
		{
	    	printf("Usage : camera <1|0|p|f|s> <n>\r\n");
	    	printf(" 1 - Turn Camera on\r\n");
	    	printf(" 0 - Turn Camera off\r\n");
	    	printf(" p - Pulse the camera power on then off to recharge clock power\r\n");
		  	printf("     (Length of pulse controlled by config parameter 'H')\r\n");
	    	printf(" f - Full cycle : Turn on/Shoot/Turn off\r\n");
	    	printf(" s - Shoot a picture\r\n");
		} break;
    }
    
    if (iReturn != CC_NO_ERROR)
    {
		printf("Error while controlling camera (%d)\r\n", iReturn);
		switch(iReturn)
		{	
			case CC_VINPUT_FAULT: 
				printf("Input voltage is too low to turn on camera.\r\n");
				printf("More than %.1f volts is required.\r\n", configTable.fVInputCamera);
				break;
			case CC_CAMERA_NOT_ACTIVE:
				printf("Camera did not seem to activate.\r\n");
				printf("Check shutter release cable.\r\n");
				break;
		}
    }
}

/*
 * Sets the LED's
 */
void ledCmd(char *strArgs)
{
    char *p;
    
    p = getFirstParameter(strArgs);
    p = getNextParameter(); 
        
    switch(*p)
    {
    case 'r' : 
	{
	    p = getNextParameter();
	    
	    if (*p != 0)
	    {
		u8LEDRed = (*p - '0');
		uiLEDOffTimer = LED_OFF_TIME;
	    }
	}
	break;
    case 'g' :
	{
	    p = getNextParameter();
	    
	    if (*p != 0)
	    {
		u8LEDGreen = (*p - '0');
		uiLEDOffTimer = LED_OFF_TIME;
	    }
	}
	break;
    case 'a' :
	{
	    p = getNextParameter();

	    if (*p != 0)
	    {
		u8LEDGreen = (*p - '0');
		u8LEDRed = (*p - '0');
		uiLEDOffTimer = LED_OFF_TIME;
	    }
	}
	break;
    case 't' :
	{
	    p = getNextParameter();
	    
	    if (*p != 0)
	    {
		uiLEDOffTimer = (unsigned int)stoi(p, 10);
	    }
	}
	break;
    default : 
	{
	    printf("Usage : led t <value>\r\n");
	    printf("  <value>  : led timer value in minutes\r\n");
	    printf("             (turn off in %u minutes)\r\n", uiLEDOffTimer);
	};
	break;
    }
}

/* 
 * Does a full picture cycle - open shutter, take picture, close shutter
 * 
 */
void pictureCmd(char *strArgs)
{
    INT8U u8Err;
    int iError;
    
    OSMutexPend(HardwareMutex, 0, &u8Err);

    iError = controlCamera(CC_STAGE_POWERON_DELAY);
    
    if ((iError == CC_NO_ERROR) &&
	(controlCamera(CC_STAGE_QUERY) == CC_CAMERA_ON))
    {
	iError = controlServo(SERVO_ON);

	if (iError == SERVO_NO_ERROR)
	{
	    iError = controlServo(SERVO_OPEN);
	    
	    if (iError == SERVO_NO_ERROR)
		iError |= controlCamera(CC_STAGE_SHOOT_DELAY);
	    
	    /* Close shutter */
	    iError |= controlServo(SERVO_CLOSE);
	}
	
	/* turn off servo */
	iError |= controlServo(SERVO_OFF);
    }
    
    /* Turn off camera */
    iError |= controlCamera(CC_STAGE_POWEROFF);

    OSMutexPost(HardwareMutex);
    
    if (iError != CC_NO_ERROR)
	printf("Fault while taking picture (%d)\r\n", iError);
}


/*
 * dipsw command - shows status of DIP switches
 */
void dipswCmd(char *strArgs)
{
    int i;
    
    for (i = 1; i <= 4; i++)
    {
	printf("Switch %d = ", i);
	if (dipSwitch(i))
	    printf("on\r\n");
	else
	    printf("off\r\n");	
    }
}
