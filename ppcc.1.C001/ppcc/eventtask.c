/*
 *	Copyright (C) 2004 Australian Antarctic Division
 *
 * This file contains functions used to to manipulate eventTasks.
 */

#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <float.h>

#ifdef TARGET 
# include <signal.h>
#endif

#include <ppcc.h>
#include <ucosii.h>
#include <cmd.h>
#include <ds3234.h>
#include <eventtab.h>

#include <ppcc/main.h>
#include <ppcc/util.h>
#include <ppcc/camera.h>
#include <ppcc/eventtask.h>

#ifndef DEBUG
# define DEBUG 0
#endif

OS_STK eventTaskStk[EVENT_TASK_STK_SIZE];

#define ZERO_PERIOD_SLEEP 60       /* time to sleep when perid = 0, Seconds */

/*
 * OSTimeDlyBreak - extension of ucosII OSTimeDly to allow long sleeps
 *                  and to allow breakouts. u32Sleep is number of ticks
 *                  to sleep. Relies on the i8Signal variable being set
 *                  before the OSTimeDlyResume(eventTask_Prio) is called
 * 
 * Returns 1 if the delay has been resumed, 0 otherwise.
 * 
 */
int OSTimeDlyBrk(INT32U u32Sleep, uint8_t *u8Signal)
{
    INT16U u16Loops;
    INT16U u16SleepRemain;
    
    /* Now loop around OSTimeDly to sleep for the total time required */
    u16Loops = (INT16U)(u32Sleep / 65536L);
    u16SleepRemain = (INT16U)(u32Sleep % 65536L);
    
    /* Sleep for the remainder first */
    OSTimeDly(u16SleepRemain);
    
    if ((*u8Signal & IPC_UPDATE) != 0)
		return 1;
    
    /* Now sleep for the longer periods */
    while (u16Loops > 0)
    {
		/* Call OSTimeDly twice because we can't call OSTimeDly(65536) */
		OSTimeDly(32768);
	
		if ((*u8Signal & IPC_UPDATE) != 0)
	   	 return 1;
	
		OSTimeDly(32768);
	
		if ((*u8Signal & IPC_UPDATE) != 0)
	   	 return 1;
	
		u16Loops--;
    }
    
    return 0;
}

/******************************************************/
/* Private globals for the event task only             */
/* Made global so that stack space is minimised..     */

INT32U u32TimeStart;        // Absolute time : Actual task started
INT32U u32TimeEnd;          // Absolute time : Actual task ended
INT32U u32Period;           // delta time    : Fixed length of period 
INT32U u32PeriodStart;      // Absolute time : Start of a period
INT32U u32PeriodEnd;        // Absolute time : End of a period
INT32U u32Offset;           // delta time    : Offset from start of Period
INT32U u32MeasureTime;      // delta time    : Actual task time
INT32U u32PeriodCount = 0;  // number of periods 
INT32U u32Sleep;            // total time to sleep in OS_TICKS
int iIndex;                 // Index to eventtab table
int iCmdIndex;

RTCTimeStruct LastRTCTime;
RTCTimeStruct ThisRTCTime;
eventTabEntryStruct eventTabEntry;

void eventTask(void *pdata)
{
    INT8U u8Err;
    
#ifdef HOST
    pdata = pdata;				/* Prevent compiler warning */
#endif
    
    while (1)
    {	
	/* Synchronise OSTime to RTC - maybe do this a little less often? */
	if (syncOSToRTC(OSTIME_SYNC_LIMIT, OSTIME_SYNC_RETRIES))
	{
	    instrumentErrorCode &= ~IEC_OSTIME_UNSYNC;
	    BISB(IPCGlobals.u8Flags, IPC_UPDATE);
	}
	else if (configTable.u8Messages == 1) 
	{
	    instrumentErrorCode |= IEC_OSTIME_UNSYNC;
	    printf("EVENTTASK : Unable to synchronise OSTIME to RTC.\r\n");
	}

	/* Check powerdown timeout - turn off camera if timed out */
	if ((configTable.uiCameraOffTimer > 0) & (uiCameraOffTimer == 0))
	    if (CC_CAMERA_ON == controlCamera(CC_STAGE_QUERY))
	    {
#if DEBUG >= 1
		printf("EVENTTASK : Camera Off Timer Expired. Turning off camera\r\n");
#endif
		OSMutexPend(HardwareMutex, 0, &u8Err);
		controlCamera(CC_STAGE_POWEROFF);
		OSMutexPost(HardwareMutex);
	    }

	/* if values have been updated, reset interval vars */
	if ((IPCGlobals.u8Flags & IPC_UPDATE) == IPC_UPDATE)
	{
	    u32PeriodCount = 0;

	    u32Period = ((INT32U)IPCGlobals.uiPeriod) * OS_TICKS_PER_SEC;
	    u32Offset = ((INT32U)IPCGlobals.uiOffset) * OS_TICKS_PER_SEC;
	    
	    /* Ensure u32Offset is at most u32Period  */
	    if (u32Offset > u32Period)
		u32Offset = u32Period;
	    
	    /* Force a re-sync at the end of the first loop */
	    u32PeriodStart = ULONG_MAX-1;
	    u32PeriodEnd = ULONG_MAX;
	    
	    /* acknowledge update */
	    BICB(IPCGlobals.u8Flags, IPC_UPDATE);
	}
	
	/* Check Period & RTC<->OSTIME Sync */
	if ((u32Period > 0) &&
	    ((instrumentErrorCode & IEC_OSTIME_UNSYNC) == 0) &&
	    ((instrumentErrorCode & IEC_EVENTTAB_NOENTRIES) == 0))
	{
	    /***********************************************************/
	    /* Task begins                                             */

	    u32TimeStart = OSTimeGet();
	    
	    /***********************************************************/
	    /* Perform EVENT Task                                       */
	    
	    OSMutexPend(RTCMutex, 0, &u8Err);
	    ds3234BCDReadRTC(&ThisRTCTime);
	    OSMutexPost(RTCMutex);

	    /* Copy across the pertinent fields */
	    eventTabEntry.BCD.u8Month = ThisRTCTime.u8Month;
	    eventTabEntry.BCD.u8DayOfMonth = ThisRTCTime.u8DayOfMonth;
	    eventTabEntry.BCD.u8Hour = ThisRTCTime.u8Hour;
	    eventTabEntry.BCD.u8Minute = ThisRTCTime.u8Minute;
	    
	    if (eventTabEntry.u32Packed != 
		NVRAMu32Read(NVRAM_LASTEVENT_DATETIME))		
	    {
		iIndex = -1;
		
		do 
		{
		    iIndex = eventTabFind(&eventTabEntry, ++iIndex);

#if DEBUG >= 1
		    if (iIndex != -1)
			printf("EVENTTASK : iIndex %d matched\r\n", iIndex);
		    else
			printf("EVENTTASK : No event for this time period\r\n");
#endif
		    
		    if (iIndex != -1)
		    {
			iCmdIndex = FindCommand(eventTable[iIndex].strCmdLine);
			
#if DEBUG >= 1
			printf("EVENTTASK : iCmdIndex %d found\r\n", iCmdIndex);
#endif
			
			/* Only attempt to execute the function if it was found */
			/* and event table is not disabled..                    */
			if ((iCmdIndex >= 0) && 
			    ((IPCGlobals.u8Flags & IPC_EVENT_DISABLE) == 0))
			{
			    /* stop sleeping while executing the command */
			    uiSleep |= SLEEP_EVENTTASK_ACTIVE;
			    
#if DEBUG >= 1
			    printf("EVENTTASK : execute command, u8Flags %u\r\n", IPCGlobals.u8Flags);
#endif
			    
			    /* Execute the command - maybe add a return value? */
			    CmdTab[iCmdIndex].Function(eventTable[iIndex].strCmdLine);
			    
			    /* Update NVRAM counters */
			    OSMutexPend(RTCMutex, 0, &u8Err);
			    NVRAMu16Write(NVRAM_LASTEVENT_INDEX, iIndex);
			    NVRAMu32Write(NVRAM_LASTEVENT_DATETIME, eventTabEntry.u32Packed);
			    NVRAMu16Write(NVRAM_EVENT_COUNT, NVRAMu16Read(NVRAM_EVENT_COUNT) + 1);
			    OSMutexPost(RTCMutex);
			    
			    uiSleep &= ~SLEEP_EVENTTASK_ACTIVE;
			}
			else
			{
#if DEBUG >= 1
			    printf("EVENTTASK : command disabled\r\n");
#endif
			}
		    }
		} while (iIndex != -1);
	    }
	    
	    /**********************************************************/

	    /* re-enable low power sleeps */
	    uiSleep &= ~SLEEP_EVENTTASK_ACTIVE;
	    
	    if ((IPCGlobals.u8Flags & IPC_UPDATE) != 0)
		continue;
		
	    /* Grab time at end */
	    u32TimeEnd = OSTimeGet();

	    /********************************************************/
	    /* Sleep to start of next period                        */
	    	    
	    /* Check for OSTicks rollover and calculate accordingly */
	    if (u32TimeEnd >= u32TimeStart)
		u32MeasureTime = u32TimeEnd - u32TimeStart;
	    else
		u32MeasureTime = ULONG_MAX - u32TimeStart + u32TimeEnd;
	    	    
	    /* Check for OSTime rollovers - maths for calculation of time to sleep
	     * is slightly different for rollover & non-rollover cases */
	    if (u32PeriodStart < u32PeriodEnd)
	    {
		u32Sleep = u32PeriodEnd - u32TimeEnd;
	    }
	    else
	    {
		u32Sleep = ULONG_MAX - u32TimeEnd + u32PeriodEnd;
	    }

	    /* Impose a maximum sleep limit for unexpected cases when clock changes */
	    if (u32Sleep > u32Period)
	    {
		/* re-sync */
		u32PeriodStart = (u32TimeEnd / u32Period) * u32Period + u32Offset;
		u32PeriodEnd = u32PeriodStart + u32Period;

		if (u32PeriodEnd > u32PeriodStart)
		{
		    /* Period boundaries do NOT straddle a rollover */
		    
		    if (u32TimeEnd < u32PeriodStart)
		    {
			/* Sleep to start of measurement */
			u32Sleep = u32PeriodStart - u32TimeEnd;
		    }
		    else
		    {
			/* Missed measurement, sleep to start of next */
			u32Sleep = u32PeriodEnd - u32TimeEnd;
			
			/* Advance points to next period */
			u32PeriodStart = u32PeriodEnd;
			u32PeriodEnd = u32PeriodStart + u32Period;
		    }		    
		}
		else
		{
		    /* Period Boundaries DO straddle a rollover */
		    
		    if ((u32TimeEnd <= u32PeriodStart) && (u32TimeEnd > u32PeriodEnd))
		    {
			/* End time before PeriodStart but after PeriodEnd  */
			/* This is usually caused by a large offset pushing */
			/* Period boundaries beyond EndTime                 */
			/* We can sleep to start of current measurement     */
			u32Sleep = u32PeriodStart - u32TimeEnd;
		    }
		    else if (u32TimeEnd > u32PeriodStart)
		    {
			/* End time is before rollover & after PeriodStart  */
			/* Sleep to next measurement                        */
			u32Sleep = ULONG_MAX - u32TimeEnd + u32PeriodEnd;
			
			u32PeriodStart = u32PeriodEnd;
			u32PeriodEnd = u32PeriodStart + u32Period;
		    }
		    else
		    {
			/* End time is after rollover 
			 * Sleep to next measurement */
			u32Sleep = u32PeriodEnd - u32TimeEnd;
			
			u32PeriodStart = u32PeriodEnd;
			u32PeriodEnd = u32PeriodStart + u32Period;			
		    }
		}
	    }
	    else
	    {
		/* Advance points to next period */
		u32PeriodStart = u32PeriodEnd;
		u32PeriodEnd = u32PeriodStart + u32Period;	    	    
	    }

#if DEBUG >= 1
	    printf("eventTask: PeriodLength %lus Offset %lus TimeEnd %lus NextPeriodStart %lus NextPeriodEnd %lus\r\n",
		   u32Period/OS_TICKS_PER_SEC,
		   u32Offset/OS_TICKS_PER_SEC,
		   u32TimeEnd/OS_TICKS_PER_SEC,
		   u32PeriodStart/OS_TICKS_PER_SEC,
		   u32PeriodEnd/OS_TICKS_PER_SEC);
	    printf("eventTask: Sleeping for %lus\r\n", u32Sleep/OS_TICKS_PER_SEC);
#endif		    							    
	    
	    if (OSTimeDlyBrk(u32Sleep, &IPCGlobals.u8Flags) == 1)
		continue;
	}
	else
	{	    
	    if ((configTable.u8Messages == 1) && (instrumentErrorCode & IEC_OSTIME_UNSYNC))
	    {
		printf("EVENTTASK : Disabled until OSTIME synchronised\r\n");
	    }
	    
	    /* Sleep when period is zero or out of synchronisation */
	    OSTimeDly(ZERO_PERIOD_SLEEP * OS_TICKS_PER_SEC);
	}
    }
}
