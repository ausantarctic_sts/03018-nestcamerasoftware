/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2005 Australian Antarctic Division
 *	Written by kym.newbery@aad.gov.au
 *
 * This file contains functions used to to manipulate EOS-300/EOS-350D cameras.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <float.h>

#include <ppcc.h>
#include <ucosii.h>
#include <adc12.h>

#include <ppcc/main.h>
#include <ppcc/util.h>
#include <ppcc/camera.h>

#ifdef TARGET
# include <signal.h>
#endif

#ifndef DEBUG
# define DEBUG 0
#endif

/*
 * Returns VCamera, scaled & adjusted.
 */
float readVCamera(void)
{
    int iADCResult;

    /* Need SampleTime > 11us */
    adc12PowerUp(ADC12_CLKDIV_8, SAMPLE_HOLD_512, REF_VOLTAGE_2V5);
    iADCResult = adc12Read(ADC12_PPCC_VCAMERA);    
    adc12Shutdown();
    
    return (2.5*(float)(iADCResult)/1024);
}

/*
 * Returns the status of the Camera, true (1) if camera powered up 
 * otherwise false
 */
int8_t cameraActive(void)
{
#ifdef TARGET
    return ((P1IN & P1_CAMERA_SLEEP) == P1_CAMERA_SLEEP);
#endif
#ifdef HOST
    return 1;
#endif
}

/*
 * Controls VCamera, 
 * 
 */
int setVCamera(int8_t i8Level)
{        
#ifdef TARGET
    if (i8Level == CC_CAMERA_OFF)
    {
		BICB(P6OUT, P6_VCAMERA_ENABLE);
    }
    else if (i8Level == CC_CAMERA_ON)
    {
		/* check input level first - VInput must be above minimum */
		if (readVInput() >= configTable.fVInputCamera)
		{
		    BISB(P6OUT, P6_VCAMERA_ENABLE);
		    instrumentErrorCode &= ~IEC_VINPUT_FAULT;
		}
		else
		{
		    instrumentErrorCode |= IEC_VINPUT_FAULT;
		    return CC_VINPUT_FAULT;
		}
    }
#endif    

    return CC_NO_ERROR;
}


/*
 * controlCamera - a one stop camera control shop
 * 
 * performs the bulk of the camera control functions to turn on & take pics
 * eg turning on the camera or wakeup, check VInput & VCamera
 * etc and then shutdown.
 * 
 * By having focus asserted on powerup, the process of bootup from 
 * normal powerdown or from sleep mode is sped up. Focus is asserted
 * almost constantly until picture is taken or an error occurs ... this is
 * ok because it is the way it happens in the mechanical remote
 * control anyway..
 * 
 * Shutter is also opened/closed at the beginning/end of routine
 * 
 * You need hardware mutexes at the caller level around this routine..
 * 
 * You can control the stage at which the camera is activated with
 * the u8Stage which is one of the following :
 * 
 * CC_STAGE_QUERY         - Query power and status of the camera.
 * CC_STAGE_QUERY_POWER   - Query only power
 * CC_STAGE_POWERON       - open shutter, powerup/wake only
 * CC_STAGE_POWERON_DELAY - as above plus power on delay
 * CC_STAGE_SHOOT         - Assert shutter & focus to take a picture
 * CC_STAGE_SHOOT_DELAY   - as above, but delay for exposure & write
 * CC_STAGE_POWEROFF      - poweroff, close shutter
 * CC_PULSE               - power on/power off quickly to retain clock
 *                          in the DSLR (models EOS1000D and later)
 * 
 * For CC_STAGE_QUERY returns
 *    CC_CAMERA_ON
 *    CC_CAMERA_OFF   
 * 
 * If there was an error :
 *    CC_CAMERA_NOT_ACTIVE
 *    CC_CAMERA_VINPUT_FAULT
 *    otherwise CC_NO_ERROR
 * 
 */
int controlCamera(uint8_t u8Stage)
{
    INT16U u16Ticks;

    /* Answer queries first */
    if ((u8Stage == CC_STAGE_QUERY) || 
	(u8Stage == CC_STAGE_QUERY_POWER))
    {
#ifdef TARGET
	if ((P6OUT & P6_VCAMERA_ENABLE) == P6_VCAMERA_ENABLE)
	{
	    if (u8Stage == CC_STAGE_QUERY)
	    {
		if (cameraActive())
		    return CC_CAMERA_ON;
		else
		    return CC_CAMERA_NOT_ACTIVE; 
	    }
	    else
		return CC_CAMERA_ON;
	}
	else
	    return CC_CAMERA_OFF;
#endif
    }

    /* Pulse on/off command */
    if (u8Stage == CC_PULSE)
    {
        /* Test for VInput too low to turn on camera */
        if (setVCamera(CC_CAMERA_ON) != CC_NO_ERROR)
        {
            return CC_VINPUT_FAULT;
        }
		
		setVCamera(CC_CAMERA_ON);
		
		/* Perform Camera powerup delay - seconds conv to ms then OSTicks */
		u16Ticks = (INT16U)(configTable.fClockPowerPulseLength * OS_TICKS_PER_SEC);
		OSTimeDly(u16Ticks);	    	    
		setVCamera(CC_CAMERA_OFF);
	
		return CC_NO_ERROR;
    }

    /* Turn on command */
    if ((u8Stage == CC_STAGE_POWERON) ||
		(u8Stage == CC_STAGE_POWERON_DELAY))
    {
		/* Test for VInput too low to turn on camera */
		if (setVCamera(CC_CAMERA_ON) != CC_NO_ERROR)
		{
		    return CC_VINPUT_FAULT;
		}
	
		/* Reset the camera off timer to the maximum setting
		 * Must do this BEFORE we turn on camera because automatic
		 * timer may be just about to expire.. 
		 */
		uiCameraOffTimer = configTable.uiCameraOffTimer;

#ifdef TARGET
		/* hold down focus while powering up camera to ensure powerup */
#if DEBUG >= 1 
		printf("controlCamera: Hold down focus signal\r\n");
#endif 		
		BISB(P1OUT, P1_CAMERA_FOCUS);
#endif
		
#if DEBUG >= 1 
		printf("controlCamera: Turn on VCamera\r\n");
#endif 		
		setVCamera(CC_CAMERA_ON);
	
		/* Perform Camera powerup delay - seconds conv to ms then OSTicks */
		u16Ticks = (INT16U)(configTable.fTurnOnDelay * OS_TICKS_PER_SEC);
#if DEBUG >= 1
		printf("controlCamera: Camera powerup delay %u ticks\r\n", u16Ticks);
#endif
		OSTimeDly(u16Ticks);	    	    
#if DEBUG >= 1
		printf("controlCamera: Done powerup delay\r\n");
#endif

#ifdef TARGET
		/* release focus */
#if DEBUG >= 1 
		printf("controlCamera: release focus signal\r\n");
#endif 		
		BICB(P1OUT, P1_CAMERA_FOCUS);
#endif
	
		/* Check for camera active after powerup - continue regardless */
		if (!cameraActive())
		{
#if DEBUG >= 1
		    printf("controlCamera: Camera not active after powerup\r\n");
#endif
		    instrumentErrorCode |= IEC_CAMERA_INACTIVE;
			return CC_CAMERA_NOT_ACTIVE;
		}	   

		return CC_NO_ERROR;
    }    
    
    /* Turn off command */
    if (u8Stage == CC_STAGE_POWEROFF)
    {
#if DEBUG >= 1
		printf("controlCamera: CC_STAGE_POWEROFF, Turn off VCamera\r\n");
#endif
		setVCamera(CC_CAMERA_OFF);
	
	return CC_NO_ERROR;
    }

    /* Shoot - just pulse Focus & Shutter to take a picture */
    if ((u8Stage == CC_STAGE_SHOOT) ||
		(u8Stage == CC_STAGE_SHOOT_DELAY))
    {
		u16Ticks = (INT16U)(configTable.fPulseLength * OS_TICKS_PER_SEC);
#if DEBUG >= 1
		printf("controlCamera: CC_STAGE_SHOOT/SHOOT_DELAY, Assert Shutter & focus\r\n");
		printf("controlCamera: CAMERA_SHUTTER/FOCUS pulse length = %u ticks\r\n", u16Ticks);
#endif
#ifdef TARGET
		BISB(P1OUT, P1_CAMERA_FOCUS);
		OSTimeDly(OS_TICKS_PER_SEC);
		BISB(P1OUT, P1_CAMERA_SHUTTER);
		OSTimeDly(u16Ticks);
#endif
#if DEBUG >= 1
		printf("controlCamera: de-assert CAMERA_SHUTTER & CAMERA_FOCUS\r\n");
#endif
#ifdef TARGET
		/* Turn off focus / shutter */
		BICB(P1OUT, P1_CAMERA_SHUTTER | P1_CAMERA_FOCUS);
#endif	
		if (u8Stage == CC_STAGE_SHOOT_DELAY)
		{
	    	/* Taking picture - so wait for exposure & picture to be written to card */
	    	u16Ticks = (INT16U)(configTable.fExposeWriteDelay * OS_TICKS_PER_SEC);
#if DEBUG >= 1
	    	printf("controlCamera: Expose & card write delay %u ticks\r\n", u16Ticks);
#endif
	    	OSTimeDly(u16Ticks);
		}
		return CC_NO_ERROR;
    }
    
    return CC_NO_ERROR;
}
