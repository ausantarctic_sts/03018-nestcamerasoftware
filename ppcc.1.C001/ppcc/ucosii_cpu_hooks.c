/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *
 * This file contains functions used to to manipulate ucosii_cpu_hookss.
 */


#if (OS_CPU_HOOKS_EN == 0)
# include <ppcc/ucosii_cpu_hooks.h>
# ifdef TARGET 
#  include <io.h>
# endif
#endif /* OS_CPU_HOOKS_EN */


#include <ppcc.h>
#include <ppcc/main.h>
#include <ppcc/util.h>
#include <ppcc/buttontask.h>

/***********************************************************************************************************
 *					OS INITIALIZATION HOOK
 *                                            (BEGINNING)
 **********************************************************************************************************
 */
#if (OS_CPU_HOOKS_EN == 0)
void  OSInitHookBegin (void)
{
}
#endif
/*
 * *********************************************************************************************************
 * *                                       OS INITIALIZATION HOOK
 * *                                               (END)
 * *********************************************************************************************************
 * */
#if (OS_CPU_HOOKS_EN == 0)
void  OSInitHookEnd (void)
{
}
#endif
/*
 * *********************************************************************************************************
 * *                                          TASK CREATION HOOK
 * *********************************************************************************************************
 * */
#if (OS_CPU_HOOKS_EN == 0)
void  OSTaskCreateHook (OS_TCB *ptcb)
{
	    ptcb = ptcb;                       /* Prevent compiler warning                                     */
}
#endif

/*
 * *********************************************************************************************************
 *                                           TASK DELETION HOOK
 * *********************************************************************************************************
 * */
#if OS_CPU_HOOKS_EN == 0
void  OSTaskDelHook (OS_TCB *ptcb)
{
	    ptcb = ptcb;                       /* Prevent compiler warning                                     */
}
#endif

/*
 * *********************************************************************************************************
 * *                                             IDLE TASK HOOK
 * *********************************************************************************************************
 * */
#if (OS_CPU_HOOKS_EN == 0)
void  OSTaskIdleHook (void)
{
#ifdef TARGET
    OS_ENTER_CRITICAL(); /* uiSleep is volatile and needs interrupts clear around it */
    if (uiSleep == 0)
    {
	/* Invalidate the DCO tune control */
	DCOTune.u8Control = 0;

        /* Interrupts must be enabled before sleeping, 
         * The eint instruction must be just before the LPM instruction
         * incase an interrupt happens between the two, this causes
         * the interrupt to happen after the LPM instruction */
        OS_EXIT_CRITICAL();

	/* Sleep CPU/SMCLK/MCLK & DCO but not ACLK */
	LPM3;
    }	
    else
    {
        OS_EXIT_CRITICAL();

	/* sleep the CPU */
	LPM0;
    }
#endif
}
#endif


/*
 * *********************************************************************************************************
 * *                                           STATISTIC TASK HOOK
 * *********************************************************************************************************
 * */
#if (OS_TASK_STAT_HOOKS_EN == 0)
void  OSTaskStatHook (void)
{

}
#endif
/*
 * *********************************************************************************************************
 * *                                           TASK SWITCH HOOK
 * *********************************************************************************************************
 * */
#if (OS_TASK_SW_HOOK_EN == 0)
void  OSTaskSwHook (void)
{
#if (OS_TASK_CREATE_EXT_EN > 0)
    TASK_USER_DATA *puser;
    
    puser = OSTCBCur->OSTCBExtPtr;
    if (puser != (TASK_USER_DATA *) 0)
    {
	puser->TaskCtr++;
    }
#endif
}
#endif
/*
 * *********************************************************************************************************
 * *                                           OSTCBInit() HOOK
 * *********************************************************************************************************
 * */
#if (OS_CPU_HOOKS_EN == 0)
void  OSTCBInitHook (OS_TCB *ptcb)
{
    ptcb = ptcb;                                           /* Prevent Compiler warning                 */
}
#endif
/*
 * *********************************************************************************************************
 * *                                               TICK HOOK
 * *********************************************************************************************************
 * */
#if (OS_CPU_HOOKS_EN == 0)
void  OSTimeTickHook (void)
{
    unsigned int uiTimerB;
    
    /**************************************************/
    /* decrement timeout counters on minute intervals */
    if ((OSTimeGet() % (60 * OS_TICKS_PER_SEC)) == 0)
    {
	if (uiCameraOffTimer > 0)
	    uiCameraOffTimer--;
	if (uiLEDOffTimer > 0)
	    uiLEDOffTimer--;	    
    }

    if (uiLEDOffTimer > 0)
    {
	if ((u8LEDRed == LED_BIST_ON) || (u8LEDGreen == LED_BIST_ON))
	{
	    // ignore
	}
	else
	{
	    /* Update flashing LEDs */
	    if (instrumentErrorCode >= IEC_CRITICAL)
	    {
		u8LEDRed   = LED_FLASH_2HZ;
		u8LEDGreen = LED_OFF;
	    }
	    else
	    {
		u8LEDRed   = LED_OFF;	    
		u8LEDGreen = LED_FLASH_2HZ;
	    }
	}
    }
    else
    {
	u8LEDRed = LED_OFF;
	u8LEDGreen = LED_OFF;
    }
    
    ledFlashUpdate();

    /**************************************************/
    /* Button Press / De-bounce                       */
    buttonCheck();
    
    /**************************************************************/
    /* Increment the number of times we have attempted a DCO Tune */
    DCOTune.u16Ticks++;

    /* make sure DCOtunes are performed regularly 
     * by forcing the CPU out of LPM3 for the minimum number
     * of OSTicks.
     */
    if (DCOTune.u16Ticks >= (DCOTUNE_RETUNE_PERIOD * OS_TICKS_PER_SEC))
	uiSleep |= SLEEP_FORCE_DCOTUNE;
    
    /***************************************************/
    /* Now do a DCO Tune                               */
    
    uiTimerB = TBR;

    /* if we have captured two timers in a row, calculate the period */
    if (DCOTune.u8Control > 0)
	DCOTune.u16Period = uiTimerB - DCOTune.u16Last;
    DCOTune.u16Last = uiTimerB;
        
    /* If we have two consecutive captures and thus a valid period,
     * we can perform a DCOTune
     */
    if (DCOTune.u8Control > 1)
    {
	
	/* calc error */
	DCOTune.i16Error = DCOTune.u16Period - (SMCLK * WDTCLKDIV)/ACLK;

	/* throw away bad DCO tuning errors */
	if (abs(DCOTune.i16Error > DCOTUNE_BIG_ERROR_LIMIT))
	{
	    instrumentErrorCode |= IEC_DCO_BIG_ERROR;
	    DCOTune.u8Control = 1;
	    return;
	}
	
	/* perform the feedback part of the control loop */
	DCOTune.i16NewDCO = DCOCTL - (DCOTune.i16Error/64);	
	DCOTune.u8Rsel = BCSCTL1 & (RSEL0 | RSEL1 | RSEL2);
	
	if (DCOTune.i16NewDCO < 0)
	{
	    if (DCOTune.u8Rsel > 0)
	    {
		BCSCTL1--;
		DCOCTL = 0x60;
	    }
	}
	else if (DCOTune.i16NewDCO > 255)
	{
	    if (DCOTune.u8Rsel < 7)
	    {
		BCSCTL1++;
		DCOCTL = 0x60;
	    }
	}
	else
	{
	    DCOCTL = (uint8_t)(DCOTune.i16NewDCO & 0xFF);
	}
    }
    
    if (DCOTune.u8Control < 4)
	DCOTune.u8Control++;
    else
    {
	/* we have done a DCO tune so can go back to sleep */
	DCOTune.u16Ticks = 0;
	uiSleep &= ~SLEEP_FORCE_DCOTUNE;
    }
}
#endif
