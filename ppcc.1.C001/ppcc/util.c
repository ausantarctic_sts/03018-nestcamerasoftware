/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by kym_new@localhost
 *
 * This file contains functions used to to manipulate utils.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <float.h>

#include <ppcc.h>
#include <ucosii.h>
#include <adc12.h>
#include <ds3234.h>

#include <ppcc/main.h>
#include <ppcc/camera.h>
#include <ppcc/servo.h>
#include <ppcc/util.h>
#include <ppcc/camera.h>
#include <ppcc/servo.h>

#ifdef TARGET
# include <signal.h>
#endif

#ifndef DEBUG
# define DEBUG 0
#endif

/*
 * Returns VInput, scaled & adjusted
 */
float readVInput(void)
{
    int iADCResult;
    
    /* Need Ref 2.5v, SampleHoldTime > 3.3ms */
    adc12PowerUp(ADC12_CLKDIV_8, SAMPLE_HOLD_512, REF_VOLTAGE_2V5);
    iADCResult = adc12Read(ADC12_PPCC_VINPUT);
    adc12Shutdown();
        
    return (2.5*(float)(iADCResult)/512);
}

/*
 * Returns VServo, scaled & adjusted - Not implemented earlier to rev 6 hardware
 */
float readVServo(void)
{
    int iADCResult;
    
    /* Need Ref 2.5v, SampleHoldTime > 11ms */
    adc12PowerUp(ADC12_CLKDIV_8, SAMPLE_HOLD_512, REF_VOLTAGE_2V5);
//    iADCResult = adc12Read(ADC12_PPCC_VSERVO);
    iADCResult = adc12Read(ADC12_PPCC_VSERVO);
    adc12Shutdown();
        
    return (2.5*(float)(iADCResult)/1024);
}


/*
 * Returns MSP430 internal diode temperature
 */
float readMSP430Temp(void)
{
    int iADCResult;
    float fResult;
    
    adc12PowerUp(ADC12_CLKDIV_8, SAMPLE_HOLD_512, REF_VOLTAGE_1V5);
    OSTimeDly(150/OS_TICKS_PER_SEC);    
    iADCResult = adc12Read(ADC12_TEMP_DIODE);
    adc12Shutdown();
    
    /*
     * The following formula is a scaled version of
     * 
     *    Vtemp = 0.00355 x TempC + 0.986
     * 
     * Taken from the TI User Manual. Valid only for REF=1V5
     */
    fResult = 1.5*((float)iADCResult)/4096.0;
    return ((fResult - 0.986)/0.00355);
}


/*
 * Print out the clock - done as a function to minimise constant stack space
 */
void printClock(void)
{
    RTCTimeStruct RTCTime;
    INT8U u8Err;

    OSMutexPend(RTCMutex, 0, &u8Err);
    ds3234BCDReadRTC(&RTCTime);
    OSMutexPost(RTCMutex);
    
    printf("20%02X/%02X/%02X %02X:%02X:%02X\r\n",
	   RTCTime.u8Year,
	   RTCTime.u8Month,
	   RTCTime.u8DayOfMonth,
	   RTCTime.u8Hour,
	   RTCTime.u8Minute,
	   RTCTime.u8Second);
}

/*
 * Synchronise OSTime to the RTC seconds/minute - 
 * Tries for iRetries times or until error less then iLimit
 * all othertimes returns error in seconds
 */
int syncOSToRTC(int iLimit, int iRetries)
{
    INT8U u8Err;
    uint8_t u8RTCSecs, u8OSSecs;
    int iError = INT_MAX;
    int iCount = 0;
    INT32U u32OSTime;
    RTCTimeStruct RTCTime;

    /* Can't synchronise until clock osc is enabled AND!
     * clock is set */
    if (instrumentErrorCode & IEC_RTC_OSCFAULT)
    {
	instrumentErrorCode |= IEC_OSTIME_UNSYNC;
	return 0;
    }

#if DEBUG >= 2
    printf("syncOSToRTC: iError %d iLimit %d iCount %d iRetries %d\r\n",
	   iError, iLimit, iCount, iRetries);
#endif
    
    while ((abs(iError) > iLimit) &&
	   (iCount++ < iRetries))
    {
	/* Read the RTC */
	OSMutexPend(RTCMutex, 0, &u8Err);
	ds3234BCDReadRTC(&RTCTime);
	OSMutexPost(RTCMutex);
	
	u8RTCSecs = BCD2BIN(RTCTime.u8Second);
	
	/* Get system time */
	u32OSTime = OSTimeGet() / OS_TICKS_PER_SEC;
	u8OSSecs = u32OSTime % 60;
	
	/* then calc error */
	iError = u8RTCSecs - u8OSSecs;
	
#if DEBUG >= 2
	printf("syncOSToRTC: iCount %d OSTime %lu OSSecs %u RTCSecs %u Error %d\r\n", 
	       iCount, u32OSTime * OS_TICKS_PER_SEC, u8OSSecs, u8RTCSecs, iError);
#endif
	
	/* Shift OSTime to match RTC Time */
#ifdef TARGET
	OSTimeSet((u32OSTime + iError) * OS_TICKS_PER_SEC);
#endif
    }
    
    if ((abs(iError) <= iLimit) || (iCount <= iRetries))
    {
	instrumentErrorCode &= ~IEC_OSTIME_UNSYNC;
	return 1; // got synchronisation
    }
    else
	return 0;
}

/*
 * LED flashing - moved from ucos_cpu_hooks
 *
 */
void ledFlashUpdate(void)
{
#ifdef TARGET
    INT16U u16LEDTime;
    
    /**************************************************/
    /* Control RED/GRN/YEL Leds */
    u16LEDTime = OSTimeGet();


    
    /* RED */
    if (u8LEDRed == LED_OFF)
	    BICB(P5OUT, P5_LED_RED);
    else if ((u8LEDRed == LED_ON) || (u8LEDRed == LED_BIST_ON))
	    BISB(P5OUT, P5_LED_RED);	
    else
    {
	    if (u16LEDTime % (OS_TICKS_PER_SEC / u8LEDRed) == 0)
	    {
	        if (P5OUT & P5_LED_RED)
    		    BICB(P5OUT, P5_LED_RED);
	        else
    		    BISB(P5OUT, P5_LED_RED);
	    }
    }

    /* GREEN */
    if (u8LEDGreen == LED_OFF)
    	BICB(P5OUT, P5_LED_GREEN);
    else if ((u8LEDGreen == LED_ON) || (u8LEDGreen == LED_BIST_ON))
    	BISB(P5OUT, P5_LED_GREEN);
    else
    {
	    if (u16LEDTime % (OS_TICKS_PER_SEC / u8LEDGreen) == 0)
	    {
	        if (P5OUT & P5_LED_GREEN)
    		    BICB(P5OUT, P5_LED_GREEN);
	        else
    		    BISB(P5OUT, P5_LED_GREEN);
	    }
    } 
#endif
}

/* 
 * init port bits for relay
 */
void relayInit(void)
{
#ifdef TARGET
    /* select pins as plain I/O */
    BICB(P2SEL, P2_RELAY_SET | P2_RELAY_RESET | P2_RELAY_STATE);
    BICB(P2DIR, P2_RELAY_STATE); // make relay state input
   
    // turn off coil drivers
    BICB(P2OUT, P2_RELAY_SET | P2_RELAY_RESET);
    BISB(P2DIR, P2_RELAY_SET | P2_RELAY_RESET);  // make all gate drive out
#endif
}

/*
 * return state of relay
 */
int  relayRead(void)  
{
#ifdef TARGET
    return (((P2IN & P2_RELAY_STATE) == P2_RELAY_STATE) ? 1 : 0);
#endif
#ifdef HOST
    return 0;
#endif
}

/* Delay for 50ms */
#define RELAY_DELAY ((50*OS_TICKS_PER_SEC)/1000) 

/* 
 * set the relay
 */
void relaySet(void)
{
#ifdef TARGET
    /* turn on set */
    BISB(P2OUT, P2_RELAY_SET);
    
    /* pause for relay delay */
    OSTimeDly(RELAY_DELAY);    

    /* turn off */
    BICB(P2OUT, P2_RELAY_SET);
#endif
}


/*
 * reset the relay
 */
void relayReset(void)
{
#ifdef TARGET
    /* turn on reset */
    BISB(P2OUT, P2_RELAY_RESET);
    
    /* pause for relay delay */
    OSTimeDly(RELAY_DELAY);    

    /* turn off */
    BICB(P2OUT, P2_RELAY_RESET);
#endif
}

/* 
 * read the DIP switch 1 to 4, return state of bit
 * 
 * If iSelect = 0, then return 1 if any bits are set. 
 * 
 */
int dipSwitch(int iSelect)
{
#ifdef TARGET
    switch (iSelect)
    {
    case 0 : return ((P5IN & P5_SW2_ALL) != 0 ? 1 : 0); break;
    case 1 : return ((P5IN & P5_SW2_1) == P5_SW2_1 ? 1 : 0); break;
    case 2 : return ((P5IN & P5_SW2_2) == P5_SW2_2 ? 1 : 0); break;
    case 3 : return ((P5IN & P5_SW2_3) == P5_SW2_3 ? 1 : 0); break;
    case 4 : return ((P5IN & P5_SW2_4) == P5_SW2_4 ? 1 : 0); break;
    }
#endif
    return -1;
}


/*
 * Manufacturing Built-In-Self-Test (BIST)
 * 
 * Usually only invoked when dipswitch 3 is set.
 * 
 * Things checked :
 *  - VInput checked to be 12V +/- 0.5V
 *  - VCamera turned on, voltage measured, VCAMERA_PGOOD checked, VCamera turned off.
 *  - VCServo turned on, voltage measured, VSERVO_PGOOD checked, VServo turned off.
 *  - Real Time Clock checked. 
 *  - Check CAMERA_SHUTTER loop back to Button 1 input
 *  - Check CAMERA_FOCUS loop back to Button 2 input
 *   
 * 
 * 
 */

#define BIST_VCAMERA_MIN  7.5
#define BIST_VCAMERA_MAX  8.5
#define BIST_VSERVO_MIN   5.5
#define BIST_VSERVO_MAX   6.5
#define FAIL_STR "\033[7m\033[31mFAIL\033[0m"
#define PASS_STR "\033[7m\033[32mPASS\033[0m"

void builtInSelfTest(void)
{
#ifdef TARGET
    float fVInput, fVCamera, fVServo;
    int i, iOverallPass;
    uint8_t u8Temp;
    RTCTimeStruct RTCstart, RTCend, RTCsave;
    
    // turn LED's off to begin with - drive directly
    uiLEDOffTimer = 0;
    u8LEDRed = LED_OFF;
    u8LEDGreen = LED_OFF;
    
    iOverallPass = 1;

    //-------------------------------------------------
    // Read VInput
    fVInput = readVInput();
    
    printf("Input Voltage = %02.3fV,", fVInput); 
    if ((fVInput >= configTable.fVInputMin) && (fVInput <= configTable.fVInputMax))
	printf("%s\r\n", PASS_STR);
    else
	printf("%s\r\n", FAIL_STR);
    
    //-------------------------------------------------
    // Test VCAMERA PSU
    
    // turn on VCamera
    BISB(P6OUT, P6_VCAMERA_ENABLE);
    
    // Pause to stabilise?
    OSTimeDly(1*OS_TICKS_PER_SEC);
    
    fVCamera = readVCamera();
    
    printf("VCAMERA = %02.3fV,", fVCamera);
    
    if ((fVCamera >= configTable.fVCameraMin) && (fVCamera <= configTable.fVCameraMax))
	printf("%s\r\n", PASS_STR);
    else
    {
	printf("%s\r\n", FAIL_STR);
	iOverallPass = 0;
    }
    
    i = (((P4IN & P4_VCAMERA_PGOOD) == P4_VCAMERA_PGOOD) ? 1 : 0);
    printf("VCAMERA_PGOOD,%d,", i);
    if (i == 1)
	printf("%s\r\n", PASS_STR);
    else
    {
	printf("%s\r\n", FAIL_STR);
	iOverallPass = 0;
    }

    OSTimeDly(1*OS_TICKS_PER_SEC);
    
    // turn off VCamera
    BICB(P6OUT, P6_VCAMERA_ENABLE);

    //-------------------------------------------------
    // Test VServo
    OSTimeDly(1*OS_TICKS_PER_SEC);
    
    // turn on VServo
    BISB(P6OUT, P6_VSERVO_ENABLE);

    // Pause to stabilise
    OSTimeDly(1*OS_TICKS_PER_SEC);
    
    fVServo = readVServo();

    printf("VSERVO = %02.3f,", fVServo);
    
    if ((fVServo >= configTable.fVServoMin) && (fVServo <= configTable.fVServoMax))
    {
	printf("%s\r\n", PASS_STR);
    }
    else
    {
	printf("%s\r\n", FAIL_STR);
	iOverallPass = 0;
    }

    i = (((P4IN & P4_VSERVO_PGOOD) == P4_VSERVO_PGOOD) ? 1 : 0);
    printf("VSERVO_PGOOD,%d,", i);
    if (i == 1)
	printf("%s\r\n", PASS_STR);
    else
    {
	printf("%s\r\n", FAIL_STR);
	iOverallPass = 0;
    }
    
    // turn off VServo
    BICB(P6OUT, P6_VSERVO_ENABLE);
    
    //--------------------------------------------------
    // Test clock with or without battery
    // Clobbers the existing time - is there a better way?
    // 
    // To test the clock, we first save the clock, then write 
    // "1999/12/31 23:59:59" to the RTC, then wait 2 seconds
    // then read it back and check that it equals "2000/01/01 00:00:01",
    // then restore the original value (yes - makes it drift by 2 seconds)
    
    u8Temp = ds3234Read(DS3234_STA);
    
    // Read clock to saved registers
    ds3234BCDReadRTC(&RTCsave);
    
    printf("DS3234 RTC,");

    RTCstart.u8Year = BIN2BCD(07);
    RTCstart.u8Month = BIN2BCD(07); 
    RTCstart.u8DayOfMonth = BIN2BCD(07);
    RTCstart.u8Hour = BIN2BCD(07);
    RTCstart.u8Minute = BIN2BCD(07);
    RTCstart.u8Second = BIN2BCD(07);
    
    // set clock to just before turn of the century
    ds3234BCDWriteRTC(&RTCstart);
    
    // wait for 3 seconds
    OSTimeDly(3 * OS_TICKS_PER_SEC);
    
    ds3234BCDReadRTC(&RTCend);
    
    // compare end time - should be 2 seconds over century
    if ((RTCend.u8Year  == BIN2BCD(7)) &&
	(RTCend.u8Month == BIN2BCD(7)) &&
	(RTCend.u8DayOfMonth = BIN2BCD(7)) &&
	(RTCend.u8Hour == BIN2BCD(7)) && 
	(RTCend.u8Minute == BIN2BCD(7)) &&
	((RTCend.u8Second == BIN2BCD(9)) ||
	 (RTCend.u8Second == BIN2BCD(10))))
    {
	printf("%s\r\n", PASS_STR);
    }
    else
    {
	printf("%s\r\n", FAIL_STR);
	printClock();
	iOverallPass = 0;
    }
    
    // restore clock
    ds3234BCDWriteRTC(&RTCsave);
        
    //--------------------------------------------------
    // Test Loopback
    //
    // Shutter Output -> Button 1

    // assert output, Button1 should be low
    printf("P1_CAMERA_SHUTTER loop back to P1_BUTTON1 : ");
    
    BISB(P1OUT, P1_CAMERA_SHUTTER);
    i = (((P1IN & P1_BUTTON1) == P1_BUTTON1) ? 1 : 0);
    BICB(P1OUT, P1_CAMERA_SHUTTER);

    if (i == 0)
	printf("%s\r\n", PASS_STR);
    else
    {
	printf("%s\r\n", FAIL_STR);
	iOverallPass = 0;
    }
	
    // Focus Output -> Button 2
    printf("P1_CAMERA_FOCUS loop back to P1_BUTTON2 : ");
    BISB(P1OUT, P1_CAMERA_FOCUS);
    i = (((P1IN & P1_BUTTON2) == P1_BUTTON2) ? 1 : 0);
    BICB(P1OUT, P1_CAMERA_FOCUS);
    
    if (i == 0)
	printf("%s\r\n", PASS_STR);
    else
    {
	printf("%s\r\n", FAIL_STR);
	iOverallPass = 0;
    }
    
    //--------------------------------------------------
    // Test RELAY
    
    // Set relay
    relaySet();
    OSTimeDly(1*OS_TICKS_PER_SEC);
    i = relayRead();
    printf("RELAY SET,%d,", i);
    if (i == 1)
	printf("%s\r\n", PASS_STR);
    else
	printf("%s\r\n", FAIL_STR);    
    
    relayReset();
    OSTimeDly(1*OS_TICKS_PER_SEC);
    i = relayRead();
    printf("RELAY RESET,%d,", i);    
    if (i == 0)
	printf("%s\r\n", PASS_STR);
    else
	printf("%s\r\n", FAIL_STR);
    
    relaySet();
    OSTimeDly(1*OS_TICKS_PER_SEC);
    i = relayRead();
    printf("RELAY SET,%d,", i);
    if (i == 1)
	printf("%s\r\n", PASS_STR);
    else
	printf("%s\r\n", FAIL_STR);
    
    //---------------------------------------------------
    // Print overall result & light LED
    printf("OVERALL RESULT,");
    
    uiLEDOffTimer = 10;
    if (iOverallPass == 1)
    {
	printf("%s\r\n", PASS_STR);
	u8LEDRed = LED_OFF;
	u8LEDGreen = LED_BIST_ON;
    }
    else
    {
	printf("%s\r\n", FAIL_STR);
	u8LEDRed = LED_BIST_ON;
	u8LEDGreen = LED_OFF;
    }
#endif
}
