/*
 *  Copyright (C) 2003 Australian Antarctic Division
 *  Written by kym_new <kym.newbery@aad.gov.au>
 *
 * This file contains the main code for the nest camera controller
 *
 * 2010/05/10 - KBN - Updated for REV 6/7 Hardware & manufacturing BIST.
 * 2012/09/09 - KBN - Added camera pulse command, with associated config setting
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <signal.h>

#include <ppcc.h>
#include <mspio.h>
#include <version.h>
#include <ucosii.h>
#include <cmd.h>
#include <adc12.h>
#include <ds3234.h>
#include <usdelay.h>
#include <eventtab.h>
#include <usart0.h>
#include <usart1.h>
#include <nmea.h>

#ifdef HOST
# include <sys/socket.h>
# include <sys/types.h>  
# include <arpa/inet.h>  
# include <netinet/in.h>
# include <fcntl.h>
# include <unistd.h>
static int usart0Socket, usart0Connected;
static struct sockaddr_in usart0Address;
#endif

#include <ppcc/main.h>
#include <ppcc/cmd.h>
#include <ppcc/buttontask.h>
#include <ppcc/eventtask.h>
#include <ppcc/util.h>
#include <ppcc/servo.h>
#include <ppcc/camera.h>

#ifndef DEBUG
# define DEBUG 0
#endif

/* These values are written into flash when the CPU is first programmed,
 * thus they are the DEFAULT values 
 */
configTableSection struct configTableStruct configTable = 
{
    "<NOT SET>",                  /* Flash based serial number                */
    (uint8_t)1,                   /* controls messages, default yes           */
    10.8,                         /* Minimum input to turn on camera          */
    5.0,                          /* Turn on delay in seconds                 */
    30.0,                         /* Expose & Write Picture delay             */
    0.3,                          /* Focus/Shutter pulse length               */
    15,                           /* Camera Off Timer (mins)                  */
    7.0,                          /* Minimum input to turn on servo           */
    SERVO_STEP_DEFAULT,           /* Servo step size                          */
    SERVO_INITIAL_OPEN,           /* Initial servo open position              */ 
    SERVO_INITIAL_CLOSED,         /* Initial servo closed position            */
    SERVO_INITIAL_OVER,           /* Position to move servo to relax bristles */
    11.5,                         /* BIST : VInput minimum                    */
    12.5,                         /* BIST : VInput maximum                    */ 
    5.7,                          /* BIST : VServo minimum                    */
    6.3,                          /* BIST : VServo maximum                    */
    7.7,                          /* BIST : VCamera minimum                   */
    8.3,                          /* BIST : VCamera maximum                   */
    2.0,                          /* Clock Power Pulse Length in seconds      */
};

unsigned int instrumentErrorCode;
static void  CommandTask(void *p);
struct       IPCGlobalsStruct IPCGlobals;

/* Mutex's */
OS_EVENT     *HardwareMutex;    // For all but RTC
OS_EVENT     *RTCMutex;         // For only RTC

/* Power Control / DCO Tuning globals & timers */
volatile unsigned int uiSleep;
struct DCOTuneStruct DCOTune;
volatile unsigned int uiCameraOffTimer;
volatile unsigned int uiLEDOffTimer;

/* Task monitoring */
OS_STK          CmdTaskStk[CMD_TASK_STK_SIZE];
TASK_USER_DATA  TaskUserData[PPCC_TASKS];

/*******************/
/* LED controls    */
uint8_t u8LEDRed = LED_OFF;
uint8_t u8LEDGreen = LED_OFF;

/***********************************************/

int NmeaPutch(int ch)
{
    /* Check for broadcast NMEA packet */   
    if ((Line[0] == '$') && (Line[3] == '0') && (Line[4] == '0'))
	return ch;
    
    NmeaCheckSum ^= ch;
    
    return putchar(ch);
}

inline int port0Putchar(char ch)
{
#ifdef TARGET
    return usart0Putch(ch);
#endif
    
#ifdef HOST
    if(usart0Connected == 0)
    {
        return (int)send(usart0Socket, &ch, 1, 0);
    }
    else
    {
        return 0;
    }
#endif
}

inline int port0Getchar(void)
{
#ifdef TARGET
    return usart0Getch();
#endif
#ifdef HOST
    int c;
    
    if(usart0Connected == 0)
    {
        c = -1;
        (void)recv(usart0Socket, &c, 1, 0);

        return c;
    }
    /* Not connected so fail */
    return -1;
#endif
}

#ifdef TARGET
int putchar(int ch)
{
    int c;
    int i;
    
    c = usart1Putch((char)ch);    
    while ((i = usart1TxEmpty()) == 0);

    return c;
}

int getchar(void)
{
    char ch;
    
    ch = usart1Getch();
    
    /* echo back character */
    usart1Putch(ch);
    
    return ((int)ch);
}
#endif

/*
 * startupDCOTune - Tunes the DCO at startup.
 * 
 * Uses the watchdog as an interval timer without interrupts to
 * measure the DCO which is driven by SMCLK. It then sets the DCO 
 * control bits accordingly.
 * 
 * Assumes 
 *   1. SMCLK = DCO which drives TimerB
 *   2. ACLK  = 32768KHz
 *   3. TimerB has not been configured
 *   4. Watchdog has not been configured
 *   
 * Performs DCO measurement/adjustment until the the last STABLE_COUNT 
 * consecutive error values are less than the ERROR_LIMIT, then it quits
 * leaving the global DCO tune values to be picked up by the main
 * ucosii time tick hook tune routine.
 * 
 */
void startupDCOTune(void)
{
#ifdef TARGET    
    int iStable;
    unsigned int uiTimerB;
    
    /* Clear Watchdog timer flag */
    IFG1 &= ~WDTIFG;
    
    /* configure watchdog as interval timer, 16ms timeout */
    WDTCTL = WDT_ADLY_16;
    
    /* Setup the timer b, driven by SMCLK, free running */
    TBCTL = TBCLR | ID_DIV1 | MC_CONT | TBSSEL_SMCLK | CNTL_0;
    TBCTL &= ~TBCLR;
    TBCTL |= MC_CONT;

    /* init control variables */
    DCOTune.i16Error = 32767;
    DCOTune.u8Control = 0;
    iStable = 0;
    
    while (iStable < DCOTUNE_STABLE_STARTUP)
    {
	/* wait for watchdog timeout */
	while ((IFG1 & WDTIFG) == 0);
	
	/* clear watchdog timer flag */
	IFG1 &= ~WDTIFG;
	
	/* grab timerb */
	uiTimerB = TBR;

	/* if we have captured two timers in a row, calculate the period */
	if (DCOTune.u8Control > 0)
	    DCOTune.u16Period = uiTimerB - DCOTune.u16Last;
	DCOTune.u16Last = uiTimerB;
        
	/* If we have two consecutive captures and thus a valid period,
	 * we can perform a DCOTune
	 */
	if (DCOTune.u8Control > 1)
	{	    
	    /* calc error */
	    DCOTune.i16Error = DCOTune.u16Period - (SMCLK * WDTCLKDIV)/ACLK;
	    
	    /* if the error was below our threshold increment the 
	     * stable count, otherwise reset and try again
	     */
	    if (DCOTune.i16Error < DCOTUNE_ERROR_MAX)
		iStable++;
	    else
		iStable = 0;
	    
	    /* perform the feedback part of the control loop */
	    DCOTune.i16NewDCO = DCOCTL - (DCOTune.i16Error/64);
	    DCOTune.u8Rsel = BCSCTL1 & (RSEL0 | RSEL1 | RSEL2);
	    
	    if (DCOTune.i16NewDCO < 0)
	    {
		if (DCOTune.u8Rsel > 0)
		{
		    BCSCTL1--;
		    DCOCTL = 0x60;
		}
	    }
	    else if (DCOTune.i16NewDCO > 255)
	    {
		if (DCOTune.u8Rsel < 7)
		{
		    BCSCTL1++;
		    DCOCTL = 0x60;
		}
	    }
	    else
	    {
		DCOCTL = (uint8_t)(DCOTune.i16NewDCO & 0xFF);
	    }
	}
    
	if (DCOTune.u8Control < 4)
	    DCOTune.u8Control++;    
    }   

    /* invalidate the DCO tune because it could be awhile
     * before the main ucosii time tick hook DCO tune is activated
     */
    DCOTune.u8Control = 0;    
#endif
}

int main (void)
{
    INT8U u8Status;
    INT8U u8Err;

    /* initially disable events, and force update */
    IPCGlobals.u8Flags = IPC_UPDATE | IPC_EVENT_DISABLE;
    
    /* Set Instrument error code to indicate unsynced OSTIME 
     * RESET all other bits      */
    instrumentErrorCode = IEC_OSTIME_UNSYNC | IEC_EVENT_DISABLE;

    /* stop sleeping until we are ready */
    uiSleep |= SLEEP_STARTUP;
    
#ifdef TARGET
    /*************************************************************/
    /* Initialise all hardware and modules                       */
    
    WDTCTL = WDTPW | WDTHOLD;             // stop Watchdog    
        
    /***********************************************/
    /* Clock initialisers                          */

    /* DCO only, no 4Mhz XT2 xtal */
    BCSCTL1 |= XT2OFF;                       
    BCSCTL2 |= DIVM_DIV1 | SELM_DCOCLK;      
    BCSCTL2 |= DIVS_DIV2;                   
	 
    /***********************************************/
    /* Command hardware initialisation             */

    /***********************************************/
    /* Setup all I/O Ports                         */
    
    /* Spares all I/O Output */
    BICB(P1SEL, P1_SPARE1 | P1_SPARE2);
    BISB(P1DIR, P1_SPARE1 | P1_SPARE2);
    
    BICB(P2SEL, P2_SPARE1 | P2_SPARE2 | P2_SPARE3 | P2_SPARE4);
    BISB(P2DIR, P2_SPARE1 | P2_SPARE2 | P2_SPARE3 | P2_SPARE4);

    BICB(P3SEL, P3_SPARE1);
    BISB(P3DIR, P3_SPARE1);
 
    BICB(P4SEL, P4_SPARE1 | P4_SPARE2 | P4_SPARE3 | P4_SPARE4);
    BISB(P4DIR, P4_SPARE1 | P4_SPARE2 | P4_SPARE3 | P4_SPARE4);

    BICB(P5SEL, P5_SPARE1 | P5_SPARE2);
    BISB(P5DIR, P5_SPARE1 | P5_SPARE2);
 
    BICB(P6SEL, P6_SPARE1 | P6_SPARE3 | P6_SPARE4);
    BISB(P6DIR, P6_SPARE1 | P6_SPARE3 | P6_SPARE4);
    
    /* Port 1 */
    BICB(P1SEL, P1_BUTTON1 | P1_BUTTON2 | P1_CAMERA_SLEEP | P1_CAMERA_FOCUS | P1_SERVO_SIGNAL);
    BICB(P1DIR, P1_CAMERA_SLEEP | P1_BUTTON1 | P1_BUTTON2);             // in
    BICB(P1OUT, P1_CAMERA_SHUTTER | P1_CAMERA_FOCUS | P1_SERVO_SIGNAL); // low
    BISB(P1DIR, P1_CAMERA_SHUTTER | P1_CAMERA_FOCUS | P1_SERVO_SIGNAL); // out
        
    /* Port 2 */
    /* RTC Chip done in local initialisation routine */
    relayInit();
    
    /* Port 3 */
    /* don't have to init pins for USART0/1 because that
     * is done in the respective usartInit() function */
    
    /* Port 4 */
    BICB(P4SEL, P4_RS232_STATUS | P4_RTC_SQW | P4_VCAMERA_PGOOD | P4_VSERVO_PGOOD);
    BICB(P4DIR, P4_RS232_STATUS | P4_RTC_SQW | P4_VCAMERA_PGOOD | P4_VSERVO_PGOOD);
    
    /* Port 5 */
    BICB(P5SEL, P5_SW2_1 | P5_SW2_2 | P5_SW2_3 | P5_SW2_4 | P5_LED_GREEN | P5_LED_RED);
    BICB(P5DIR, P5_SW2_1 | P5_SW2_2 | P5_SW2_3 | P5_SW2_4); // make input
    BICB(P5OUT, P5_LED_GREEN | P5_LED_RED);             // low
    BISB(P5DIR, P5_LED_GREEN | P5_LED_RED);             // Make output
        
    /* Port 6 */
    BICB(P6SEL, P6_VCAMERA_ENABLE | P6_VSERVO_ENABLE);  // Digital I/O
    BISB(P6SEL, P6_VINPUT_DIV | P6_VCAMERA_DIV | P6_VSERVO_DIV);        // Analog I/O
    BICB(P6DIR, P6_VINPUT_DIV | P6_VCAMERA_DIV | P6_VSERVO_DIV);        // Make input
    BICB(P6OUT, P6_VCAMERA_ENABLE | P6_VSERVO_ENABLE);  // low
    BISB(P6DIR, P6_VCAMERA_ENABLE | P6_VSERVO_ENABLE);  // Make output
        
#endif /* TARGET */

    usDelayInit();

    /* perform a startup DCO tune */
    startupDCOTune();
    
    /* Init CMDTask <->eventTask globals */
    IPCGlobals.uiPeriod = 60;        // default for PPCC (minute processing)
    IPCGlobals.uiOffset = 5;         // default for PPCC (ensure always gets the right time)
    BISB(IPCGlobals.u8Flags, IPC_UPDATE | IPC_EVENT_DISABLE);
 
    /***************************************************/    
        
#ifdef HOST
    /* Init for HOST usart0 TCP connection */
    
    usart0Socket = 0;
    usart0Connected = 1;
    if ((usart0Socket = socket(AF_INET, SOCK_STREAM, 0)) > 0)
    {
	printf("Socket opened\n");
	
	usart0Address.sin_family = AF_INET;
	usart0Address.sin_port = htons(8000);
	(void)inet_aton("127.0.0.1", &usart0Address.sin_addr);
	
	if ((usart0Connected = connect(usart0Socket, 
			       (struct sockaddr *)&usart0Address, 
			       (socklen_t)sizeof(usart0Address))) == 0)
	{
	    printf("Usart0 Socket Accepted\n");
	    (void)fcntl(usart0Socket, F_SETFL, O_NONBLOCK);
	}
    }
#endif        
   
    OSInit();          /* Initialize uC/OS-II                      */

#ifdef TARGET
    /* initialise the interactive command/Nmea port */
    usart1Init(USART1_BAUD_DIV(CONSOLE_BAUD), 
	       USART1_BAUD_MOD(CONSOLE_BAUD), 
	       USART_8N1);

    /* enabled RX Edge detection for interactive command/Nmea port */
    usart1EnableRXSE();
    
    /* USART0 (sensor bus) initialisation done by read functions */
#endif
    
    /* Create the hardware mutex */
    HardwareMutex = OSMutexCreate(HARDWARE_MUTEX_PRIORITY, &u8Err);

    if ((u8Err != OS_NO_ERR) &&
	(configTable.u8Messages != 0))
	printf("Error creating HardwareMutex, %d\r\n", u8Err);
    
    /* Create the ds1305Mutex */
    RTCMutex = OSMutexCreate(RTC_MUTEX_PRIORITY, &u8Err);

    if ((u8Err != OS_NO_ERR) &&
	(configTable.u8Messages != 0))
	printf("Error creating RTCMutex, %d\r\n", u8Err);
    
    /* Init the servo output */
    servoInit();
    
    /* Initialise the real time clock. */
    if (ds3234PowerUp() != 0)
	instrumentErrorCode |= IEC_RTC_OSCFAULT;
    
    /* Create the startup task */
    TaskUserData[CMD_TASK_ID].TaskCtr = 0;
    
    u8Status = OSTaskCreateExt(CommandTask,
                 (void *)0, 
                 &CmdTaskStk[CMD_TASK_STK_SIZE - 1], 
                 CMD_TASK_PRIORITY,
                 CMD_TASK_ID,                               /* id */
                 &CmdTaskStk[0],                            /* pbos */
                 CMD_TASK_STK_SIZE,                         /* stack_size */
                 &TaskUserData[CMD_TASK_ID],                /* user data */
                 OS_TASK_OPT_STK_CHK + OS_TASK_OPT_STK_CLR  /* options */
                );
    
    /* can start sleeping now */
    uiSleep &= ~SLEEP_STARTUP;
    
    /* Start multitasking */
    OSStart();

    /* should never reach here ... */
    return 0;
}


/*
 * Command Task
 *
 */
void  CommandTask (void *pdata)
{
    INT8U u8Status, u8Err;
    int8_t i8Cmd;
    float fVInput;
    
#if OS_CRITICAL_METHOD == 3
    OS_CPU_SR cpu_sr;
#endif
    pdata  = pdata;                          /* Prevent compiler warning */
    
    OS_ENTER_CRITICAL();
#ifdef TARGET
    /* Using WDT (ACLK) as UCOS ticker */
    WDTCTL = WDT_ADLY_16;      /* Use ACLK and set a 16 msec period */
    IE1 |= WDTIE;              /* Enable Watchdog timer interrupts         */
#endif
    OS_EXIT_CRITICAL();

#ifdef TARGET
    /* check any of the dip switches */
    if (dipSwitch(0))
    {
		if (dipSwitch(1))
		{
	    	// If dip switch 1 on during reset, set servo to closed position
	    	servoPower(SERVO_ON);
	    	servoWidth(configTable.iServoClosed);
		}

		if (dipSwitch(2))
		{
	    	// If dip switch on during reset, turn on camera
	    	controlCamera(CC_STAGE_POWERON);
		}

    	// conduct manufacturing self test
		if (dipSwitch(3))
		{
	    	// Manufacturing test
	   		if (configTable.u8Messages != 0)
	    	{
				printf("\033[0;0H\033[2J");  // Ansi escape sequence to clear screen and home cursor
				printf("Built-In-Self-Test (%s, %s %s)\r\n", FirmwareVersion, __DATE__, __TIME__);	
	    	}
	    
	    	// Start built in self test
	    	builtInSelfTest();
	    
	    	if (configTable.u8Messages != 0)
				printf("BIST end. Pause for 5 minutes\r\n");
	 
	    	// wait for 5 minutes before continuing bootup
	    	OSTimeDly(5 * 60 * OS_TICKS_PER_SEC);
	    
	    	u8LEDRed = LED_OFF;
	    	u8LEDGreen = LED_OFF;
		}
    }
#endif
    
    /* grab hardware Mutex until bootup completed */
    OSMutexPend(HardwareMutex, 0, &u8Err);

    /* Task specific code */    
    if (configTable.u8Messages != 0)
		printf("Penguin Population Camera Controller (%s, %s, %s)\r\n", FirmwareVersion, __DATE__, __TIME__);
        
    /* Create application tasks */
    /* event task */
    TaskUserData[EVENT_TASK_ID].TaskCtr = 0;
    
    u8Status = OSTaskCreateExt(eventTask,
                 (void *)0, 
                 &eventTaskStk[EVENT_TASK_STK_SIZE - 1],      /* ptos */
                 EVENT_TASK_PRIORITY,                        /* priority */
                 EVENT_TASK_ID,                              /* id */
                 eventTaskStk,                               /* pbos */
                 EVENT_TASK_STK_SIZE,                        /* stack_size */
                 &TaskUserData[EVENT_TASK_ID],               /* user data */
                 OS_TASK_OPT_STK_CHK + OS_TASK_OPT_STK_CLR  /* options */
                 );

    /* button task */
    TaskUserData[BUTTON_TASK_ID].TaskCtr = 0;
    
    u8Status = OSTaskCreateExt(ButtonTask,
                 (void *)0, 
                 &ButtonTaskStk[BUTTON_TASK_STK_SIZE - 1],  /* ptos */
                 BUTTON_TASK_PRIORITY,                      /* priority */
                 BUTTON_TASK_ID,                            /* id */
                 ButtonTaskStk,                             /* pbos */
                 BUTTON_TASK_STK_SIZE,                      /* stack_size */
                 &TaskUserData[BUTTON_TASK_ID],             /* user data */
                 OS_TASK_OPT_STK_CHK + OS_TASK_OPT_STK_CLR  /* options */
                 );

    /* enable events */
    BICB(IPCGlobals.u8Flags, IPC_EVENT_DISABLE);
    instrumentErrorCode &= ~IEC_EVENT_DISABLE;
        
    /* Init the LED TurnOffTimer to 10 minutes */
    uiLEDOffTimer = LED_OFF_TIME;
    
    /* Display RTC */
    if ((instrumentErrorCode & IEC_RTC_OSCFAULT) != IEC_RTC_OSCFAULT)
    {
		/* display time */
		if (configTable.u8Messages != 0)
		{
		    printf("Time is now ");
		    printClock();
		}
	
		/* Synchronise OSTime to the RTC */
		if (syncOSToRTC(OSTIME_SYNC_LIMIT, OSTIME_SYNC_RETRIES))
		    instrumentErrorCode &= ~IEC_OSTIME_UNSYNC;
		else
		{
		    if (configTable.u8Messages != 0)
			printf("Unable to synchronise OSTime to RTC\r\n");
		}
    }
    else
	{
		/* Clock fault */
		if (configTable.u8Messages != 0)
		{
		    printf("**ERROR** : Real time clock is invalid. Please set!\r\n");
		    printf("**ERROR** : OSTime not synchronised to RTC - eventTab disabled\r\n");
		}
    }
    
    if (configTable.u8Messages != 0)
    {
		/* Check eventTable */
		if (eventTabEmpty())
		{
		    instrumentErrorCode |= IEC_EVENTTAB_NOENTRIES;
		    printf("WARNING! No events have been defined! Use the 'event a' command.\r\n");
		}
	
		/* Check VINPUT */
		fVInput = readVInput();
	
		/* set IEC code if VInput too low for servo & camera*/
		if (fVInput <= configTable.fVInputCamera)
		{
		    printf("WARNING! Input voltage too low to operate camera\r\n");
		    instrumentErrorCode |= IEC_VINPUT_FAULT;
		}
		if (fVInput <= configTable.fVInputServo)
		{
		    printf("WARNING! Input voltage too low to operate servo motor\r\n");
		    instrumentErrorCode |= IEC_VINPUT_FAULT;
		}
	
		printf("Type 'help' for a list of available commands\r\n");
    }

	printf(CMD_PROMPT);	
    
    /* release HardwareMutex */
    OSMutexPost(HardwareMutex);    
    
    for(;;)
    {
        i8Cmd = Cmd();

        if (i8Cmd >= 0)
        {
			/* print command line ready prompt for next command */
		    printf(CMD_PROMPT);
        }

		if (i8Cmd == CMDERR_NOTFOUND)
		{
	    	instrumentErrorCode |= IEC_UNKNOWN_COMMAND;
	    	printf("\r\nUnknown command '%s', try 'help'\r\n", Line);
			printf(CMD_PROMPT);
		}
    }
}
