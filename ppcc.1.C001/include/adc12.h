/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by kym.newbery@aad.gov.au
 *
 * Interface definition for the include/adc12.c file.
 */

#ifndef INCLUDE_ADC12_H
#define INCLUDE_ADC12_H

/* Clock divider selectors */
#define ADC12_CLKDIV_1    0
#define ADC12_CLKDIV_2    1
#define ADC12_CLKDIV_3    2
#define ADC12_CLKDIV_4    3
#define ADC12_CLKDIV_5    4
#define ADC12_CLKDIV_6    5
#define ADC12_CLKDIV_7    6
#define ADC12_CLKDIV_8    7

/* Sample and hold time selectors */
#define SAMPLE_HOLD_4     0
#define SAMPLE_HOLD_8     1
#define SAMPLE_HOLD_16    2
#define SAMPLE_HOLD_32    3
#define SAMPLE_HOLD_64    4
#define SAMPLE_HOLD_96    5
#define SAMPLE_HOLD_128   6
#define SAMPLE_HOLD_192   7
#define SAMPLE_HOLD_256   8
#define SAMPLE_HOLD_384   9
#define SAMPLE_HOLD_512   10
#define SAMPLE_HOLD_768   11
#define SAMPLE_HOLD_1024  12

/* Reference voltage selectors */
#define REF_VOLTAGE_1V5   0   /* Internal stabilised reference */
#define REF_VOLTAGE_2V5   1   /* Internal stabilised reference */
#define REF_VOLTAGE_VCC   2   /* not stabilised .. external input */
#define REF_VOLTAGE_VEREF 3   /* Externally input reference */

/* Common channel selectors */
#define ADC12_VEREF       8
#define ADC12_VREF_NEG    9
#define ADC12_TEMP_DIODE  10
#define ADC12_HALF_VCC    11

void adc12Init(void);
void adc12PowerUp(int iADC12ClkDiv, int iSampleHoldTime, int iRefVoltage);
unsigned int adc12Read(int iChannel);
unsigned int adc12ReadAverage(int iChannel, int iCount);
void adc12Shutdown(void);

#endif /* INCLUDE_ADC12_H */
