/*
*********************************************************************************************************
*                                                uC/OS-II
*                                          The Real-Time Kernel
*
*                              (c) Copyright 2002, Micrium, Inc., Weston, FL
*                                           All Rights Reserved
*
*                                               TI MSP430 
*
*
* File         : OS_CPU.H
* By           : Jian Chen (yenger@hotmail.com)
*                Jean J. Labrosse
*********************************************************************************************************
*/

#ifndef _GNU_ASSEMBLER_
#ifdef HOST
#include <unistd.h>
#include <signal.h>
#include <string.h>
#endif
#endif

#ifdef  OS_CPU_GLOBALS
#define OS_CPU_EXT
#else
#define OS_CPU_EXT  extern
#endif

/*
*********************************************************************************************************
*                                              DATA TYPES
*                                         (Compiler Specific)
*********************************************************************************************************
*/
#ifndef _GNU_ASSEMBLER_
typedef unsigned char  BOOLEAN;
typedef unsigned char  INT8U;                    /* Unsigned  8 bit quantity                           */
typedef signed   char  INT8S;                    /* Signed    8 bit quantity                           */
typedef unsigned int   INT16U;                   /* Unsigned 16 bit quantity                           */
typedef signed   int   INT16S;                   /* Signed   16 bit quantity                           */
typedef unsigned long  INT32U;                   /* Unsigned 32 bit quantity                           */
typedef signed   long  INT32S;                   /* Signed   32 bit quantity                           */
typedef float          FP32;                     /* Single precision floating point                    */
typedef double         FP64;                     /* Double precision floating point                    */

typedef unsigned int   OS_STK;                   /* Each stack entry is 16-bit wide                    */

#ifdef TARGET
typedef unsigned int   OS_CPU_SR;                /* Define size of CPU status register (SR = 16 bits)  */
#endif
#ifdef HOST
//typedef sigset_t       OS_CPU_SR;
#endif

#endif /* GNU_ASSEMBLER */

#ifdef TARGET

/* 
*********************************************************************************************************
*                                             TI MSP430
*
* Method #1:  Disable/Enable interrupts using simple instructions.  After critical section, interrupts
*             will be enabled even if they were disabled before entering the critical section.
*
* Method #2:  Disable/Enable interrupts by preserving the state of interrupts.  In other words, if 
*             interrupts were disabled before entering the critical section, they will be disabled when
*             leaving the critical section.
*             NOT IMPLEMENTED!
*
* Method #3:  Disable/Enable interrupts by preserving the state of interrupts.  Generally speaking you
*             would store the state of the interrupt disable flag in the local variable 'cpu_sr' and then
*             disable interrupts.  'cpu_sr' is allocated in all of uC/OS-II's functions that need to 
*             disable interrupts.  You would restore the interrupt disable state by copying back 'cpu_sr'
*             into the CPU's status register.
*********************************************************************************************************
*/
#define  OS_CRITICAL_METHOD    1

/* A NOP is need as an interrupt can happen in the instruction after the DINT */

#if      OS_CRITICAL_METHOD == 1
#define  OS_ENTER_CRITICAL()		_DINT();asm("nop"); /* Disable interrupts */
#define  OS_EXIT_CRITICAL()		_EINT();            /* Enable  interrupts */
#endif

#if      OS_CRITICAL_METHOD == 2
#define  OS_ENTER_CRITICAL()                              /* Disable interrupts */
#define  OS_EXIT_CRITICAL()                               /* Enable  interrupts */
#endif

#if      OS_CRITICAL_METHOD == 3
#define  OS_ENTER_CRITICAL()  _DINT();asm("nop");cpu_sr = READ_SR;  /* Disable interrupts */
#define  OS_EXIT_CRITICAL()   _EINT();WRITE_SR(cpu_sr);             /* Enable  interrupts */
#endif

/*
*********************************************************************************************************
*                                             MSP430
*********************************************************************************************************
*/

#define  OS_STK_GROWTH        1                       /* Stack grows from HIGH to LOW memory on MSP430 */

#define  OS_TASK_SW()         OSCtxSw()				  /* Task level context switch routine */

/*
*********************************************************************************************************
*                                         GLOBAL VARIABLES
*********************************************************************************************************
*/
#ifndef _GNU_ASSEMBLER_
OS_CPU_EXT  OS_STK  *OSISRStkPtr;                    /* Pointer to top-of ISR stack                    */

/*
*********************************************************************************************************
*                                           PROTOTYPES
*********************************************************************************************************
*/

OS_CPU_SR  OSCPUSaveSR(void);
void       OSCPURestoreSR(OS_CPU_SR cpu_sr);
#endif
#endif /* TARGET */

#ifdef HOST
/** used in os_cpu_c */
void OSTaskSwHook (void);

/** linuxInit does the setup of the signal handler. */
void linuxInit(void); 

/** linuxInitInt starts periodic interrupts. */
void linuxInitInt(void);

/** Macro to block interrupts (on Linux: signals)
 *
 * Critical method 1 which does not restore the signal state may lead to hanging
 * timer interrupts, especially when debugging (i.e. real time is much faster than debug
 * time).
 */
#ifdef TEST
#define OS_CRITICAL_METHOD 3
#else
#define OS_CRITICAL_METHOD 1
#endif

/** We add all the virtual interrupt signals to the mask and save the old process
 * signal mask to the backup 'status register'.
 */
#ifdef TEST
#define OS_ENTER_CRITICAL() { 	sigset_t set; \
				sigemptyset(&set); \
				sigaddset(&set, SIGALRM); \
				sigaddset(&set, SIGIO); \
				sigprocmask(SIG_SETMASK, &set, &cpu_sr); \
				}
#else
#define OS_ENTER_CRITICAL()
#endif
/** Macro to unblock interrupts
 *
 * Here we just restore the state that was returned by previous call to sigprocmask
 */
#ifdef TEST
#define OS_EXIT_CRITICAL() {	sigprocmask(SIG_SETMASK, &cpu_sr, NULL); \
				} 
#else
#define OS_EXIT_CRITICAL()
#endif
/** 
 * Stack grows from HIGH to LOW memory on linux x86
 */
#define  OS_STK_GROWTH      1

/**
 * This macro posts a Linux signal to ourselves; it is returned in the handler with the
 * threads context. The SIGUSR1 handler saves the context and then calls OSCtxSw().
 *
 * (use OSCtxSw() directly when no sw int available)
 */
#define OS_TASK_SW() { kill(getpid(), SIGUSR1); } 

#endif /* HOST */
