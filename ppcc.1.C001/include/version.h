/*
 *	msp430 - The msp430 program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *	Written by Peter Jansen <peter_jan@sts-cvs>
 *
 * Interface definition for the lib/ver/version.c file.
 */

#ifndef INCLUDE_VERSION_H
#define INCLUDE_VERSION_H

extern const char FirmwareVersion[];
extern const char FirmwareDate[];

#endif /* INCLUDE_VERSION_H */
