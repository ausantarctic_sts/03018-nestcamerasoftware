/*
 *	EFM - The "EFM" program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *	Written by peter.jansen@aad.gov.au
 *
 * Interface definition for the include/Flash.c file.
 */

#ifndef INCLUDE_FLASH_H
#define INCLUDE_FLASH_H

#include <stdint.h>

#ifdef HOST
#define ERASE 0x0002
#define FWKEY 0x1234
#define LOCK  0x1230
#endif

extern void flashErase(unsigned char mode, unsigned char *addr);
extern void flashUpdate(unsigned char *addr, size_t len, unsigned char *data);
extern void flashUnlock(uint16_t key);
extern int8_t flashLocked(void);
extern void flashEraseFromLen(unsigned char *start, size_t len);

#endif /* INCLUDE_FLASH_H */
