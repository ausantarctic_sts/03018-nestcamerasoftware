/*
 *	EFM - The "EFM" program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *	Written by peter.jansen@aad.gov.au
 *
 * Interface definition for the include/intel.c file.
 */

#ifndef INCLUDE_INTEL_H
#define INCLUDE_INTEL_H

void Download(void);

#endif /* INCLUDE_INTEL_H */
