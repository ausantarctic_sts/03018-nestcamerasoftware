/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *
 * Interface definition for the include/eventtab.c file.
 */

#ifndef INCLUDE_EVENTTAB_H
#define INCLUDE_EVENTTAB_H

#include <stdint.h>

/*
 * Note ! EVENTTAB_MAX_ENTRY is the number of entries in the
 * eventtab section. This number x sizeof(eventTabEntryStruct) must
 * be less than or equal to the space allocate to the eventTab
 * section in the msp430x14x.ld file
 * 
 */
#define EVENTTAB_MAX_ENTRY      500
#define eventTabSection __attribute((section(".eventTab")))

#define EVENTTAB_CHAR_WC        '*'
#define EVENTTAB_BCD_WC         0xA
#define EVENTTAB_ENTRY_DELETED  0x00000000
#define EVENTTAB_ENTRY_ERASED   0xFFFFFFFF

/*************************************/
/* The Event Table                   */

#define EVENTTAB_CMDLINE_LEN    12

typedef struct 
{
    union {
	struct RTCBCD {
	    uint8_t u8Month;
	    uint8_t u8DayOfMonth;
	    uint8_t u8Hour;
	    uint8_t u8Minute;
	} BCD;
	uint32_t u32Packed;
    };
    char strCmdLine[EVENTTAB_CMDLINE_LEN];
} eventTabEntryStruct;

extern eventTabSection eventTabEntryStruct eventTable[]; 

/***************************************/

void eventTabErase(void);  
int16_t eventTabAdd(eventTabEntryStruct *Entry);
int eventTabEmpty(void);
void eventTabDel(uint16_t u16Index);
int16_t eventTabFind(eventTabEntryStruct *Entry, int iStartIndex);
uint16_t eventTabCount(void);
uint8_t strToBCDwc(char *p, const char wc, uint8_t u8wc, uint8_t *u8Mask);
void printBCDwc(uint8_t u8BCD, const char wc, uint8_t u8wc);
uint8_t eventTabCompareEntry(eventTabEntryStruct *Entry1, 
			    eventTabEntryStruct *Entry2,
			    uint8_t u8wc);

#endif /* INCLUDE EVENTTAB_H */
