/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by peter.jansen@aad.gov.au
 *
 * Interface definition for the include/string files
 */

#ifndef INCLUDE_STR_H
#define INCLUDE_STR_H

#include <stdlib.h>
#include <stdint.h>

extern int stoi(char *p, uint8_t base);
extern float stof(char *p, uint8_t base);
extern char strtokcp(char *str, const char *delim, char *cmd, size_t len);
extern unsigned char IsXDigit(char c);
extern unsigned char IsFDigit(char c);

#ifdef TARGET
extern int strspn(char *, char *);
extern int strcspn(char *, char *);
#endif

#endif /* INCLUDE_STR_H */
