/*
 *	Copyright (C) 2004 Australian Antarctic Division
 *
 * Interface definition for the include/ds1305.c file.
 * 
 * Driver for Dallas DS1305 SPI RTC
 */

#ifndef INCLUDE_DS1305_H
#define INCLUDE_DS1305_H

#include <stdint.h>

/* Clock register definitions */
#define DS1305_SECOND      0x00  // BCD, 00 - 59
#define DS1305_MINUTE      0x01  // BCD, 00 - 59
#define DS1305_HOUR        0x02  // BCD, 00 - 23 or 00 - 12 (AM/PM)
#define DS1305_HOUR_12     0x40  // Bit 6 controls 12/24 Hour mode
#define DS1305_HOUR_PM     0x20  // Bit 5 controls AM/PM indication
#define DS1305_DAYOFWEEK   0x03  // BCD, 01 - 07
#define DS1305_DAYOFMONTH  0x04  // BCD, 01 - 31
#define DS1305_MONTH       0x05  // BCD, 01 - 12
#define DS1305_YEAR        0x06  // BCD, 00 - 99

/* Might put alarm defs in if we need them */

/* Control register definitions */ 
#define DS1305_CONTROL     0x0F
#define DS1305_STATUS      0x10
#define DS1305_TRICKLE     0x11
#define DS1305_RAM_START   0x20
#define DS1305_RAM_END     0x7F

/* Control Register Bits */
#define DS1305_CTL_EOSC    0x80
#define DS1305_CTL_WP      0x40
#define DS1305_CTL_INTCN   0x04
#define DS1305_CTL_AIE0    0x02
#define DS1305_CTL_AIE1    0x01

/* Status Register Bits */
#define DS1305_STA_IRQF0   0x01
#define DS1305_STA_IRQF1   0x02

/* RTC Registers structure */
typedef struct {
    uint8_t u8Second;
    uint8_t u8Minute;
    uint8_t u8Hour;
    uint8_t u8DayOfMonth;
    uint8_t u8Month;
    uint8_t u8Year;
} RTCTimeStruct;

/* Convert 2 x 4 bit BCD to 8 bit Binary */
extern uint8_t BCD2BIN(uint8_t u8BCD);

/* Convert 8 bit Binary to 2 x 4 bit BCD */
extern uint8_t BIN2BCD(uint8_t u8BIN);

/* Initialises and configures USART0 for SPI connection
 * to DS1305 & removes internal write protect and enables
 * internal oscillator if they have been turned off.
 */
extern uint8_t ds1305PowerUp(void);

/* Write a byte to a DS1305 RTC register */
extern void    ds1305Write(uint8_t u8Address, uint8_t u8Data);

/* Read a byte from a DS1305 RTC register */
extern uint8_t ds1305Read(uint8_t u8Address);

/* Read the YY/MM/DD HH:MM:SS from DS1305 to RTCTime Struct as BCD */
extern void    ds1305BCDReadRTC(RTCTimeStruct *RTCTime); 

/* Write the YY/MM/DD HH:MM:SS from RTCTime to the DS1305 as BCD */
extern void    ds1305BCDWriteRTC(RTCTimeStruct *RTCTime);

/* Read various sizes from NVRAM */
extern uint8_t  NVRAMu8Read(uint8_t u8Addr);
extern uint16_t NVRAMu16Read(uint8_t u8Addr);
extern uint32_t NVRAMu32Read(uint8_t u8Addr);

/* Write various sizes to NVRAM */
extern void NVRAMu8Write(uint8_t u8Addr, uint8_t u8Data);
extern void NVRAMu16Write(uint8_t u8Addr, uint16_t u16Data);
extern void NVRAMu32Write(uint8_t u8Addr, uint32_t u32Data);

#endif /* INCLUDE_DS1305_H */
