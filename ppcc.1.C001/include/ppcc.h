/*
 * Interface definition for the include/ppcc.c file.
 * 
 * 2010/07/26    KBN     Updated for Rev 7 hardware
 * 2010/09/17    KBN     Fixed Vservo BIST fault during manufacture
 * 
 */

#ifndef INCLUDE_PPCC_H
#define INCLUDE_PPCC_H

#include <stdio.h>
#include <stdint.h>

/**************************************/
/* Clock Frequencies                  */

#define LFXT1CLK                 (32768)
// #define XT2CLK                   (4000000)  // XT2CLK not used
#define DCOCLK                   (2000000)   /* Nominal */

#define ACLKDIV                  1
#define MCLKDIV                  1
#define SMCLKDIV                 2
#define WDTCLKDIV                512

/* Clock sources                      */

#define ACLK                     (LFXT1CLK/ACLKDIV)                    
#define MCLK                     (DCOCLK/MCLKDIV)
#define SMCLK                    (DCOCLK/SMCLKDIV)

/**************************************/
/* Comms baud rate for                */
/* CONSOLE/PORT1                      */
#define CONSOLE_BAUD               (9600)

/**************************************/
/* Instrument Error Codes             */

extern unsigned int instrumentErrorCode;

/* Critical IEC's */
#define IEC_EVENTTAB_NOENTRIES   0x8000     /* eventTab is empty! */
#define IEC_SERVO_FAULT          0x4000     /* Servo fault */
#define IEC_RTC_OSCFAULT         0x2000     /* RTC OSC Fault - clock not set */
#define IEC_CAMERA_INACTIVE      0x1000     /* Camera seems inactive */
#define IEC_VINPUT_FAULT         0x0800     /* VInput Fault (low) */
#define IEC_EVENT_DISABLE        0x0400     /* events have been disabled */    
#define IEC_SPARE3               0x0200     

/* Set IEC_CRITIAL to be the lowest value in the critical code list */
#define IEC_CRITICAL             IEC_SPARE3

/* Non-critical IEC's */
#define IEC_OSTIME_UNSYNC        0x0100     /* OSTime not synced to RTC */
#define IEC_WRONG_PARAMETER_NO   0x0080     /* Wrong # Parameters */
#define IEC_UNKNOWN_COMMAND      0x0040     /* unrecognised command */
#define IEC_DCO_BIG_ERROR        0x0020     /* DCO ERROR */
#define IEC_SPARE2               0x0010     /* SPARE */
#define IEC_SPARE1               0x0008     /* SPARE */
#define IEC_USART1_ERROR         0x0004     /* USART1 - FE,PE,OE,BRK */
#define IEC_USART0_ERROR         0x0002     /* USART0 - FE,PE,OE,BRK */
#define IEC_RESET                0x0001     /* Reset */

/***************************************/
/* Define Port Pins on the PPCC        */
#define P1_BUTTON1             0x01    
#define P1_BUTTON2             0x02    
#define P1_CAMERA_SHUTTER      0x04    
#define P1_CAMERA_FOCUS        0x08    
#define P1_CAMERA_SLEEP        0x10    
#define P1_SPARE1              0x20    
#define P1_SERVO_SIGNAL        0x40
#define P1_SPARE2              0x80

#define P2_SPARE1              0x01
#define P2_RELAY_STATE         0x02
#define P2_RELAY_SET           0x04
#define P2_RELAY_RESET         0x08
#define P2_SPARE2              0x10
#define P2_SPARE3              0x20
#define P2_SPARE4              0x40
#define P2_RTC_CS              0x80

#define P3_SPARE1              0x01
#define P3_MOSI                0x02
#define P3_MISO                0x04
#define P3_SCLK                0x08
#define P3_P0_TXD              0x10
#define P3_P0_RXD              0x20
#define P3_P1_TXD              0x40
#define P3_P1_RXD              0x80

#define P4_RS232_STATUS        0x01
#define P4_SPARE1              0x02
#define P4_SPARE2              0x04
#define P4_SPARE3              0x08
#define P4_SPARE4              0x10
#define P4_VCAMERA_PGOOD       0x20
#define P4_VSERVO_PGOOD        0x40
#define P4_RTC_SQW             0x80

#define P5_SPARE1              0x01
#define P5_SW2_4               0x02
#define P5_SW2_3               0x04
#define P5_SW2_2               0x08
#define P5_SW2_1               0x10
#define P5_LED_GREEN           0x20
#define P5_LED_RED             0x40
#define P5_SPARE2              0x80

#define P5_SW2_ALL (P5_SW2_1 | P5_SW2_2 | P5_SW2_3 | P5_SW2_4)

#define P6_VINPUT_DIV          0x01
#define P6_VCAMERA_DIV         0x02
#define P6_VSERVO_DIV          0x04
#define P6_VCAMERA_ENABLE      0x08
#define P6_VSERVO_ENABLE       0x10
#define P6_SPARE1              0x20
#define P6_SPARE3              0x40
#define P6_SPARE4              0x80

/************************************/
/* Analog Input Channels for PPCC   */
#define ADC12_PPCC_VINPUT       0   /* A0 */
#define ADC12_PPCC_VCAMERA      1   /* A1 */
#define ADC12_PPCC_VSERVO       2   /* A2 */

/*******************************************/
/* Project wide power & clock management   */

#define DCOTUNE_RETUNE_PERIOD  5    /* seconds between forced DCO Tunes */
#define DCOTUNE_ERROR_MAX      64   /* Maximum stable startup error limit */
#define DCOTUNE_STABLE_STARTUP 10   /* Number of stable start errors required */
#define DCOTUNE_BIG_ERROR_LIMIT 15000  /* ignore large error tunes greater than this */ 

struct DCOTuneStruct
{
    volatile uint8_t  u8Control; /* Controls DCO Tune process */
    volatile uint16_t u16Ticks;  /* Number of times DCO Tune performed */
    volatile uint16_t u16Last;   /* The last value of timer B */ 
    volatile uint16_t u16Period; /* Period between consecutive TimerB reads */
    volatile int16_t  i16Error;  /* Error between actual and desired OSTickPeriod */
    volatile int16_t  i16NewDCO; /* new value to write into DCO control */
    volatile uint8_t  u8Rsel;    /* Rsel value */
};

extern struct DCOTuneStruct DCOTune;

/************************************************************/
/* Global timeout counters used for camera powerdown timeout
 * and led turn off (after bootup)
 */
extern volatile unsigned int uiCameraOffTimer;

#define LED_OFF_TIME              10         /* minutes */
extern volatile unsigned int uiLEDOffTimer;

/********************************************/
/* Sleep Control */
extern volatile unsigned int uiSleep;

/* Flags for the uiSleep variable */
#define SLEEP_FORCE_DCOTUNE       0x0001
#define SLEEP_COMMAND_ACTIVE      0x0002
#define SLEEP_EVENTTASK_ACTIVE    0x0004
#define SLEEP_STARTUP             0x0020
#define SLEEP_CHAR_RX1            0x0040   // USART1 RX Char sleep control
#define SLEEP_CHAR_RX0            0x0080   // USART0 RX Char sleep control
#define SLEEP_SERVO_ACTIVE        0x0100
#define SLEEP_MANUAL_CONTROL      0x8000

/**********************************************/
/* Synchronisation limit between OSTIME & RTC */
#define OSTIME_SYNC_LIMIT        2        // Seconds
#define OSTIME_SYNC_RETRIES      3        // No. of retries

/*******************************************/
/* DS1305 NVRAM Allocation                 */

// Last event date/time
#define NVRAM_LASTEVENT_DATETIME 0      // 32 bit int
// Index into eventTab for last found entry
#define NVRAM_LASTEVENT_INDEX    4      // 16 bit int
// Count of events
#define NVRAM_EVENT_COUNT        6      // 16 bit int

/*******************************************/

#ifdef TARGET
#define BICB(location, bits) __asm__ __volatile__ (" bic.b %0, %1" : : "i" (bits) , "m" (location))
#define BICW(location, bits) __asm__ __volatile__ (" bic.w %0, %1" : : "i" (bits) , "m" (location))
#define BISB(location, bits) __asm__ __volatile__ (" bis.b %0, %1" : : "i" (bits) , "m" (location))
#define BISW(location, bits) __asm__ __volatile__ (" bis.w %0, %1" : : "i" (bits) , "m" (location))
#endif
#ifdef HOST
#define BICB(location, bits)
#define BICW(location, bits)
#define BISB(location, bits)
#define BISW(location, bits)
#endif


#endif /* INCLUDE_PPCC_H */
