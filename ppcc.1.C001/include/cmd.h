/*
 *      msp430 - The msp430 program.
 *      Copyright (C) 2003 Australian Antarctic Division
 *      Written by Peter Jansen <peter_jan@sts-cvs>
 *
 * Interface definition for the include/cmd.c file.
 */

#ifndef INCLUDE_CMD_H
#define INCLUDE_CMD_H

#include <stdlib.h>

/* Command Parser Error Types */

#define CMDERR_NOCMD     -1      // no command yet
#define CMDERR_NOTFOUND  -2      // command not found
#define CMDERR_PARAMETER -3      // wrong number of parameters
#define CMDERR_SYNTAX    -4      // syntax error
#define CMDERR_CHECKSUM  -5      // chechsum Error
#define CMDERR_IDERROR   -6      // Master or Slave ID did not match
#define CMDERR_NOQUERY   -7      // No Query Character found
#define CMDERR_NOCOMMA   -8      // No comma following the Query Character
#define CMDERR_TOOSHORT  -9      // Command line too short

#define CMD_COMMENT      -99
#define CMD_COMMENT_CHAR  '#'
#define CMD_PROMPT        "ready> "

typedef struct CmdTableStruct
{
    const char      Command[10];
    void            (*const Function)(char *CmdLine);
    const char      HelpMsg[40];
} CmdTable;

extern char Line[];

extern const CmdTable CmdTab[];
extern int8_t FindCommand(char *);

int8_t Cmd(void);

char *getNextParameter();
char *getFirstParameter(register char *p);

#endif /* INCLUDE_CMD_H */
