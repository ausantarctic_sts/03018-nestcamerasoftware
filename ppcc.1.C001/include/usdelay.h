/*
 *	AWS2003 - The "AWS2003" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by kym.newbery@aad.gov.au
 *
 * Interface definition for the include/usdelay.c file.
 */

#ifndef INCLUDE_USDELAY_H
#define INCLUDE_USDELAY_H

extern void usDelayInit(void);
extern void usDelay(unsigned int uiDelay);

#endif /* INCLUDE_USDELAY_H */
