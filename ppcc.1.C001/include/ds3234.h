/*
 *	Copyright (C) 2004 Australian Antarctic Division
 *
 * Interface definition for the include/ds1305.c file.
 * 
 * Driver for Dallas DS3234 SPI RTC
 */

#ifndef INCLUDE_DS3234_H
#define INCLUDE_DS3234_H

#include <stdint.h>

/* Registers */
#define DS3234_SECONDS     0x00
#define DS3234_MINUTES     0x01
#define DS3234_HOURS       0x02
#define DS3234_DAYOFWEEK   0x03
#define DS3234_DAYOFMONTH  0x04
#define DS3234_CENTMON     0x05
#define DS3234_YEAR        0x06
#define DS3234_A1SECS      0x07
#define DS3234_A1MINS      0x08
#define DS3234_A1HOURS     0x09
#define DS3234_A1DAYDATE   0x0A
#define DS3234_A2MINS      0x0B
#define DS3234_A2HOURS     0x0C
#define DS3234_A2DAYDATE   0x0D
#define DS3234_CTL         0x0E
#define DS3234_STA         0x0F
#define DS3234_AGEOFFSET   0x10
#define DS3234_TEMP_MSB    0x11
#define DS3234_TEMP_LSB    0x12
#define DS3234_TEMP_CTL    0x13
#define DS3234_SRAM_ADDR   0x18
#define DS3234_SRAM_DATA   0x19

#define DS3234_SRAM_MAX    0xFF // Limit of SRAM

/* Time register bits */
#define DS3234_HOUR_12     0x40

/* Control Register Bits */
#define DS3234_CTL_EOSC    0x80
#define DS3234_CTL_BBSQW   0x40
#define DS3234_CTL_CONV    0x20
#define DS3234_CTL_RS2     0x10
#define DS3234_CTL_RS1     0x08
#define DS3234_CTL_INTCN   0x04
#define DS3234_CTL_A2IE    0x02
#define DS3234_CTL_A1IE    0x01

/* Status Register Bits */
#define DS3234_STA_OSF     0x80
#define DS3234_STA_BB32KHZ 0x40
#define DS3234_STA_CRATE1  0x20
#define DS3234_STA_CRATE0  0x10
#define DS3234_STA_EN32KHZ 0x08
#define DS3234_STA_BSY     0x04
#define DS3234_STA_A2F     0x02
#define DS3234_STA_A1F     0x01

/* RTC Registers structure */
typedef struct {
    uint8_t u8Second;
    uint8_t u8Minute;
    uint8_t u8Hour;
    uint8_t u8DayOfMonth;
    uint8_t u8Month;
    uint8_t u8Year;
} RTCTimeStruct;

/* Convert 2 x 4 bit BCD to 8 bit Binary */
extern uint8_t BCD2BIN(uint8_t u8BCD);

/* Convert 8 bit Binary to 2 x 4 bit BCD */
extern uint8_t BIN2BCD(uint8_t u8BIN);

/* Initialises and configures USART0 for SPI connection
 * to DS3234 & removes internal write protect and enables
 * internal oscillator if they have been turned off.
 */
extern uint8_t ds3234PowerUp(void);

/* Write a byte to a DS3234 RTC register */
extern void    ds3234Write(uint8_t u8Address, uint8_t u8Data);

/* Read a byte from a DS3234 RTC register */
extern uint8_t ds3234Read(uint8_t u8Address);

/* Read the YY/MM/DD HH:MM:SS from DS3234 to RTCTime Struct as BCD */
extern void    ds3234BCDReadRTC(RTCTimeStruct *RTCTime); 

/* Write the YY/MM/DD HH:MM:SS from RTCTime to the DS3234 as BCD */
extern void    ds3234BCDWriteRTC(RTCTimeStruct *RTCTime);

/* Read temperature, age and conversion rate of clock chip */
extern float   ds3234ReadTemperature(int iConvert, 
				     int8_t *i8AgeOffset, 
				     int *iRate);

/* Read various sizes from NVRAM */
extern uint8_t  NVRAMu8Read(uint8_t u8Addr);
extern uint16_t NVRAMu16Read(uint8_t u8Addr);
extern uint32_t NVRAMu32Read(uint8_t u8Addr);

/* Write various sizes to NVRAM */
extern void NVRAMu8Write(uint8_t u8Addr, uint8_t u8Data);
extern void NVRAMu16Write(uint8_t u8Addr, uint16_t u16Data);
extern void NVRAMu32Write(uint8_t u8Addr, uint32_t u32Data);

#endif /* INCLUDE_DS3234_H */
