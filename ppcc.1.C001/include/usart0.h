/*
 *	msp430 - The msp430 program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *	Written by Peter Jansen <peter_jan@sts-cvs>
 *
 * Interface definition for the include/usart0.c file.
 */

#ifndef INCLUDE_USART0_H
#define INCLUDE_USART0_H

#include <stdint.h>

#if TARGET

// code is optimized for power of 2 buffer sizes (16, 32, 64, 128, ...)
// NOTE: the buffers are only used if the respective interrupt mode is
// enabled

#define USART0_RX_BUFFER_SIZE 64        // usart0 receive buffer size
#define USART0_TX_BUFFER_SIZE 64       // usart0 transmit buffer size

// BRCLKn may be driven by UCLKn, ACLK, or SMCLK

#define USART0_UCLK       0.0           // External Clock frequency (if used)

// uncomment ONE of the following pairs for USART0
//#define USART0_BRCLK      UCLK0
//#define USART0_BRSEL      (0)

//#define USART0_BRCLK      ACLK
//#define USART0_BRSEL      (SSEL0)

#define USART0_BRCLK      SMCLK
#define USART0_BRSEL      (SSEL1)

// use the following macros to determine the 'baudDiv' parameter values
// for usartInit0() and usartInit1()
// CAUTION - 'baud' SHOULD ALWAYS BE A CONSTANT or
// a lot of code will be generated.

#define USART0_BAUD_DIV(baud) (uint16_t)(USART0_BRCLK / (baud))

// the following are used to calculate the UMOD values at compile time
#define CMOD_0(baud)    ((USART0_BRCLK / (baud)) - USART0_BAUD_DIV(baud))
#define M0_0(baud)      (CMOD_0(baud) + CMOD_0(baud))
#define M0_1(baud)      (CMOD_1(baud) + CMOD_1(baud))
#define M1_0(baud)      (M0_0(baud) + CMOD_0(baud))
#define M1_1(baud)      (M0_1(baud) + CMOD_1(baud))
#define M2_0(baud)      (M1_0(baud) + CMOD_0(baud))
#define M2_1(baud)      (M1_1(baud) + CMOD_1(baud))
#define M3_0(baud)      (M2_0(baud) + CMOD_0(baud))
#define M3_1(baud)      (M2_1(baud) + CMOD_1(baud))
#define M4_0(baud)      (M3_0(baud) + CMOD_0(baud))
#define M4_1(baud)      (M3_1(baud) + CMOD_1(baud))
#define M5_0(baud)      (M4_0(baud) + CMOD_0(baud))
#define M5_1(baud)      (M4_1(baud) + CMOD_1(baud))
#define M6_0(baud)      (M5_0(baud) + CMOD_0(baud))
#define M6_1(baud)      (M5_1(baud) + CMOD_1(baud))
#define M7_0(baud)      (M6_0(baud) + CMOD_0(baud))
#define M7_1(baud)      (M6_1(baud) + CMOD_1(baud))
#define C0_0(baud)      (uint8_t)((int)M0_0(baud) ? BV(0) : 0)
#define C0_1(baud)      (uint8_t)((int)M0_1(baud) ? BV(0) : 0)
#define C1_0(baud)      (uint8_t)(((int)M1_0(baud) - (int)M0_0(baud)) ? BV(1) : 0)
#define C1_1(baud)      (uint8_t)(((int)M1_1(baud) - (int)M0_1(baud)) ? BV(1) : 0)
#define C2_0(baud)      (uint8_t)(((int)M2_0(baud) - (int)M1_0(baud)) ? BV(2) : 0)
#define C2_1(baud)      (uint8_t)(((int)M2_1(baud) - (int)M1_1(baud)) ? BV(2) : 0)
#define C3_0(baud)      (uint8_t)(((int)M3_0(baud) - (int)M2_0(baud)) ? BV(3) : 0)
#define C3_1(baud)      (uint8_t)(((int)M3_1(baud) - (int)M2_1(baud)) ? BV(3) : 0)
#define C4_0(baud)      (uint8_t)(((int)M4_0(baud) - (int)M3_0(baud)) ? BV(4) : 0)
#define C4_1(baud)      (uint8_t)(((int)M4_1(baud) - (int)M3_1(baud)) ? BV(4) : 0)
#define C5_0(baud)      (uint8_t)(((int)M5_0(baud) - (int)M4_0(baud)) ? BV(5) : 0)
#define C5_1(baud)      (uint8_t)(((int)M5_1(baud) - (int)M4_1(baud)) ? BV(5) : 0)
#define C6_0(baud)      (uint8_t)(((int)M6_0(baud) - (int)M5_0(baud)) ? BV(6) : 0)
#define C6_1(baud)      (uint8_t)(((int)M6_1(baud) - (int)M5_1(baud)) ? BV(6) : 0)
#define C7_0(baud)      (uint8_t)(((int)M7_0(baud) - (int)M6_0(baud)) ? BV(7) : 0)
#define C7_1(baud)      (uint8_t)(((int)M7_1(baud) - (int)M6_1(baud)) ? BV(7) : 0)

// use the following macros to determine the 'baudMod' parameter values
// for usartInit0() and usartInit1()
// CAUTION - 'baud' SHOULD ALWAYS BE A CONSTANT or
// a lot of code will be generated.

#define USART0_BAUD_MOD(baud) (uint8_t)(C7_0(baud) + C6_0(baud) + C5_0(baud) + \
                                        C4_0(baud) + C3_0(baud) + C2_0(baud) + \
                                        C1_0(baud) + C0_0(baud))

// use the following macros to determine the 'mode' parameter values
// for usartInit0() and usartInit1()

#define USART_NONE  (0) 
#define USART_EVEN  (PENA + PEV)
#define USART_ODD   (PENA)
#define USART_1STOP (0)
#define USART_2STOP (SPB)
#define USART_7BIT  (0)
#define USART_8BIT  (CHAR)

// Definitions for typical USART 'mode' settings

#define USART_8N1   (uint8_t)(USART_8BIT + USART_NONE + USART_1STOP)
#define USART_7N1   (uint8_t)(USART_7BIT + USART_NONE + USART_1STOP)
#define USART_8N2   (uint8_t)(USART_8BIT + USART_NONE + USART_2STOP)
#define USART_7N2   (uint8_t)(USART_7BIT + USART_NONE + USART_2STOP)
#define USART_8E1   (uint8_t)(USART_8BIT + USART_EVEN + USART_1STOP)
#define USART_7E1   (uint8_t)(USART_7BIT + USART_EVEN + USART_1STOP)
#define USART_8E2   (uint8_t)(USART_8BIT + USART_EVEN + USART_2STOP)
#define USART_7E2   (uint8_t)(USART_7BIT + USART_EVEN + USART_2STOP)
#define USART_8O1   (uint8_t)(USART_8BIT + USART_ODD  + USART_1STOP)
#define USART_7O1   (uint8_t)(USART_7BIT + USART_ODD  + USART_1STOP)
#define USART_8O2   (uint8_t)(USART_8BIT + USART_ODD  + USART_2STOP)
#define USART_7O2   (uint8_t)(USART_7BIT + USART_ODD  + USART_2STOP)

#endif /* TARGET */

#define usart0EnableRXSE()  { U0TCTL |= URXSE; }
#define usart0DisableRXSE() { U0TCTL &= ~URXSE; }

/******************************************************************************
 *
 * Function Name: usart0Init()
 *
 * Description:  
 *    This function initializes the USART for async mode
 *
 * Calling Sequence: 
 *    baudrate divisor - use USART0_BAUD_DIV0 macro
 *    baudrate modulation - use USART0_BAUD_MOD macro
 *    mode - see typical modes (above)
 *
 * Returns:
 *    void
 *
 * NOTE: usart0Init(USART0_BAUD_DIV(9600), USART0_BAUD_MOD(9600), USART_8N1);
 *
 *****************************************************************************/
void usart0Init(uint16_t baudDiv, uint8_t baudMod, uint8_t mode);

/******************************************************************************
 *
 * Function Name: usart0Disable()
 *
 * Description:  
 *    This function disables USART hardware, turns off the interrupts
 *    and deletes the ucosii receive semaphore.
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart0Disable(void);

/******************************************************************************
 *
 * Function Name: usart0Baud()
 *
 * Description:  
 *    This function initializes the USART baudrate regs
 *
 * Calling Sequence: 
 *    baudrate divisor - use USART0_BAUD_DIV0 macro
 *    baudrate modulation - use USART0_BAUD_MOD macro
 *    mode - see typical modes (above)
 *
 * Returns:
 *    void
 *
 * NOTE: usart0Baud(USART0_BAUD_DIV(9600), USART0_BAUD_MOD(9600), USART_8N1);
 *
 *****************************************************************************/
void usart0Baud(uint16_t baudDiv, uint8_t baudMod, uint8_t mode);

/******************************************************************************
 *
 * Function Name: usart0Putch()
 *
 * Description:  
 *    This function puts a character into the USART output queue for
 *    transmission.
 *
 * Calling Sequence: 
 *    character to be transmitted
 *
 * Returns:
 *    ch on success, -1 on error (queue full)
 *
 *****************************************************************************/
int usart0Putch(char ch);

/******************************************************************************
 *
 * Function Name: usart0TxEmpty()
 *
 * Description:
 *    This function returns the status of the USART transmit data
 *    registers.
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    TRUE - if both the tx holding & shift registers are empty
 *    FALSE - either the tx holding or shift register is not empty
 *
 *****************************************************************************/
int usart0TxEmpty(void);

/******************************************************************************
 *
 * Function Name: usart0TxFlush()
 *
 * Description:  
 *    This function removes all characters from the USART transmit queue
 *    (without transmitting them).
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart0TxFlush(void);

/******************************************************************************
 *
 * Function Name: usart0RxFlush()
 *
 * Description:  
 *    This function removes all characters from the USART receive queue
 *    and  clears all outstanding semaphores.
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *
 *****************************************************************************/
void usart0RxFlush(void);

/******************************************************************************
 *
 * Function Name: usart0Getch()
 *
 * Description:  
 *    This function gets a character from the USART receive queue
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    character on success, -1 if no character is available
 *
 *****************************************************************************/
int usart0Getch(void);

/******************************************************************************
 *
 * Function Name: usart0GetchTimeout()
 *
 * Description:  
 *    This function gets a character from the USART receive queue
 *
 * Calling Sequence: 
 *    int - timeout in ticks to wait for character to arrive
 *
 * Returns:
 *    character on success, -1 if no character is available
 *
 *****************************************************************************/
int usart0GetchTimeout(unsigned int uiTimeout);

#endif /* INCLUDE_USART0_H */
