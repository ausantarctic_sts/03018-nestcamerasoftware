/*
 *	msp430 - The msp430 program.
 *	Copyright (C) 2003 Australian Antarctic Division
 *	Written by Peter Jansen <peter_jan@sts-cvs>
 *
 * Interface definition for the include/nmea.c file.
 */

#ifndef INCLUDE_NMEA_H
#define INCLUDE_NMEA_H

#include <stdint.h>

/* Command Packet Special Characters */

#define NMEA_START_PACKET    '$'
#define NMEA_QUERY_CHAR      'Q'
#define NMEA_START_CHECKSUM  '*'

extern char strMasterID[3];
extern char strSlaveID[3];

extern int8_t NmeaCheckString(char *String);
extern uint8_t NmeaCheckSum;
void NmeaPrintf(const char *fmt, ...);
extern int NmeaPutch(int ch);
extern void NmeaPacketStart(char *cmd);
extern void NmeaPacketEnd(void);

#endif /* INCLUDE_NMEA_H */
