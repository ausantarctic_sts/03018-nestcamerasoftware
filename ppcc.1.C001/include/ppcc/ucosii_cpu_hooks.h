/*
 *	Copyright (C) 2003 Australian Antarctic Division
 *
 * Interface definition for the include/ppcc/ucosii_cpu_hooks.c file.
 */

#ifndef INCLUDE_PPCC_UCOSII_CPU_HOOKS_H
#define INCLUDE_PPCC_UCOSII_CPU_HOOKS_H

#include <ucosii.h>
#ifdef TARGET
#include <msp430x14x.h>
#endif

/* 
 * OS_CPU_HOOKS_EN (os_cfg.h) - when 1 use definitions in os_cpu_c.c 
 * 
 * */ 
#if OS_CPU_HOOKS_EN == 0
void  OSInitHookBegin (void);
void  OSInitHookEnd (void);
void  OSTaskCreateHook (OS_TCB *ptcb);
void  OSTaskDelHook (OS_TCB *ptcb);
void  OSTaskIdleHook (void);
void  OSTaskStatHook (void);
void  OSTaskSwHook (void);
void  OSTCBInitHook (OS_TCB *ptcb);
void  OSTimeTickHook (void);
#endif
#endif /* INCLUDE_PPCC_UCOSII_CPU_HOOKS_H */
