/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2005 Australian Antarctic Division
 *	Written by kym_new@localhost
 *
 * Interface definition for the include/ppcc/servo.c file.
 */

#ifndef INCLUDE_PPCC_SERVO_H
#define INCLUDE_PPCC_SERVO_H

#define SERVO_WIDTH_MIN       750
#define SERVO_WIDTH_MAX      2450
#define SERVO_STEP_DEFAULT     20
#define SERVO_INITIAL_OPEN   1100
#define SERVO_INITIAL_CLOSED 2220
#define SERVO_INITIAL_OVER   2400

/* servoOpenClose Commands */
#define SERVO_ON               1
#define SERVO_OFF              2
#define SERVO_OPEN             3
#define SERVO_CLOSE            4
#define SERVO_SETTLE           5

/* servoPower / constants  */
#define SERVO_NO_ERROR         0
#define SERVO_VINPUT_ERROR     -1

extern int iServoOpenWidth;
extern int iServoClosedWidth;

extern void         servoInit(void);
extern int          servoPower(int iPower);
extern void         servoWidth(unsigned int uiWidth);
extern unsigned int servoRead(void);
extern void         servoMove(unsigned int uiWidth);
extern int          controlServo(int iOpenClose);

#endif /* INCLUDE_PPCC_SERVO_H */
