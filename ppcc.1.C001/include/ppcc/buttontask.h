/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by kym_new@localhost
 *
 * Interface definition for the include/ppcc/buttons.c file.
 */

#ifndef INCLUDE_PPCC_BUTTONS_H
#define INCLUDE_PPCC_BUTTONS_H

#include <stdint.h>

/* Task stack */
extern OS_STK ButtonTaskStk[BUTTON_TASK_STK_SIZE];

void ButtonTask(void *pdata);
void buttonCheck(void);

#endif /* INCLUDE_PPCC_BUTTONS_H */
