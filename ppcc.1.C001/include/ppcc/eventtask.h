/*
 *	Copyright (C) 2004 Australian Antarctic Division
 *
 * Interface definition for the include/ppcc/eventtask.c file.
 */

#ifndef INCLUDE_PPCC_EVENTTASK_H
#define INCLUDE_PPCC_EVENTTASK_H

extern OS_STK eventTaskStk[EVENT_TASK_STK_SIZE];

void eventTask(void *pdata);

#endif /* INCLUDE_PPCC_EVENTTASK_H */
