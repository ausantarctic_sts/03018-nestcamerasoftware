/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2004 Australian Antarctic Division
 *	Written by kym_new@localhost
 *
 * Interface definition for the include/ppcc/util.c file.
 * Core / Utility functions for the PPCC
 * 
 */

#ifndef INCLUDE_PPCC_UTIL_H
#define INCLUDE_PPCC_UTIL_H

#include <stdint.h>

/*
 * A/D Conversion & reading
 */
extern float  readVInput(void);
extern float  readMSP430Temp(void);
extern float  readVServo(void);

/* Clock */
extern void   printClock(void);
extern int    syncOSToRTC(int iLimit, int iRetries);

/* LED Flash Update */
extern void   ledFlashUpdate(void);

/* Relay */
#define RELAY_SET     1
#define RELAY_RESET   0  

void relayInit(void);
int  relayRead(void);
void relaySet(void);
void relayReset(void);

/* dip switch */
int dipSwitch(int iSelect);

/* built in self test */
void builtInSelfTest(void);

#endif /* INCLUDE_PPCC_UTIL_H */
