/*
 *	ppcc - The "ppcc" program.
 *	Copyright (C) 2005 Australian Antarctic Division
 *	Written by kym.newbery@aad.gov.au
 *
 * Interface definition for the include/ppcc/camera.c file.
 */

#ifndef INCLUDE_PPCC_CAMERA_H
#define INCLUDE_PPCC_CAMERA_H

extern float  readVCamera(void);

/* 
 * functions to return true/false for hardware conditions
 */
extern int8_t cameraActive(void);
    
/* 
 * functions to turn on/off camera & servo power 
 */
extern int setVCamera(int8_t i8Level);

/* Defines for controlCamera */
/* Stage controls */
#define CC_PULSE                8    /* pulse power on/off */
#define CC_STAGE_QUERY_POWER    7    /* query only power   */
#define CC_STAGE_QUERY          6    /* Query power and status */
#define CC_STAGE_SHOOT          5    /* Shoot only         */
#define CC_STAGE_SHOOT_DELAY    4    /* Shoot & Delay      */
#define CC_STAGE_POWEROFF       3    /* Power Off          */
#define CC_STAGE_POWERON_DELAY  2    /* Powerup & Delay    */
#define CC_STAGE_POWERON        1    /* Powerup no delay   */

/* Return Error/status */
#define CC_NO_ERROR             0
#define CC_VINPUT_FAULT        -1
#define CC_CAMERA_NOT_ACTIVE   -2
#define CC_CAMERA_ON           -3
#define CC_CAMERA_OFF          -4

extern int  controlCamera(uint8_t u8Stage);

#endif /* INCLUDE_PPCC_CAMERA_H */
