/*
 *	Copyright (C) 2003 Australian Antarctic Division
 *	Written by kym_new <kym_new@sts-cvs>
 *
 * Interface definition for the include/ppcc/main.c file.
 */

#ifndef INCLUDE_PPCC_MAIN_H
#define INCLUDE_PPCC_MAIN_H

#include <stdint.h>

#define SERIALNUMBER_STRLEN 31
struct configTableStruct
{
    char    strSerialNumber[SERIALNUMBER_STRLEN];
    uint8_t u8Messages;               /* Controls bootup messages */

    /* Parameters for controlling camera timing / operation */
    float fVInputCamera;              /* Minimum input to turn on camera        */
    float fTurnOnDelay;               /* Camera Turn on delay                   */
    float fExposeWriteDelay;          /* Delay for photo and writing to card    */
    float fPulseLength;               /* Pulse length for focus/shutter signals */
    unsigned int uiCameraOffTimer;    /* Camera turn off timer                  */
    
    /* Parameters for shutter timing & operation */
    float fVInputServo;               /* minimum input for servo                */
    int   iServoStep;                 /* Default servo step size                */
    int   iServoOpen;                 /* Open position                          */
    int   iServoClosed;               /* Closed position                        */
    int   iServoOver;                 /* Position beyond closed                 */
    
    /* Parameters for built in self test - upper & lower limits for voltages */
    float fVInputMin;
    float fVInputMax;
    float fVServoMin;
    float fVServoMax;
    float fVCameraMin;
    float fVCameraMax;

    float fClockPowerPulseLength;     /* Camera clock power pulse length        */
};

// #define configTableSection __attribute__((section(".infomem")))
#define configTableSection
extern configTableSection struct configTableStruct configTable;

/* ucosii specific */
#define SPARE1_MUTEX_PRIORITY   (INT8U)0
#define HARDWARE_MUTEX_PRIORITY (INT8U)1
#define RTC_MUTEX_PRIORITY      (INT8U)2
#define SPARE2_MUTEX_PRIORITY   (INT8U)3
#define CMD_TASK_PRIORITY	(INT8U)4
#define EVENT_TASK_PRIORITY	(INT8U)5
#define BUTTON_TASK_PRIORITY    (INT8U)6
#define IDLE_TASK_PRIORITY      (INT8U)7

/* The number of tasks on the PPCC */
#define PPCC_TASKS              3    

/* TASK ID - for stack checking - i.e. creating tasks with CreatetaskExt() */
#define CMD_TASK_ID		0
#define EVENT_TASK_ID	        1
#define BUTTON_TASK_ID          2

#define CMD_TASK_NAME           "CMD Task"
#define EVENT_TASK_NAME         "EVENT Task"
#define BUTTON_TASK_NAME        "BUTTON Task"
#define IDLE_TASK_NAME          "IDLE Task"

/*****************************************************/

/* Task Stack size doesn't have to be uniform 
 * 
 * Default size of a task's stack is 64 entries - see OS_CFG.H - 
 * each entry is OS_STK wide (unsigned int)
 *
 */

#define CMD_TASK_STK_SIZE     OS_TASK_DEF_STK_SIZE + 60
#define EVENT_TASK_STK_SIZE   OS_TASK_DEF_STK_SIZE + 60
#define BUTTON_TASK_STK_SIZE  OS_TASK_DEF_STK_SIZE + 20

typedef struct  {
    unsigned int  TaskCtr;
} TASK_USER_DATA;

extern OS_EVENT *HardwareMutex;
extern OS_EVENT *RTCMutex;

extern TASK_USER_DATA TaskUserData[];
extern OS_STK_DATA OSStackData[];

/* end ucosii */

/*******************************************************/
/* Command Task <-> event Task inter process           */
/* communication globals                               */

#define IPC_RESULT_STRLEN   12  
#define IPC_UPDATE          0x01
#define IPC_EVENT_DISABLE   0x02      /* used to disable event table */

struct IPCGlobalsStruct
{
    unsigned int  uiPeriod;         // Period between measurements in secs
    unsigned int  uiOffset;         // Offset from start of period
    uint8_t       u8Flags;          // Set when values updated
};
extern struct IPCGlobalsStruct IPCGlobals;

/******************************************************************/
/* LED Modes                                                      */
#define LED_BIST_ON    3
#define LED_FLASH_2HZ  2
#define LED_ON         1
#define LED_OFF        0

extern uint8_t u8LEDRed;
extern uint8_t u8LEDGreen;

/*********************************/
extern int NmeaPutch(int ch);
extern int port0Putchar(char ch);
extern int port0Getchar(void);

extern int main(void);

#endif /* INCLUDE_PPCC_MAIN_H */
