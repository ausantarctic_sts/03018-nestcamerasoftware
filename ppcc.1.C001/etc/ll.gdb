set remoteaddresssize 64
set remotetimeout 999999
set download-write-size 512
handle SIGUSR1 nostop noprint
set remote memory-write-packet-size 1024
set remote memory-write-packet-size fixed
set remote memory-read-packet-size 1024
set remote memory-read-packet-size fixed
directory bl
directory .
target remote localhost:2000
mon erase all
load
