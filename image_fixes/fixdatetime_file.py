#!/bin/env python

"""
 Script to create another sh script that uses exiftool to re-write the datetimes 
 in many JPG files, given a CSV file with filenames and date/times that should be written.

 eg:

 File Number,DateTime
 IMG_0772.JPG,2012:10:01,5:00:00
 IMG_0773.JPG,2012:10:02,5:00:00
 IMG_0774.JPG,2012:10:03,5:00:00
 IMG_0775.JPG,2012:10:04,5:00:00

"""
import sys
import os
import argparse
import csv
from datetime import datetime, tzinfo

class tzinfo_UTC(tzinfo):
    """     UTC tzinfo object   """
    def utcoffset(self, dt):
        return datetime.timedelta(0)

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return datetime.timedelta(0)


if __name__ == "__main__":

    # parse the command line arguments
    parser = argparse.ArgumentParser(description='create exif re-writer script based on input file')
    
    parser.add_argument('--file',
                        action='store',
                        nargs='?',
                        required=True,
                        help='filename for datetime re-write control file')
    
    if (len(sys.argv) < 2):
        print;
        parser.print_help()
        print;
        sys.exit()
    else:
        args = parser.parse_args()
    
        # test for config
        try:
            fControl = open(os.path.abspath(args.file), 'r')
        except IOError as e:
            sys.stderr.write("%s\r\n" % e)
            sys.exit()
        
        # Get dirname to prepend to image filenames
        fcDirname = os.path.dirname(args.file)
        
        # read in as CSV
        rows = csv.reader(fControl)
        
        """ - example file
        ['File Number', 'DateTime']
        ['IMG_0772.JPG', '2012:10:01 5:00:00']
        ['IMG_0773.JPG', '2012:10:02 5:00:00']
        ['IMG_0774.JPG', '2012:10:02 5:00:00']
        ['IMG_0775.JPG', '2012:10:02 5:00:00']

        """
        
        for row in rows:
            filename = os.path.join(fcDirname, row[0])
            if os.path.isfile(filename):
                # make datetime to program with
                dtFile = datetime.strptime(row[1], "%Y:%m:%d %H:%M:%S")
                
                UserComment = "DateTimeOriginal fixed"
                
                print 'exiftool -UserComment="%s" -DateTimeOriginal="%s" -CreateDate="%s" -ModifyDate="%s" "%s"' % (UserComment, 
                                                                                                					dtFile.isoformat(), 
                                                                                                   					dtFile.isoformat(), 
																													dtFile.isoformat(),
                                                                                                   					filename)
            else:
                sys.stderr.write("Skipped '%s'\r\n" % filename)
                
        
        
    
