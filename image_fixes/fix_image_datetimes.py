#!/bin/env python
#
# fix_image_datetimes.py
#
# This program has been written to fix a problem with the Canon EOS1000D camera where it
# looses it's date/time due to the power being removed for too long. The EOS1000D does NOT
# have a built in backup battery like the EOS350D/400D cameras. Hence, until the camera 
# program is fixed to periodically recharge/repower the camera, all images in EOS1000D
# cameras where there are no regular photos throughout the entire year (mostly Adelie)
# will probably have date/time image problems.
#
# Read a nest camera program and determine a list of date/times for which photos are taken 
# given a start and stop times (since nest camera programs do not consider the year), then
# read a list of JPG files from a folder then generate command line arguments for exiftool
# to write the correct date/time data back into the files.  It is up to the user to work out
# the proper date/time for the image files since there is absolutely NO indication in the 
# image exif metadata. 
#
# The output command line arguments modify the following :
#    DateTimeOriginal & CreateDate  - set to the supposed correct date for the picture
#    ModifyDate                     - set to the current datetime (when the fix is done)
#    UserComment                    - set to a text string to say "DateTimeOriginal is fixed"
#
# Current issues :
#   - this is a very basic script that steps through all possible dates/times between start
#     and stop.  It steps in HOURS!! (to speed up the process), and doesn't check to make sure
#     that there are any picture programs that occur on non-hourly boundaries. To step in 
#     minutes modify 
#            dt_delta = datetime.timedelta(hours=1)
#     to 
#            dt_delta = datetime.timedelta(minutes=1)
#
#   - Currently only looks for *.JPG files, modify script to detect *.jpg files..
#
# Command line arguments
#
# --start    <YYYY/MM/DD>         start date/time, eg 2011/07/01
# --end      <YYYY/MM/DD>         end date/time, eg 2012/06/30
# --program  <filename>           nest camera program file, eg camera_program_Casey_Adelie.txt
# --filedir  <filedir>            directory of JPG files, ./fi
#
# Output format is directly suitable for running as a /bin/sh script, here is an example only!! 
#
# [kym_new@sts-dev image_fixes]$ python read_program.py --start 2011/07/01 --end 2012/06/30 --program ../CameraConfigurations/Casey/camera_program_Casey_Adelie.txt --filedir ../CameraConfigurations/Davis/Gardner\ Island/site1/
# exiftool -v -UserComment="DateTimeOriginal fixed" -DateTimeOriginal=2011-10-01T05:00:00 -CreateDate=2011-10-01T05:00:00 -ModifyDate=2012-06-19T03:28:22.443505 ../CameraConfigurations/Davis/Gardner Island/site1/IMG_2098.JPG
#
# KBN 2012/06/19
#
import sys
import argparse
import datetime
import glob

#
# dt = datetime to test
# wc_packed = wildcard packed in string as "MMDDHHmm"  (MM = month, mm = minute)
#
# returns True or False
#
def wc_test (dt, wc_packed):
    """ test a wildcard against a datetime, returns true/false"""
    # build packed datetime to compare with the wildcard packed time
    dt_packed = dt.strftime("%m%d%H%M")

    # step through to compare strings for a match, skip wildcards
    digit = 0
    for char in wc_packed:
        if ((char == '*') or ( char == '?')):
            digit = digit + 1
            continue
        if (char == dt_packed[digit]):
            digit = digit + 1
        else:
            return False

    return True


# parse the command line arguments
parser = argparse.ArgumentParser(description='Read a camera program, and determine actual date/times when pictures taken')

parser.add_argument('--start',
                    action='store',
                    nargs='?',
                    required=True,
                    help='start date for period, in YYYY/MM/DD format eg 2009/07/01')
parser.add_argument('--end',
                    action='store',
                    nargs='?',
                    required=True,
                    help='start date for period, in YYYY/MM/DD format eg 2010/06/31')
parser.add_argument('--program',
                    action='store',
                    nargs='?',
                    required=True,
                    help='nest camera program file')
parser.add_argument('--filedir',
                    action='store',
                    nargs='?',
                    required=True,
                    help='path to jpg files')

args = parser.parse_args()

#print "start   [", args.start,"]"
#print "end     [", args.end,"]"
#print "program [", args.program,"]"
#print "filedir [", args.filedir,"]"

dt_start = datetime.datetime.strptime(args.start, "%Y/%m/%d")
dt_end = datetime.datetime.strptime(args.end, "%Y/%m/%d")

if (dt_start > dt_end):
    print "Error : start time must be before end time"
    sys.exit()

# find all files in a folder (no subdirectories) & order by sequence
jpg_files = glob.glob(args.filedir+"/*.JPG")
if (len(jpg_files) == 0):
    print "No Image files found"
    sys.exit()

# sort by filename - this will break if sequence number wraps around
# from 9999 to 0000. Try looking at internal sequence number? 100_XXXX
# does it go to 101_XXXX ?
#
jpg_files.sort()
#for jpg_file in jpg_files:
#    print jpg_file

# open the program file
f = open(args.program, 'r')

#print "start time ", dt_start.isoformat()
#print "end time   ", dt_end.isoformat()

# check lines for picture events, then load wilcard values
lines = f.readlines()
wc_lines = []
for line in lines:
    args = line.split(' ')
    if ((args[0].lower() == 'event') and (args[1].lower() == 'a')): # event 'add'
        if (args[6].lower().count('picture') > 0):                   # 'picture' command
            # pack datetime from wildcard
            wc_packed = args[2]+args[3]+args[4]+args[5]
            wc_lines.append(wc_packed)

#print wc_lines

# check all wildcards to find out the minimum time step, and the start/stop months.


# now iterate through time checking all wildcards - currently set to hourly steps (for speed)
dt = dt_start
dt_delta = datetime.timedelta(hours=1)
prog_times = []
while dt < dt_end:
    for wc in wc_lines:
        if wc_test(dt, wc):
            prog_times.append(dt)
    dt += dt_delta

f.close()

#for prog_time in prog_times:
#    print prog_time

mincount = min(len(prog_times), len(jpg_files))
#print "prog_times=%d jpg_files=%d mincount=%d" % (len(prog_times), len(jpg_files), mincount)

# iterate through jpg files & programs from first date/time program
# and write in the datetime original, createdate, and modifydate
i = 0
UserComment = "DateTimeOriginal fixed"
ModifyDate = datetime.datetime.now()
while i < mincount:
    ts = prog_times[i].isoformat()
    print "exiftool -v -UserComment=\"%s\" -DateTimeOriginal=%s -CreateDate=%s -ModifyDate=%s %s" % (UserComment, ts, ts, ModifyDate.isoformat(), jpg_files[i])
    i += 1


